<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Technical extends Model
{
    protected $table = 'details';

    // Street condition relationship
    public function street()
    {
        return $this->belongsTo('App\Street');
    }

    public function maintenanceType()
    {
        return $this->belongsTo('App\MaintenanceType');
    }
    
    public function streetType()
    {
        return $this->belongsTo('App\StreetType');
    }

    protected $fillable = [
        'street_id',
        'year',
        'length',
        'width',
        'lhrt',
        'vcr',
        'street_type_id',
        'mst',
        'pavement',
        'gravel',
        'macadam',
        'asphalt',
        'rigid',
        'last_maintained_at',
        'maintenance_type_id',
    ];
}
