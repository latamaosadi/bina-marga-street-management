<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Street;
use App\Bridge;

class DashboardController extends Controller
{
  // Home controller to view the home page
  public function index(Request $request)
  {
    $year = $request->query('year', date('Y'));
    if (empty($year)) {
      $year = date('Y');
    }

    $bridge = [];
    $street = [];

    $street['count'] = Street::count();
    $bridge['count'] = Bridge::count();

    $street['perDistrict'] = DB::table('streets')
                              ->join('districts', 'streets.kecamatan_id', '=', 'districts.id')
                              ->select('districts.id', DB::raw('count(*) as total'), 'districts.name')
                              ->groupBy('districts.id')
                              ->orderBy('total', 'desc')
                              ->get();

    $bridge['perDistrict'] = DB::table('bridges')
                              ->join('streets', 'bridges.street_id', '=', 'streets.id')
                              ->join('districts', 'streets.kecamatan_id', '=', 'districts.id')
                              ->select('districts.id', DB::raw('count(*) as total'), 'districts.name')
                              ->groupBy('districts.id')
                              ->get();

    $street['noDistricts'] = DB::table('streets')
                              ->whereNull('kecamatan_id')
                              ->count();

    $bridge['noStreets'] = DB::table('bridges')
                              ->whereNull('street_id')
                              ->count();

    $street['lengthPerYear'] = DB::table('details')
                              ->select('last_maintained_at as year', DB::raw('SUM(length) as total_length'))
                              ->groupBy('last_maintained_at')
                              ->get();

    $street['notMaintained'] = DB::table('details')
                              ->join('streets', 'details.street_id', '=', 'streets.id')
                              ->leftJoin('districts', 'streets.kecamatan_id', '=', 'districts.id')
                              ->select('streets.id', 'streets.name', 'districts.name as kecamatan', DB::raw('replace(streets.polyline, ";", "|") as polyline'), DB::raw('max(details.last_maintained_at) as last_maintained_at'))
                              ->where('details.last_maintained_at', '!=', $year)
                              ->orderBy('last_maintained_at', 'asc')
                              ->groupBy('streets.id')
                              ->limit(12)
                              ->get();

    $data = compact('bridge', 'street', 'year');


    // dd($data);

    return view('dashboard.index', $data);
  }

}
