<?php

namespace App\Http\Controllers\Street;

use App\Http\Controllers\Controller;
use App\Street;
use App\Condition;
use App\Technical;
use App\Image;
use App\StreetVideo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Indonesia;
use Cocur\Slugify\Slugify;
use InterventionImage;

class StreetController extends Controller
{
  /**
   * Street list view controller
   *
   * @param Request $request Application request
   * @return view
   * @throws null
   **/
  public function list(Request $request)
  {
    $query = $request->input('query');
    $take = config('app.pagination.street.take');
    $streets = Street::with('village', 'village.district', 'village.district.city');
    if (! empty($query)) {
      $streets = $streets->where('name', 'like', sprintf('%%%s%%', $query))
                        ->orWhere('section_number', 'like', sprintf('%%%s%%', $query))
                        ->orWhere('base_section_name', 'like', sprintf('%%%s%%', $query))
                        ->orWhere('end_section_name', 'like', sprintf('%%%s%%', $query));
    }
    $streets = $streets->paginate($take);

    if ($request->ajax()) {
      return View::make('street.table-records', compact('streets', 'query'));
    }

    return view('street.list', compact('streets', 'query'));
  }

  /**
   * Street list view controller
   *
   * @param Request $request Application request
   * @return view
   * @throws null
   **/
  public function create(Request $request)
  {
    $kabupaten = Indonesia::findProvince(51, ['cities']);

    return view('street.create', compact('kabupaten'));
  }

  /**
   * Street list view controller
   *
   * @param Request $request Application request
   * @return view
   * @throws null
   **/
  public function edit(Request $request, $id)
  {
    $kabupaten = Indonesia::findProvince(51, ['cities']);
    $street = Street::find($id);
    $street->base_coordinate_section = sprintf('%s,%s', $street->base_section_latitude, $street->base_section_longitude);
    $street->end_coordinate_section = sprintf('%s,%s', $street->end_section_latitude, $street->end_section_longitude);

    $path = [];
    $rawCoordinates = explode(';', $street->polyline);
    foreach ($rawCoordinates as $key => $coordinate) {
      $explodedCoordinate = explode(',', $coordinate);
      array_push($path, (object)[
        'lat' => (float)$explodedCoordinate[0],
        'lng' => (float)$explodedCoordinate[1],
      ]);
    }
    $street->polyline = json_encode($path);

    return view('street.edit', compact('kabupaten', 'street'));
  }

  /**
   * Street new street controller
   *
   * @param Request $request Application request
   * @return redirect
   * @throws null
   **/
  public function new(Request $request)
  {
    $baseSectionCoordinate = explode(',', $request->input('base_coordinate_section', ''));
    $endSectionCoordinate = explode(',', $request->input('end_coordinate_section', ''));

    $street = new Street;
    $street->name = $request->input('name');
    $street->section_number = $request->input('number');
    $street->base_section_name = $request->input('base_section_name');
    $street->end_section_name = $request->input('end_section_name');
    $street->kabupaten_id = config('app.kabupaten');
    $street->kecamatan_id = $request->input('kecamatan');
    $street->kelurahan_id = $request->input('kelurahan');
    $street->base_section_latitude = $baseSectionCoordinate[0];
    $street->base_section_longitude = $baseSectionCoordinate[1];
    $street->end_section_latitude = $endSectionCoordinate[0];
    $street->end_section_longitude = $endSectionCoordinate[1];
    $street->polyline = $request->input('polyline');
    $street->google_base_street_name = $request->input('google_base_street_name');
    $street->google_end_street_name = $request->input('google_end_street_name');
    $street->google_middle_street_name = $request->input('google_middle_street_name');

    $street->save();
    
    return redirect()->route('admin.streets.list');
  }

  /**
   * Street new street controller
   *
   * @param Request $request Application request
   * @return redirect
   * @throws null
   **/
  public function update(Request $request, $id)
  {
    $baseSectionCoordinate = explode(',', $request->input('base_coordinate_section', ''));
    $endSectionCoordinate = explode(',', $request->input('end_coordinate_section', ''));

    $street = Street::find($id);
    $street->name = $request->input('name');
    $street->section_number = $request->input('number');
    $street->base_section_name = $request->input('base_section_name');
    $street->end_section_name = $request->input('end_section_name');
    $street->kabupaten_id = config('app.kabupaten');
    $street->kecamatan_id = $request->input('kecamatan');
    $street->kelurahan_id = $request->input('kelurahan');
    $street->base_section_latitude = $baseSectionCoordinate[0];
    $street->base_section_longitude = $baseSectionCoordinate[1];
    $street->end_section_latitude = $endSectionCoordinate[0];
    $street->end_section_longitude = $endSectionCoordinate[1];
    $street->polyline = $request->input('polyline');
    $street->google_base_street_name = $request->input('google_base_street_name');
    $street->google_end_street_name = $request->input('google_end_street_name');
    $street->google_middle_street_name = $request->input('google_middle_street_name');

    $street->save();
    
    return redirect()->route('admin.streets.list');
  }

  // Home controller to view the home page
  public function search(Request $request)
  {
    $query = $request->input('name');
    $streets = Street::where('name', 'like', sprintf('%%%s%%', $query))
                        ->orWhere('section_number', 'like', sprintf('%%%s%%', $query))
                        ->orWhere('base_section_name', 'like', sprintf('%%%s%%', $query))
                        ->orWhere('end_section_name', 'like', sprintf('%%%s%%', $query))
                        ->orWhere('google_base_street_name', 'like', sprintf('%%%s%%', $query))
                        ->orWhere('google_end_street_name', 'like', sprintf('%%%s%%', $query))
                        ->orWhere('google_middle_street_name', 'like', sprintf('%%%s%%', $query))
                        ->get();

    $data = [
      'view' => View::make('_partials.result', compact('streets', 'query'))
    ];

    return $data['view'];
  }

  public function all(Request $request)
  {
    $districtId = $request->input('kecamatan', null);
    if (! empty($districtId)) {
      $streets = Street::where('kecamatan_id', '=', $districtId)->get();
    } else {
      $streets = Street::all();
    }
    $mappedStreets = $streets->map(function($street) {
      $path = [];
      $rawCoordinates = explode(';', $street->polyline);
      foreach ($rawCoordinates as $key => $coordinate) {
        $explodedCoordinate = explode(',', $coordinate);
        array_push($path, (object)[
          'latitude' => (float)$explodedCoordinate[0],
          'longitude' => (float)$explodedCoordinate[1],
        ]);
      }
      return (object)[
        'id' => $street->id,
        'name' => $street->name,
        'path' => $path,
      ];
    });
    return response()->json($mappedStreets);
  }

  public function detail(Request $request, $id)
  {
    $street = Street::find($id);

    $data = [
      'view' => View::make('_partials.detail', compact('street'))
    ];

    return $data['view'];
    return response()->json($street);
  }

  public function destroy(Request $request, $id)
  {
    try {
      $street = Street::find($id);
      $street->delete();
    } catch (Exception $e) {
      return $e->getMessage();
    }

    return response()->json($street);
  }

  public function bridgeList(Request $request, $id)
  {
    $street = Street::find($id);

    $path = [];
    $rawCoordinates = explode(';', $street->polyline);
    foreach ($rawCoordinates as $key => $coordinate) {
      $explodedCoordinate = explode(',', $coordinate);
      array_push($path, (object)[
        'lat' => (float)$explodedCoordinate[0],
        'lng' => (float)$explodedCoordinate[1],
      ]);
    }
    $street->polyline = json_encode($path);

    return view('street.bridges', compact('street'));
  }

  public function conditionList(Request $request, $id)
  {
    $currentYear = date('Y');
    $selectedYear = $request->input('year', $currentYear);
    $currentCondition = null;

    $take = config('app.pagination.street.take');
    $street = Street::find($id);
    $rawConditions = $street->conditions();

    if (! empty($selectedYear)) {
      $currentCondition = Condition::where([
        ['street_id', '=', $id],
        ['year', '=', $selectedYear],
      ])->first();
    }

    $conditions = $rawConditions->paginate($take);
    $years = [];
    for ($year = $currentYear + 2 ; $year > $currentYear - 8; $year--) { 
      array_push($years, $year);
    }

    $vars = compact('street', 'conditions', 'years', 'selectedYear', 'currentCondition');

    return view('street.conditions', $vars);
  }

  public function technicalList(Request $request, $id)
  {
    $currentYear = date('Y');
    $selectedYear = $request->input('year', $currentYear);
    $currentTechnical = null;

    $take = config('app.pagination.street.take');
    $street = Street::find($id);
    $rawTechnicals = $street->technicals();

    if (! empty($selectedYear)) {
      $currentTechnical = Technical::where([
        ['street_id', '=', $id],
        ['year', '=', $selectedYear],
      ])->first();
    }

    $technicals = $rawTechnicals->paginate($take);
    $years = [];
    for ($year = $currentYear + 2 ; $year > $currentYear - 8; $year--) { 
      array_push($years, $year);
    }

    $vars = compact('street', 'technicals', 'years', 'selectedYear', 'currentTechnical');

    return view('street.technicals', $vars);
  }

  public function newCondition(Request $request)
  {
    $streetId = $request->input('street_id');
    $year = $request->input('year', null);

    $condition = Condition::firstOrNew([
      'street_id' => $streetId,
      'year' => $year,
    ]);

    $condition->year = $year;
    $condition->good = $request->input('good');
    $condition->average = $request->input('average');
    $condition->light = $request->input('light');
    $condition->heavy = $request->input('heavy');

    $condition->save();
    
    return redirect()->route('admin.streets.conditions.list', ['id' => $streetId, 'year' => $condition->year]);
  }

  public function newTechnical(Request $request)
  {
    $streetId = $request->input('street_id');
    $year = $request->input('year', null);

    $technical = Technical::firstOrNew([
      'street_id' => $streetId,
      'year' => $year,
    ]);

    $technical->street_id = $streetId;
    $technical->year = $year;
    $technical->length = $request->input('length');
    $technical->width = $request->input('width');
    $technical->lhrt = $request->input('lhrt');
    $technical->vcr = $request->input('vcr');
    $technical->street_type_id = $request->input('street_type_id');
    $technical->mst = $request->input('mst');
    $technical->pavement_id = $request->input('pavement_id');
    $technical->gravel = $request->input('gravel');
    $technical->macadam = $request->input('macadam');
    $technical->asphalt = $request->input('asphalt');
    $technical->rigid = $request->input('rigid');
    $technical->last_maintained_at = $request->input('last_maintained_at');
    $technical->maintenance_type_id = $request->input('maintenance_type_id');

    $technical->save();
    
    return redirect()->route('admin.streets.technicals.list', ['id' => $streetId, 'year' => $technical->year]);
  }

  public function imageList(Request $request, $id)
  {
    $take = config('app.pagination.street.take');
    $street = Street::find($id);
    $rawImages = $street->images();

    $images = $rawImages->paginate($take);

    $vars = compact('street', 'images');

    return view('street.images', $vars);
  }

  public function newImage(Request $request, $id)
  {
    $streetId = $id;

    $path = $request->file('files')[0];
    $originalName = $path->getClientOriginalName();

    $originalNameChunks = explode('.', $originalName);
    $extension = array_pop($originalNameChunks);
    $name = implode('.', $originalNameChunks);

    $resize = InterventionImage::make($path)->resize(640, 640, function ($constrain) {
        $constrain->aspectRatio();
    })->encode($extension);

    // calculate md5 hash of encoded image
    $hash = Hash::make($resize->__toString());

    // use hash as a name
    $path = "gallery/{$hash}.{$extension}";

    // save it locally to ~/public/images/{$hash}.jpg
    $savePath = Storage::put($path, $resize->__toString());

    $image = new Image;

    $image->street_id = $streetId;
    $image->filename = $path;
    $image->name = $name;

    $image->save();

    return response()->json(['files' => [$image]]);
  }

  public function setAvatar(Request $request, $id)
  {
    $street = Street::find($id);
    $street->avatar_id = $request->input('imageId', null);
    $street->save();

    return response()->json($street);
  }

  public function setCover(Request $request, $id)
  {
    $street = Street::find($id);
    $street->cover_id = $request->input('imageId', null);
    $street->save();

    return response()->json($street);
  }

  public function videoList(Request $request, $id)
  {
    $take = config('app.pagination.street.take');
    $street = Street::find($id);
    $rawVideos = $street->videos();

    $videos = $rawVideos->paginate($take);

    $vars = compact('street', 'videos');

    return view('street.videos', $vars);
  }

  public function newVideo(Request $request, $id)
  {
    $streetId = $id;

    $title = $request->input('title', '');
    $videoUrl = $request->input('video_url', '');

    // if  ($parsed = parse_url($videoUrl)) {
    //   if (! isset($parsed['scheme'])) {
    //     $videoUrl = sprintf('http://%s', $videoUrl);
    //   }
    // }

    $video = new StreetVideo;

    $video->street_id = $streetId;
    $video->video_url = $videoUrl;
    $video->title = $title;

    $video->save();

    return redirect()->route('admin.streets.videos.list', ['id' => $streetId]);
  }

  public function destroyStreetVideo(Request $request, $id, $videoId)
  {
    try {
      $video = StreetVideo::find($videoId);
      $video->delete();
    } catch (Exception $e) {
      return $e->getMessage();
    }

    return response()->json($video);
  }

}
