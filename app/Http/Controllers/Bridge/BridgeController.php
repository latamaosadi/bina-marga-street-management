<?php

namespace App\Http\Controllers\Bridge;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Bridge;
use App\Street;
use App\BridgeImage;
use App\BridgeDetail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use InterventionImage;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Facades\DB;

class BridgeController extends Controller
{
  // Home controller to view the home page
  public function list(Request $request)
  {
    $query = $request->input('query');
    $take = config('app.pagination.street.take');
    $bridges = Bridge::with('street');
    if (! empty($query)) {
      $bridges = $bridges->where('name', 'like', sprintf('%%%s%%', $query));
                        // ->orWhere('section_number', 'like', sprintf('%%%s%%', $query))
                        // ->orWhere('base_section_name', 'like', sprintf('%%%s%%', $query))
                        // ->orWhere('end_section_name', 'like', sprintf('%%%s%%', $query));
    }
    $bridges = $bridges->paginate($take);
    // dd($bridges);
    return view('bridge.list', compact('bridges', 'query'));
  }

  public function create(Request $request)
  {
    $streets = Street::all();

    return view('bridge.create', compact('streets'));
  }

  public function edit(Request $request, $id)
  {
    $streets = Street::all();
    $bridge = Bridge::find($id);

    return view('bridge.edit', compact('streets', 'bridge'));
  }

  public function store(Request $request, $id = null)
  {
    $bridgeId = $request->input('bridge_id', $id);
    $streetId = $request->input('street_id', null);
    if (! empty($bridgeId)) {
      $bridge = Bridge::find($bridgeId);
    } else {
      $bridge = new Bridge;
    }

    if (! empty($streetId)) {
      $bridge->street_id = $streetId;
    }
    $bridge->name = $request->input('name', null);
    $bridge->description = $request->input('description', null);
    $bridge->latitude = $request->input('latitude', null);
    $bridge->longitude = $request->input('longitude', null);

    $bridge->save();
    
    if (! empty($streetId)) {
      return redirect()->route('admin.streets.bridges.list', ['id' => $streetId]);
    }
    return redirect()->route('admin.bridges.list');
  }

  public function destroy(Request $request, $id)
  {
    try {
      $bridge = Bridge::find($id);
      $bridge->delete();
    } catch (Exception $e) {
      return $e->getMessage();
    }

    return response()->json($bridge);
  }

  public function all(Request $request)
  {
    $districtId = $request->input('kecamatan', null);
    $bridges = DB::table('bridges');
    if (! empty($districtId)) {
      $bridges = $bridges->join('streets', 'bridges.street_id', '=', 'streets.id')
              ->where('streets.kecamatan_id', '=', $districtId);
    }
    $bridges = $bridges->select('bridges.*')
            ->get();
    $mappedBridges = $bridges->map(function($bridge) {
      return (object)[
        'id' => $bridge->id,
        'name' => $bridge->name,
        'coord' => [
          'lat' => $bridge->latitude,
          'lng' => $bridge->longitude,
        ],
      ];
    });
    return response()->json($mappedBridges);
  }

  public function imageList(Request $request, $id)
  {
    $take = config('app.pagination.street.take');
    $bridge = Bridge::find($id);
    $rawImages = $bridge->images();

    $images = $rawImages->paginate($take);

    $vars = compact('bridge', 'images');

    return view('bridge.images', $vars);
  }

  public function newImage(Request $request, $id)
  {
    $bridgeId = $id;

    $path = $request->file('files')[0];
    $originalName = $path->getClientOriginalName();

    $originalNameChunks = explode('.', $originalName);
    $extension = array_pop($originalNameChunks);
    $name = implode('.', $originalNameChunks);

    $resize = InterventionImage::make($path)->resize(640, 640, function ($constrain) {
      $constrain->aspectRatio();
    })->encode($extension);

    // calculate md5 hash of encoded image
    $hash = Hash::make($resize->__toString());

    // use hash as a name
    $path = "gallery/{$hash}.{$extension}";

    // save it locally to ~/public/images/{$hash}.jpg
    $savePath = Storage::put($path, $resize->__toString());

    $image = new BridgeImage;

    $image->bridge_id = $bridgeId;
    $image->filename = $path;
    $image->name = $name;

    $image->save();

    return response()->json(['files' => [$image]]);
  }

  public function detailList(Request $request, $id)
  {
    $currentYear = date('Y');
    $selectedYear = $request->input('year', $currentYear);
    $currentDetail = null;

    $take = config('app.pagination.street.take');
    $bridge = Bridge::find($id);
    $rawDetails = $bridge->details();

    if (! empty($selectedYear)) {
      $currentDetail = BridgeDetail::where([
        ['bridge_id', '=', $id],
        ['year', '=', $selectedYear],
      ])->first();
    }

    $details = $rawDetails->paginate($take);
    $years = [];
    for ($year = $currentYear + 2 ; $year > $currentYear - 8; $year--) { 
      array_push($years, $year);
    }

    $vars = compact('bridge', 'details', 'years', 'selectedYear', 'currentDetail');

    return view('bridge.details', $vars);
  }

  public function newDetail(Request $request)
  {
    $bridgeId = $request->input('bridge_id');
    $year = $request->input('year', null);

    $detail = BridgeDetail::firstOrNew([
      'bridge_id' => $bridgeId,
      'year' => $year,
    ]);

    $detail->bridge_id = $bridgeId;
    $detail->year = $year;
    $detail->length = $request->input('length');
    $detail->width = $request->input('width');
    $detail->bent_amount = $request->input('bent_amount');
    $detail->top_type = $request->input('top_type');
    $detail->top_condition = $request->input('top_condition');
    $detail->floor_type = $request->input('floor_type');
    $detail->floor_material = $request->input('floor_material');
    $detail->floor_condition = $request->input('floor_condition');
    $detail->support_type = $request->input('support_type');
    $detail->support_material = $request->input('support_material');
    $detail->support_condition = $request->input('support_condition');
    $detail->foundation_type = $request->input('foundation_type');
    $detail->foundation_material = $request->input('foundation_material');
    $detail->foundation_condition = $request->input('foundation_condition');
    $detail->head_type = $request->input('head_type');
    $detail->head_material = $request->input('head_material');
    $detail->head_condition = $request->input('head_condition');
    $detail->pillar_type = $request->input('pillar_type');
    $detail->pillar_material = $request->input('pillar_material');
    $detail->pillar_condition = $request->input('pillar_condition');
    $detail->notes = $request->input('notes');

    $detail->save();
    
    return redirect()->route('admin.bridges.details.list', ['id' => $bridgeId, 'year' => $detail->year]);
  }
}
