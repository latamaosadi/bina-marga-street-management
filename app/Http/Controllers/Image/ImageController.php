<?php

namespace App\Http\Controllers\Image;

use App\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
  public function destroy(Request $request, $id)
  {
    try {
      $image = Image::find($id);
      Storage::delete($image->filename);
      $image->delete();
    } catch (Exception $e) {
      return $e->getMessage();
    }

    return response()->json($image);
  }

  public function update(Request $request, $id)
  {
    try {
      $image = Image::find($id);
      $image->name = $request->input('name');
      $image->save();
    } catch (Exception $e) {
      return $e->getMessage();
    }

    return response()->json($image);
  }
}
