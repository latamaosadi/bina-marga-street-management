<?php

namespace App\Http\Controllers\Administrative;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Indonesia;

class AdministrativeController extends Controller
{
  /**
   * Street list view controller
   *
   * @param Request $request Application request
   * @return view
   * @throws null
   **/
  public function provinces(Request $request)
  {
    $provinces = Indonesia::allProvinces();
    return response()->json($provinces);
  }

  public function cities(Request $request)
  {
    $provinceId = $request->input('provinceId');

    $province = Indonesia::findProvince($provinceId, ['cities']);
    return response()->json($province->cities);
  }

  public function districts(Request $request)
  {
    $cityId = $request->input('cityId');

    $city = Indonesia::findCity($cityId, ['districts']);
    return response()->json($city->districts);
  }

  public function villages(Request $request)
  {
    $districtId = $request->input('districtId');

    $district = Indonesia::findDistrict($districtId, ['villages']);
    return response()->json($district->villages);
  }

}