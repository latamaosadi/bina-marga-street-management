<?php

namespace App\Http\Controllers\Condition;

use App\Http\Controllers\Controller;
use App\Condition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ConditionController extends Controller
{
  public function destroy(Request $request, $id)
  {
    try {
      $condition = Condition::find($id);
      $condition->delete();
    } catch (Exception $e) {
      return $e->getMessage();
    }

    return response()->json($condition);
  }

}