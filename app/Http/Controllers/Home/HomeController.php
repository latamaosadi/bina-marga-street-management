<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Street;
use Illuminate\Http\Request;
use PDF;
use Indonesia;

class HomeController extends Controller
{
  // Home controller to view the home page
  public function index()
  {
    $kabupaten = config('app.kabupaten');
    $streets = Street::all();
    $kecamatans = Indonesia::findCity($kabupaten);
    $vars = compact(
      'streets',
      'kecamatans'
    );
    return view('home.index', $vars);
  }

  public function streetDetail(Request $request, $id)
  {
    $take = config('app.pagination.street.take');
    $street = Street::find($id);
    $images = $street->images()->paginate($take);
    $videos = $street->videos;
    $conditions = $street->conditions;//()->paginate($take);
    $technicals = $street->technicals;//()->paginate($take);

    $street->base_coordinate_section = sprintf('%s, %s', $street->base_section_latitude, $street->base_section_longitude);
    $street->end_coordinate_section = sprintf('%s, %s', $street->end_section_latitude, $street->end_section_longitude);

    $path = [];
    $rawCoordinates = explode(';', $street->polyline);
    foreach ($rawCoordinates as $key => $coordinate) {
      $explodedCoordinate = explode(',', $coordinate);
      array_push($path, (object)[
        'lat' => (float)$explodedCoordinate[0],
        'lng' => (float)$explodedCoordinate[1],
      ]);
    }
    $street->polyline = json_encode($path);

    $vars = compact(
      'street',
      'images',
      'videos',
      'conditions',
      'technicals'
    );

    switch ($request->query('view', null)) {
      case 'pdf':
        $pdf = PDF::loadView('street.pdf', $vars);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->download(sprintf('%s.pdf', $street->name));
        // return $pdf->stream();
        break;
      
      case 'print':
        return view('street.print', $vars);
        break;

      default:
        return view('home.street', $vars);
        break;
    }

  }

  public function bridgeDetail(Request $request, $id)
  {
    $take = config('app.pagination.street.take');
    $bridge = Bridge::find($id);
    $images = $bridge->images()->paginate($take);
    $details = $bridge->details;//()->paginate($take);

    $vars = compact(
      'bridge',
      'images',
      'details'
    );

    switch ($request->query('view', null)) {
      case 'pdf':
        $pdf = PDF::loadView('street.pdf', $vars);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->download(sprintf('%s.pdf', $street->name));
        // return $pdf->stream();
        break;
      
      case 'print':
        return view('street.print', $vars);
        break;

      default:
        return view('home.street', $vars);
        break;
    }

  }

}
