<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StreetVideo extends Model
{
    public function street()
    {
        return $this->belongsTo('App\Street');
    }

    protected $fillable = [
        'street_id',
        'title',
        'video_url',
    ];
}
