<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    public function street()
    {
        return $this->belongsTo('App\Street');
    }

    protected $fillable = [
        'street_id',
        'name',
        'filename',
    ];
}
