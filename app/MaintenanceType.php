<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaintenanceType extends Model
{
    public function details()
    {
        return $this->hasMany('App\Detail');
    }
}
