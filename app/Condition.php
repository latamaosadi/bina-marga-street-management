<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    // Street condition relationship
    public function street()
    {
        return $this->belongsTo('App\Street');
    }

    protected $fillable = [
        'street_id',
        'year',
        'light',
        'heavy',
        'average',
        'good',
    ];
}
