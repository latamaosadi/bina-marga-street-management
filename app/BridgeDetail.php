<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BridgeDetail extends Model
{
    //
    public function bridge()
    {
        return $this->belongsTo('App\Bridge');
    }

    protected $fillable = [
        'bridge_id',
        'year',
        'length',
        'width',
        'bent_amount',
        'top_type',
        'top_condition',
        'floor_type',
        'floor_material',
        'floor_condition',
        'support_type',
        'support_material',
        'support_condition',
        'foundation_type',
        'foundation_material',
        'foundation_condition',
        'head_type',
        'head_material',
        'head_condition',
        'pillar_type',
        'pillar_material',
        'pillar_condition',
        'notes',
    ];
}
