<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObjectType extends Model
{
    // Street bridges relationship
    public function streets()
    {
        return $this->belongsToMany('App\Street', 'street_objects', 'object_type_id', 'street_id');
    }

    protected $fillable = [
        'name',
        'street_id',
        'object_type_id',
        'latitude',
        'longitude',
    ];
}
