<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bridge extends Model
{
    // Street condition relationship
    public function street()
    {
        return $this->belongsTo('App\Street');
    }

    public function images()
    {
        return $this->hasMany('App\BridgeImage');
    }

    public function details()
    {
        return $this->hasMany('App\BridgeDetail');
    }

    protected $fillable = [
        'name',
        'description',
        'street_id',
        'latitude',
        'longitude',
    ];
}
