<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BridgeImage extends Model
{
    //
    public function bridge()
    {
        return $this->belongsTo('App\Bridge');
    }
}
