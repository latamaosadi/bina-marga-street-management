<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Street extends Model
{
    // Street condition relationship
    public function conditions()
    {
        return $this->hasMany('App\Condition');
    }

    // Street condition relationship
    public function technicals()
    {
        return $this->hasMany('App\Technical');
    }

    public function village()
    {
        return $this->belongsTo('Laravolt\Indonesia\Models\Village', 'kelurahan_id');
    }

    public function district()
    {
        return $this->belongsTo('Laravolt\Indonesia\Models\District', 'kecamatan_id');
    }

    public function images()
    {
        return $this->hasMany('App\Image');
    }

    public function avatar()
    {
        return $this->belongsTo('App\Image', 'avatar_id');
    }

    public function cover()
    {
        return $this->belongsTo('App\Image', 'cover_id');
    }

    public function bridges()
    {
        return $this->hasMany('App\Bridge');
    }

    public function videos()
    {
        return $this->hasMany('App\StreetVideo');
    }

    protected $fillable = [
        'name',
        'section_number',
        'base_section_name',
        'end_section_name',
        'validity_year',
        'polyline',
        'base_section_latitude',
        'base_section_longitude',
        'end_section_latitude',
        'end_section_longitude',
        'google_base_street_name',
        'google_end_street_name',
        'google_middle_street_name',
    ];
}
