<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $users = [
            [
                'name' => 'John Doe',
                'username' => 'johndoe',
                'password' => '123456',
                'email' => 'johndoe@mail.com',
            ],
        ];

        foreach ($users as $key => $user) {
            $newUser = new App\User;
            $newUser->name = $user['name'];
            $newUser->username = $user['username'];
            $newUser->email = $user['email'];
            $newUser->password = Hash::make($user['password']);

            $newUser->save();
        }
    }
}
