<?php

use Illuminate\Database\Seeder;

class BridgeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $bridgeData = [
          [
            'name' => 'kutsel 1',
            'description' => 'kutsel 1',
            'latitude' => -8.791100009470718,
            'longitude' => 115.1544029443064,
          ],
          [
            'name' => 'kutsel 2',
            'description' => 'kutsel 2',
            'latitude' => -8.781505360587394,
            'longitude' => 115.1646235758045,
          ],
          [
            'name' => 'kutsel 3',
            'description' => 'kutsel 3',
            'latitude' => -8.789532116925493,
            'longitude' => 115.1723834149252,
          ],
          [
            'name' => 'kutsel 4',
            'description' => 'kutsel 4',
            'latitude' => -8.79990558539117,
            'longitude' => 115.1668896980852,
          ],
          [
            'name' => 'kutsel 5',
            'description' => 'kutsel 5',
            'latitude' => -8.800091837731914,
            'longitude' => 115.1696101981144,
          ],
          [
            'name' => 'kutsel 6',
            'description' => 'kutsel 6',
            'latitude' => -8.816809317618512,
            'longitude' => 115.0997981513852,
          ],
          [
            'name' => 'kutsel 7',
            'description' => 'kutsel 7',
            'latitude' => -8.812087590718676,
            'longitude' => 115.1034839731516,
          ],
          [
            'name' => 'kutsel 8',
            'description' => 'kutsel 8',
            'latitude' => -8.837843401071478,
            'longitude' => 115.1545738257314,
          ],
          [
            'name' => 'kutsel 9',
            'description' => 'kutsel 9',
            'latitude' => -8.835151043927498,
            'longitude' => 115.1570872061182,
          ],
          [
            'name' => 'kutsel 10',
            'description' => 'kutsel 10',
            'latitude' => -8.83757158100285,
            'longitude' => 115.1704321740526,
          ],
          [
            'name' => 'kutsel 11',
            'description' => 'kutsel 11',
            'latitude' => -8.826765162710217,
            'longitude' => 115.2152945771321,
          ],
          [
            'name' => 'kutsel 12',
            'description' => 'kutsel 12',
            'latitude' => -8.820314262661341,
            'longitude' => 115.2176967724615,
          ],
          [
            'name' => 'kutsel 13',
            'description' => 'kutsel 13',
            'latitude' => -8.823605686979695,
            'longitude' => 115.2166488412949,
          ],
          [
            'name' => 'kuta 1',
            'description' => 'kuta 1',
            'latitude' => -8.724739622540415,
            'longitude' => 115.1785891400916,
          ],
          [
            'name' => 'kuta 2',
            'description' => 'kuta 2',
            'latitude' => -8.71001665657915,
            'longitude' => 115.1777049660696,
          ],
          [
            'name' => 'kuta 3',
            'description' => 'kuta 3',
            'latitude' => -8.710291266061738,
            'longitude' => 115.1810116094873,
          ],
          [
            'name' => 'kuta 4',
            'description' => 'kuta 4',
            'latitude' => -8.705530326418113,
            'longitude' => 115.1754867246186,
          ],
          [
            'name' => 'kuta 5',
            'description' => 'kuta 5',
            'latitude' => -8.698121390083552,
            'longitude' => 115.1724095638009,
          ],
          [
            'name' => 'kuta 6',
            'description' => 'kuta 6',
            'latitude' => -8.697397850218445,
            'longitude' => 115.1726838753471,
          ],
          [
            'name' => 'kuta 7',
            'description' => 'kuta 7',
            'latitude' => -8.682310608739241,
            'longitude' => 115.1528698215082,
          ],
          [
            'name' => 'kutra 1',
            'description' => 'kutra 1',
            'latitude' => -8.672257170495392,
            'longitude' => 115.1710032357945,
          ],
          [
            'name' => 'kutra 2',
            'description' => 'kutra 2',
            'latitude' => -8.66218459119287,
            'longitude' => 115.1661427591709,
          ],
          [
            'name' => 'kutra 3',
            'description' => 'kutra 3',
            'latitude' => -8.667205323603993,
            'longitude' => 115.1633447622993,
          ],
          [
            'name' => 'kutra 4',
            'description' => 'kutra 4',
            'latitude' => -8.670910875620333,
            'longitude' => 115.1464025589861,
          ],
          [
            'name' => 'kutra 5',
            'description' => 'kutra 5',
            'latitude' => -8.655072901674199,
            'longitude' => 115.1641542314392,
          ],
          [
            'name' => 'kutra 6',
            'description' => 'kutra 6',
            'latitude' => -8.640139794435347,
            'longitude' => 115.1708771095328,
          ],
          [
            'name' => 'kutra 7',
            'description' => 'kutra 7',
            'latitude' => -8.632792193308246,
            'longitude' => 115.1602438163101,
          ],
          [
            'name' => 'mengwi 1',
            'description' => 'mengwi 1',
            'latitude' => -8.646151152397305,
            'longitude' => 115.116569259569,
          ],
          [
            'name' => 'mnegwi 2',
            'description' => 'mnegwi 2',
            'latitude' => -8.640786759558864,
            'longitude' => 115.1321931551714,
          ],
          [
            'name' => 'mengwi 3',
            'description' => 'mengwi 3',
            'latitude' => -8.625190981566632,
            'longitude' => 115.1429334117707,
          ],
          [
            'name' => 'mengwi 4',
            'description' => 'mengwi 4',
            'latitude' => -8.61432787237211,
            'longitude' => 115.1491150715216,
          ],
          [
            'name' => 'mengwi 5',
            'description' => 'mengwi 5',
            'latitude' => -8.608020786355716,
            'longitude' => 115.1565117071313,
          ],
          [
            'name' => 'mengwi 6',
            'description' => 'mengwi 6',
            'latitude' => -8.603911327225312,
            'longitude' => 115.1597807154618,
          ],
          [
            'name' => 'mengwi 7',
            'description' => 'mengwi 7',
            'latitude' => -8.606198421180826,
            'longitude' => 115.1895001921015,
          ],
          [
            'name' => 'mengwi 8',
            'description' => 'mengwi 8',
            'latitude' => -8.582787519301892,
            'longitude' => 115.1895935486133,
          ],
          [
            'name' => 'mengwi 9',
            'description' => 'mengwi 9',
            'latitude' => -8.58482917999417,
            'longitude' => 115.1955791307586,
          ],
          [
            'name' => 'mengwi 10',
            'description' => 'mengwi 10',
            'latitude' => -8.57748516464682,
            'longitude' => 115.1740103330552,
          ],
          [
            'name' => 'mengwi 11',
            'description' => 'mengwi 11',
            'latitude' => -8.578215681695614,
            'longitude' => 115.1690935630729,
          ],
          [
            'name' => 'mengwi 12',
            'description' => 'mengwi 12',
            'latitude' => -8.565133906880483,
            'longitude' => 115.1764784149522,
          ],
          [
            'name' => 'mengwi 13',
            'description' => 'mengwi 13',
            'latitude' => -8.574359087744508,
            'longitude' => 115.1944369600574,
          ],
          [
            'name' => 'mengwi 14',
            'description' => 'mengwi 14',
            'latitude' => -8.545564731536215,
            'longitude' => 115.1813562109664,
          ],
          [
            'name' => 'mengwi 15',
            'description' => 'mengwi 15',
            'latitude' => -8.53907335754544,
            'longitude' => 115.1850124933482,
          ],
          [
            'name' => 'mengwi 16',
            'description' => 'mengwi 16',
            'latitude' => -8.547511574485307,
            'longitude' => 115.190288091478,
          ],
          [
            'name' => 'mengwi 17',
            'description' => 'mengwi 17',
            'latitude' => -8.538928532008686,
            'longitude' => 115.1940231767945,
          ],
          [
            'name' => 'mengwi 18',
            'description' => 'mengwi 18',
            'latitude' => -8.522410405370435,
            'longitude' => 115.1759656191587,
          ],
          [
            'name' => 'mengwi 19',
            'description' => 'mengwi 19',
            'latitude' => -8.522100731729687,
            'longitude' => 115.192389106172,
          ],
          [
            'name' => 'mengwi 20',
            'description' => 'mengwi 20',
            'latitude' => -8.52339395357806,
            'longitude' => 115.1975481183421,
          ],
          [
            'name' => 'petang 1',
            'description' => 'petang 1',
            'latitude' => -8.42912379302122,
            'longitude' => 115.2247153541482,
          ],
          [
            'name' => 'petang 2',
            'description' => 'petang 2',
            'latitude' => -8.39619611728595,
            'longitude' => 115.2204820706653,
          ],
          [
            'name' => 'petang 3',
            'description' => 'petang 3',
            'latitude' => -8.390358594803418,
            'longitude' => 115.2123659067316,
          ],
          [
            'name' => 'petang 4',
            'description' => 'petang 4',
            'latitude' => -8.367009084916491,
            'longitude' => 115.2220082825209,
          ],
          [
            'name' => 'petang 5',
            'description' => 'petang 5',
            'latitude' => -8.356342510295114,
            'longitude' => 115.2253076507348,
          ],
          [
            'name' => 'petang 6',
            'description' => 'petang 6',
            'latitude' => -8.286808523763348,
            'longitude' => 115.2359207237258,
          ],
          [
            'name' => 'petang 7',
            'description' => 'petang 7',
            'latitude' => -8.280768720648206,
            'longitude' => 115.2210343424279,
          ],
          [
            'name' => 'petang 8',
            'description' => 'petang 8',
            'latitude' => -8.28200433944777,
            'longitude' => 115.2164953303209,
          ],
          [
            'name' => 'petang 9',
            'description' => 'petang 9',
            'latitude' => -8.27820811584106,
            'longitude' => 115.209329325791,
          ],
          [
            'name' => 'petang 10',
            'description' => 'petang 10',
            'latitude' => -8.27008109398698,
            'longitude' => 115.2312302236557,
          ],
          [
            'name' => 'petang 11',
            'description' => 'petang 11',
            'latitude' => -8.244231932134916,
            'longitude' => 115.2370884506145,
          ],
          [
            'name' => 'abs 1',
            'description' => 'abs 1',
            'latitude' => -8.57510514078589,
            'longitude' => 115.1961627138076,
          ],
          [
            'name' => 'abs 2',
            'description' => 'abs 2',
            'latitude' => -8.586869712129223,
            'longitude' => 115.2097552758663,
          ],
          [
            'name' => 'abs 3',
            'description' => 'abs 3',
            'latitude' => -8.578044518235075,
            'longitude' => 115.2084387975143,
          ],
          [
            'name' => 'abs 4',
            'description' => 'abs 4',
            'latitude' => -8.568997297081978,
            'longitude' => 115.2047788553445,
          ],
          [
            'name' => 'abs 5',
            'description' => 'abs 5',
            'latitude' => -8.578169722182919,
            'longitude' => 115.2148076740533,
          ],
          [
            'name' => 'abs 6',
            'description' => 'abs 6',
            'latitude' => -8.579793194643344,
            'longitude' => 115.2372542525188,
          ],
          [
            'name' => 'abs 7',
            'description' => 'abs 7',
            'latitude' => -8.528443744281162,
            'longitude' => 115.227078335801,
          ],
          [
            'name' => 'abs 8',
            'description' => 'abs 8',
            'latitude' => -8.523188470059614,
            'longitude' => 115.2197496721625,
          ],
          [
            'name' => 'abs 9',
            'description' => 'abs 9',
            'latitude' => -8.51196172894594,
            'longitude' => 115.2022310660267,
          ],
          [
            'name' => 'abs 10',
            'description' => 'abs 10',
            'latitude' => -8.497067004207091,
            'longitude' => 115.2145395436751,
          ],
          [
            'name' => 'abs 11',
            'description' => 'abs 11',
            'latitude' => -8.454827090260794,
            'longitude' => 115.2298844767404,
          ],
        ];

        foreach ($bridgeData as $key => $bridge) {
            $newStreet = App\Bridge::create($bridge);
        }
    }
}
