<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->call(StreetTypeTableSeeder::class);
        // $this->call(MaintenanceTypeTableSeeder::class);
        $this->call(StreetTableSeeder::class);
        $this->call(BridgeTableSeeder::class);
    }
}
