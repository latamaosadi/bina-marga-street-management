<?php

use Illuminate\Database\Seeder;
use Laravolt\Indonesia\Models\District;

class StreetDistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $streetDistrictData = array (
            0 => 
            array (
                'id' => 1,
                'section_number' => '1001',
                'kecamatan' => 'Kuta Selatan',
            ),
            1 => 
            array (
                'id' => 2,
                'section_number' => '1002',
                'kecamatan' => 'Kuta Selatan',
            ),
            2 => 
            array (
                'id' => 3,
                'section_number' => '1003',
                'kecamatan' => 'Kuta Selatan',
            ),
            3 => 
            array (
                'id' => 4,
                'section_number' => '1004',
                'kecamatan' => 'Kuta Selatan',
            ),
            4 => 
            array (
                'id' => 5,
                'section_number' => '1005',
                'kecamatan' => 'Kuta Selatan',
            ),
            5 => 
            array (
                'id' => 6,
                'section_number' => '1006',
                'kecamatan' => 'Kuta Selatan',
            ),
            6 => 
            array (
                'id' => 7,
                'section_number' => '1007',
                'kecamatan' => 'Kuta Selatan',
            ),
            7 => 
            array (
                'id' => 8,
                'section_number' => '1008',
                'kecamatan' => 'Kuta Selatan',
            ),
            8 => 
            array (
                'id' => 9,
                'section_number' => '1009',
                'kecamatan' => 'Kuta Selatan',
            ),
            9 => 
            array (
                'id' => 10,
                'section_number' => '1010',
                'kecamatan' => 'Kuta Selatan',
            ),
            10 => 
            array (
                'id' => 11,
                'section_number' => '1011',
                'kecamatan' => 'Kuta Selatan',
            ),
            11 => 
            array (
                'id' => 12,
                'section_number' => '1012',
                'kecamatan' => 'Kuta Selatan',
            ),
            12 => 
            array (
                'id' => 13,
                'section_number' => '1013',
                'kecamatan' => 'Kuta Selatan',
            ),
            13 => 
            array (
                'id' => 14,
                'section_number' => '1014',
                'kecamatan' => 'Kuta Selatan',
            ),
            14 => 
            array (
                'id' => 15,
                'section_number' => '1015',
                'kecamatan' => 'Kuta Selatan',
            ),
            15 => 
            array (
                'id' => 16,
                'section_number' => '1016',
                'kecamatan' => 'Kuta Selatan',
            ),
            16 => 
            array (
                'id' => 17,
                'section_number' => '1017',
                'kecamatan' => 'Kuta Selatan',
            ),
            17 => 
            array (
                'id' => 18,
                'section_number' => '1018',
                'kecamatan' => 'Kuta Selatan',
            ),
            18 => 
            array (
                'id' => 19,
                'section_number' => '1019',
                'kecamatan' => 'Kuta Selatan',
            ),
            19 => 
            array (
                'id' => 20,
                'section_number' => '1020',
                'kecamatan' => 'Kuta Selatan',
            ),
            20 => 
            array (
                'id' => 21,
                'section_number' => '1021',
                'kecamatan' => 'Kuta Selatan',
            ),
            21 => 
            array (
                'id' => 22,
                'section_number' => '1022',
                'kecamatan' => 'Kuta Selatan',
            ),
            22 => 
            array (
                'id' => 23,
                'section_number' => '1023',
                'kecamatan' => 'Kuta Selatan',
            ),
            23 => 
            array (
                'id' => 24,
                'section_number' => '1024',
                'kecamatan' => 'Kuta Selatan',
            ),
            24 => 
            array (
                'id' => 25,
                'section_number' => '1025',
                'kecamatan' => 'Kuta Selatan',
            ),
            25 => 
            array (
                'id' => 26,
                'section_number' => '1026',
                'kecamatan' => 'Kuta Selatan',
            ),
            26 => 
            array (
                'id' => 27,
                'section_number' => '1027',
                'kecamatan' => 'Kuta Selatan',
            ),
            27 => 
            array (
                'id' => 28,
                'section_number' => '1028',
                'kecamatan' => 'Kuta Selatan',
            ),
            28 => 
            array (
                'id' => 29,
                'section_number' => '1029',
                'kecamatan' => 'Kuta Selatan',
            ),
            29 => 
            array (
                'id' => 30,
                'section_number' => '1030',
                'kecamatan' => 'Kuta Selatan',
            ),
            30 => 
            array (
                'id' => 31,
                'section_number' => '1031',
                'kecamatan' => 'Kuta Selatan',
            ),
            31 => 
            array (
                'id' => 32,
                'section_number' => '1032',
                'kecamatan' => 'Kuta Selatan',
            ),
            32 => 
            array (
                'id' => 33,
                'section_number' => '1033',
                'kecamatan' => 'Kuta Selatan',
            ),
            33 => 
            array (
                'id' => 34,
                'section_number' => '1034',
                'kecamatan' => 'Kuta Selatan',
            ),
            34 => 
            array (
                'id' => 35,
                'section_number' => '1035',
                'kecamatan' => 'Kuta Selatan',
            ),
            35 => 
            array (
                'id' => 36,
                'section_number' => '1036',
                'kecamatan' => 'Kuta Selatan',
            ),
            36 => 
            array (
                'id' => 37,
                'section_number' => '1037',
                'kecamatan' => 'Kuta Selatan',
            ),
            37 => 
            array (
                'id' => 38,
                'section_number' => '1038',
                'kecamatan' => 'Kuta Selatan',
            ),
            38 => 
            array (
                'id' => 39,
                'section_number' => '1039',
                'kecamatan' => 'Kuta Selatan',
            ),
            39 => 
            array (
                'id' => 40,
                'section_number' => '1040',
                'kecamatan' => 'Kuta Selatan',
            ),
            40 => 
            array (
                'id' => 41,
                'section_number' => '1041',
                'kecamatan' => 'Kuta Selatan',
            ),
            41 => 
            array (
                'id' => 42,
                'section_number' => '1042',
                'kecamatan' => 'Kuta Selatan',
            ),
            42 => 
            array (
                'id' => 43,
                'section_number' => '1043',
                'kecamatan' => 'Kuta Selatan',
            ),
            43 => 
            array (
                'id' => 44,
                'section_number' => '1044',
                'kecamatan' => 'Kuta Selatan',
            ),
            44 => 
            array (
                'id' => 45,
                'section_number' => '1045',
                'kecamatan' => 'Kuta Selatan',
            ),
            45 => 
            array (
                'id' => 46,
                'section_number' => '1046',
                'kecamatan' => 'Kuta Selatan',
            ),
            46 => 
            array (
                'id' => 47,
                'section_number' => '1047',
                'kecamatan' => 'Kuta Selatan',
            ),
            47 => 
            array (
                'id' => 48,
                'section_number' => '1048',
                'kecamatan' => 'Kuta Selatan',
            ),
            48 => 
            array (
                'id' => 49,
                'section_number' => '1049',
                'kecamatan' => 'Kuta Selatan',
            ),
            49 => 
            array (
                'id' => 50,
                'section_number' => '1050',
                'kecamatan' => 'Kuta Selatan',
            ),
            50 => 
            array (
                'id' => 51,
                'section_number' => '1051',
                'kecamatan' => 'Kuta Selatan',
            ),
            51 => 
            array (
                'id' => 52,
                'section_number' => '1052',
                'kecamatan' => 'Kuta Selatan',
            ),
            52 => 
            array (
                'id' => 53,
                'section_number' => '1053',
                'kecamatan' => 'Kuta Selatan',
            ),
            53 => 
            array (
                'id' => 54,
                'section_number' => '1054',
                'kecamatan' => 'Kuta Selatan',
            ),
            54 => 
            array (
                'id' => 55,
                'section_number' => '1055',
                'kecamatan' => 'Kuta Selatan',
            ),
            55 => 
            array (
                'id' => 56,
                'section_number' => '1056',
                'kecamatan' => 'Kuta Selatan',
            ),
            56 => 
            array (
                'id' => 57,
                'section_number' => '1057',
                'kecamatan' => 'Kuta Selatan',
            ),
            57 => 
            array (
                'id' => 58,
                'section_number' => '1058',
                'kecamatan' => 'Kuta Selatan',
            ),
            58 => 
            array (
                'id' => 59,
                'section_number' => '1059',
                'kecamatan' => 'Kuta Selatan',
            ),
            59 => 
            array (
                'id' => 60,
                'section_number' => '1060',
                'kecamatan' => 'Kuta Selatan',
            ),
            60 => 
            array (
                'id' => 61,
                'section_number' => '1061',
                'kecamatan' => 'Kuta Selatan',
            ),
            61 => 
            array (
                'id' => 62,
                'section_number' => '1062',
                'kecamatan' => 'Kuta Selatan',
            ),
            62 => 
            array (
                'id' => 63,
                'section_number' => '1063',
                'kecamatan' => 'Kuta Selatan',
            ),
            63 => 
            array (
                'id' => 64,
                'section_number' => '1064',
                'kecamatan' => 'Kuta Selatan',
            ),
            64 => 
            array (
                'id' => 65,
                'section_number' => '2001',
                'kecamatan' => 'Kuta',
            ),
            65 => 
            array (
                'id' => 66,
                'section_number' => '2002',
                'kecamatan' => 'Kuta',
            ),
            66 => 
            array (
                'id' => 67,
                'section_number' => '2003',
                'kecamatan' => 'Kuta',
            ),
            67 => 
            array (
                'id' => 68,
                'section_number' => '2004',
                'kecamatan' => 'Kuta',
            ),
            68 => 
            array (
                'id' => 69,
                'section_number' => '2005',
                'kecamatan' => 'Kuta',
            ),
            69 => 
            array (
                'id' => 70,
                'section_number' => '2006',
                'kecamatan' => 'Kuta',
            ),
            70 => 
            array (
                'id' => 71,
                'section_number' => '2007',
                'kecamatan' => 'Kuta',
            ),
            71 => 
            array (
                'id' => 72,
                'section_number' => '2008',
                'kecamatan' => 'Kuta',
            ),
            72 => 
            array (
                'id' => 73,
                'section_number' => '2009',
                'kecamatan' => 'Kuta',
            ),
            73 => 
            array (
                'id' => 74,
                'section_number' => '2010',
                'kecamatan' => 'Kuta',
            ),
            74 => 
            array (
                'id' => 75,
                'section_number' => '2011',
                'kecamatan' => 'Kuta',
            ),
            75 => 
            array (
                'id' => 76,
                'section_number' => '2012',
                'kecamatan' => 'Kuta',
            ),
            76 => 
            array (
                'id' => 77,
                'section_number' => '2013',
                'kecamatan' => 'Kuta',
            ),
            77 => 
            array (
                'id' => 78,
                'section_number' => '2014',
                'kecamatan' => 'Kuta',
            ),
            78 => 
            array (
                'id' => 79,
                'section_number' => '2015',
                'kecamatan' => 'Kuta',
            ),
            79 => 
            array (
                'id' => 80,
                'section_number' => '2016',
                'kecamatan' => 'Kuta',
            ),
            80 => 
            array (
                'id' => 81,
                'section_number' => '2017',
                'kecamatan' => 'Kuta',
            ),
            81 => 
            array (
                'id' => 82,
                'section_number' => '2018',
                'kecamatan' => 'Kuta',
            ),
            82 => 
            array (
                'id' => 83,
                'section_number' => '2019',
                'kecamatan' => 'Kuta',
            ),
            83 => 
            array (
                'id' => 84,
                'section_number' => '2020',
                'kecamatan' => 'Kuta',
            ),
            84 => 
            array (
                'id' => 85,
                'section_number' => '2021',
                'kecamatan' => 'Kuta',
            ),
            85 => 
            array (
                'id' => 86,
                'section_number' => '2022',
                'kecamatan' => 'Kuta',
            ),
            86 => 
            array (
                'id' => 87,
                'section_number' => '2023',
                'kecamatan' => 'Kuta',
            ),
            87 => 
            array (
                'id' => 88,
                'section_number' => '2024',
                'kecamatan' => 'Kuta',
            ),
            88 => 
            array (
                'id' => 89,
                'section_number' => '2025',
                'kecamatan' => 'Kuta',
            ),
            89 => 
            array (
                'id' => 90,
                'section_number' => '2026',
                'kecamatan' => 'Kuta',
            ),
            90 => 
            array (
                'id' => 91,
                'section_number' => '2027',
                'kecamatan' => 'Kuta',
            ),
            91 => 
            array (
                'id' => 92,
                'section_number' => '2028',
                'kecamatan' => 'Kuta',
            ),
            92 => 
            array (
                'id' => 93,
                'section_number' => '2029',
                'kecamatan' => 'Kuta',
            ),
            93 => 
            array (
                'id' => 94,
                'section_number' => '2030',
                'kecamatan' => 'Kuta',
            ),
            94 => 
            array (
                'id' => 95,
                'section_number' => '2031',
                'kecamatan' => 'Kuta',
            ),
            95 => 
            array (
                'id' => 96,
                'section_number' => '2032',
                'kecamatan' => 'Kuta',
            ),
            96 => 
            array (
                'id' => 97,
                'section_number' => '2033',
                'kecamatan' => 'Kuta',
            ),
            97 => 
            array (
                'id' => 98,
                'section_number' => '2034',
                'kecamatan' => 'Kuta',
            ),
            98 => 
            array (
                'id' => 99,
                'section_number' => '2035',
                'kecamatan' => 'Kuta',
            ),
            99 => 
            array (
                'id' => 100,
                'section_number' => '2036',
                'kecamatan' => 'Kuta',
            ),
            100 => 
            array (
                'id' => 101,
                'section_number' => '2037',
                'kecamatan' => 'Kuta',
            ),
            101 => 
            array (
                'id' => 102,
                'section_number' => '2038',
                'kecamatan' => 'Kuta',
            ),
            102 => 
            array (
                'id' => 103,
                'section_number' => '2039',
                'kecamatan' => 'Kuta',
            ),
            103 => 
            array (
                'id' => 104,
                'section_number' => '2040',
                'kecamatan' => 'Kuta',
            ),
            104 => 
            array (
                'id' => 105,
                'section_number' => '2041',
                'kecamatan' => 'Kuta',
            ),
            105 => 
            array (
                'id' => 106,
                'section_number' => '2042',
                'kecamatan' => 'Kuta',
            ),
            106 => 
            array (
                'id' => 107,
                'section_number' => '2043',
                'kecamatan' => 'Kuta',
            ),
            107 => 
            array (
                'id' => 108,
                'section_number' => '2044',
                'kecamatan' => 'Kuta',
            ),
            108 => 
            array (
                'id' => 109,
                'section_number' => '2045',
                'kecamatan' => 'Kuta',
            ),
            109 => 
            array (
                'id' => 110,
                'section_number' => '2046',
                'kecamatan' => 'Kuta',
            ),
            110 => 
            array (
                'id' => 111,
                'section_number' => '2047',
                'kecamatan' => 'Kuta',
            ),
            111 => 
            array (
                'id' => 112,
                'section_number' => '2048',
                'kecamatan' => 'Kuta',
            ),
            112 => 
            array (
                'id' => 113,
                'section_number' => '2049',
                'kecamatan' => 'Kuta',
            ),
            113 => 
            array (
                'id' => 114,
                'section_number' => '2050',
                'kecamatan' => 'Kuta',
            ),
            114 => 
            array (
                'id' => 115,
                'section_number' => '2051',
                'kecamatan' => 'Kuta',
            ),
            115 => 
            array (
                'id' => 116,
                'section_number' => '2052',
                'kecamatan' => 'Kuta',
            ),
            116 => 
            array (
                'id' => 117,
                'section_number' => '2053',
                'kecamatan' => 'Kuta',
            ),
            117 => 
            array (
                'id' => 118,
                'section_number' => '2054',
                'kecamatan' => 'Kuta',
            ),
            118 => 
            array (
                'id' => 119,
                'section_number' => '2055',
                'kecamatan' => 'Kuta',
            ),
            119 => 
            array (
                'id' => 120,
                'section_number' => '2056',
                'kecamatan' => 'Kuta',
            ),
            120 => 
            array (
                'id' => 121,
                'section_number' => '2057',
                'kecamatan' => 'Kuta',
            ),
            121 => 
            array (
                'id' => 122,
                'section_number' => '2058',
                'kecamatan' => 'Kuta',
            ),
            122 => 
            array (
                'id' => 123,
                'section_number' => '2059',
                'kecamatan' => 'Kuta',
            ),
            123 => 
            array (
                'id' => 124,
                'section_number' => '2060',
                'kecamatan' => 'Kuta',
            ),
            124 => 
            array (
                'id' => 125,
                'section_number' => '2061',
                'kecamatan' => 'Kuta',
            ),
            125 => 
            array (
                'id' => 126,
                'section_number' => '2062',
                'kecamatan' => 'Kuta',
            ),
            126 => 
            array (
                'id' => 127,
                'section_number' => '2063',
                'kecamatan' => 'Kuta',
            ),
            127 => 
            array (
                'id' => 128,
                'section_number' => '2064',
                'kecamatan' => 'Kuta',
            ),
            128 => 
            array (
                'id' => 129,
                'section_number' => '2065',
                'kecamatan' => 'Kuta',
            ),
            129 => 
            array (
                'id' => 130,
                'section_number' => '2066',
                'kecamatan' => 'Kuta',
            ),
            130 => 
            array (
                'id' => 131,
                'section_number' => '2067',
                'kecamatan' => 'Kuta',
            ),
            131 => 
            array (
                'id' => 132,
                'section_number' => '2068',
                'kecamatan' => 'Kuta',
            ),
            132 => 
            array (
                'id' => 133,
                'section_number' => '2069',
                'kecamatan' => 'Kuta',
            ),
            133 => 
            array (
                'id' => 134,
                'section_number' => '2070',
                'kecamatan' => 'Kuta',
            ),
            134 => 
            array (
                'id' => 135,
                'section_number' => '2071',
                'kecamatan' => 'Kuta',
            ),
            135 => 
            array (
                'id' => 136,
                'section_number' => '2072',
                'kecamatan' => 'Kuta',
            ),
            136 => 
            array (
                'id' => 137,
                'section_number' => '2073',
                'kecamatan' => 'Kuta',
            ),
            137 => 
            array (
                'id' => 138,
                'section_number' => '2074',
                'kecamatan' => 'Kuta',
            ),
            138 => 
            array (
                'id' => 139,
                'section_number' => '2075',
                'kecamatan' => 'Kuta',
            ),
            139 => 
            array (
                'id' => 140,
                'section_number' => '2076',
                'kecamatan' => 'Kuta',
            ),
            140 => 
            array (
                'id' => 141,
                'section_number' => '2077',
                'kecamatan' => 'Kuta',
            ),
            141 => 
            array (
                'id' => 142,
                'section_number' => '2078',
                'kecamatan' => 'Kuta',
            ),
            142 => 
            array (
                'id' => 143,
                'section_number' => '2079',
                'kecamatan' => 'Kuta',
            ),
            143 => 
            array (
                'id' => 144,
                'section_number' => '2080',
                'kecamatan' => 'Kuta',
            ),
            144 => 
            array (
                'id' => 145,
                'section_number' => '2081',
                'kecamatan' => 'Kuta',
            ),
            145 => 
            array (
                'id' => 146,
                'section_number' => '2082',
                'kecamatan' => 'Kuta',
            ),
            146 => 
            array (
                'id' => 147,
                'section_number' => '2083',
                'kecamatan' => 'Kuta',
            ),
            147 => 
            array (
                'id' => 148,
                'section_number' => '2084',
                'kecamatan' => 'Kuta',
            ),
            148 => 
            array (
                'id' => 149,
                'section_number' => '2085',
                'kecamatan' => 'Kuta',
            ),
            149 => 
            array (
                'id' => 150,
                'section_number' => '2086',
                'kecamatan' => 'Kuta',
            ),
            150 => 
            array (
                'id' => 151,
                'section_number' => '2087',
                'kecamatan' => 'Kuta',
            ),
            151 => 
            array (
                'id' => 152,
                'section_number' => '2088',
                'kecamatan' => 'Kuta',
            ),
            152 => 
            array (
                'id' => 153,
                'section_number' => '2089',
                'kecamatan' => 'Kuta',
            ),
            153 => 
            array (
                'id' => 154,
                'section_number' => '2090',
                'kecamatan' => 'Kuta',
            ),
            154 => 
            array (
                'id' => 155,
                'section_number' => '2091',
                'kecamatan' => 'Kuta',
            ),
            155 => 
            array (
                'id' => 156,
                'section_number' => '2092',
                'kecamatan' => 'Kuta',
            ),
            156 => 
            array (
                'id' => 157,
                'section_number' => '2093',
                'kecamatan' => 'Kuta',
            ),
            157 => 
            array (
                'id' => 158,
                'section_number' => '2094',
                'kecamatan' => 'Kuta',
            ),
            158 => 
            array (
                'id' => 159,
                'section_number' => '2095',
                'kecamatan' => 'Kuta',
            ),
            159 => 
            array (
                'id' => 160,
                'section_number' => '2096',
                'kecamatan' => 'Kuta',
            ),
            160 => 
            array (
                'id' => 161,
                'section_number' => '2097',
                'kecamatan' => 'Kuta',
            ),
            161 => 
            array (
                'id' => 162,
                'section_number' => '2098',
                'kecamatan' => 'Kuta',
            ),
            162 => 
            array (
                'id' => 163,
                'section_number' => '2099',
                'kecamatan' => 'Kuta',
            ),
            163 => 
            array (
                'id' => 164,
                'section_number' => '2100',
                'kecamatan' => 'Kuta',
            ),
            164 => 
            array (
                'id' => 165,
                'section_number' => '2101',
                'kecamatan' => 'Kuta',
            ),
            165 => 
            array (
                'id' => 166,
                'section_number' => '3001',
                'kecamatan' => 'Kuta Utara',
            ),
            166 => 
            array (
                'id' => 167,
                'section_number' => '3002',
                'kecamatan' => 'Kuta Utara',
            ),
            167 => 
            array (
                'id' => 168,
                'section_number' => '3003',
                'kecamatan' => 'Kuta Utara',
            ),
            168 => 
            array (
                'id' => 169,
                'section_number' => '3004',
                'kecamatan' => 'Kuta Utara',
            ),
            169 => 
            array (
                'id' => 170,
                'section_number' => '3005',
                'kecamatan' => 'Kuta Utara',
            ),
            170 => 
            array (
                'id' => 171,
                'section_number' => '3006',
                'kecamatan' => 'Kuta Utara',
            ),
            171 => 
            array (
                'id' => 172,
                'section_number' => '3007',
                'kecamatan' => 'Kuta Utara',
            ),
            172 => 
            array (
                'id' => 173,
                'section_number' => '3008',
                'kecamatan' => 'Kuta Utara',
            ),
            173 => 
            array (
                'id' => 174,
                'section_number' => '3009',
                'kecamatan' => 'Kuta Utara',
            ),
            174 => 
            array (
                'id' => 175,
                'section_number' => '3010',
                'kecamatan' => 'Kuta Utara',
            ),
            175 => 
            array (
                'id' => 176,
                'section_number' => '3011',
                'kecamatan' => 'Kuta Utara',
            ),
            176 => 
            array (
                'id' => 177,
                'section_number' => '3012',
                'kecamatan' => 'Kuta Utara',
            ),
            177 => 
            array (
                'id' => 178,
                'section_number' => '3013',
                'kecamatan' => 'Kuta Utara',
            ),
            178 => 
            array (
                'id' => 179,
                'section_number' => '3014',
                'kecamatan' => 'Kuta Utara',
            ),
            179 => 
            array (
                'id' => 180,
                'section_number' => '3015',
                'kecamatan' => 'Kuta Utara',
            ),
            180 => 
            array (
                'id' => 181,
                'section_number' => '3016',
                'kecamatan' => 'Kuta Utara',
            ),
            181 => 
            array (
                'id' => 182,
                'section_number' => '3017',
                'kecamatan' => 'Kuta Utara',
            ),
            182 => 
            array (
                'id' => 183,
                'section_number' => '3018',
                'kecamatan' => 'Kuta Utara',
            ),
            183 => 
            array (
                'id' => 184,
                'section_number' => '3019',
                'kecamatan' => 'Kuta Utara',
            ),
            184 => 
            array (
                'id' => 185,
                'section_number' => '3020',
                'kecamatan' => 'Kuta Utara',
            ),
            185 => 
            array (
                'id' => 186,
                'section_number' => '3021',
                'kecamatan' => 'Kuta Utara',
            ),
            186 => 
            array (
                'id' => 187,
                'section_number' => '3022',
                'kecamatan' => 'Kuta Utara',
            ),
            187 => 
            array (
                'id' => 188,
                'section_number' => '3023',
                'kecamatan' => 'Kuta Utara',
            ),
            188 => 
            array (
                'id' => 189,
                'section_number' => '3024',
                'kecamatan' => 'Kuta Utara',
            ),
            189 => 
            array (
                'id' => 190,
                'section_number' => '3025',
                'kecamatan' => 'Kuta Utara',
            ),
            190 => 
            array (
                'id' => 191,
                'section_number' => '3026',
                'kecamatan' => 'Kuta Utara',
            ),
            191 => 
            array (
                'id' => 192,
                'section_number' => '3027',
                'kecamatan' => 'Kuta Utara',
            ),
            192 => 
            array (
                'id' => 193,
                'section_number' => '3028',
                'kecamatan' => 'Kuta Utara',
            ),
            193 => 
            array (
                'id' => 194,
                'section_number' => '3029',
                'kecamatan' => 'Kuta Utara',
            ),
            194 => 
            array (
                'id' => 195,
                'section_number' => '3030',
                'kecamatan' => 'Kuta Utara',
            ),
            195 => 
            array (
                'id' => 196,
                'section_number' => '3031',
                'kecamatan' => 'Kuta Utara',
            ),
            196 => 
            array (
                'id' => 197,
                'section_number' => '3032',
                'kecamatan' => 'Kuta Utara',
            ),
            197 => 
            array (
                'id' => 198,
                'section_number' => '3033',
                'kecamatan' => 'Kuta Utara',
            ),
            198 => 
            array (
                'id' => 199,
                'section_number' => '3034',
                'kecamatan' => 'Kuta Utara',
            ),
            199 => 
            array (
                'id' => 200,
                'section_number' => '3035',
                'kecamatan' => 'Kuta Utara',
            ),
            200 => 
            array (
                'id' => 201,
                'section_number' => '3036',
                'kecamatan' => 'Kuta Utara',
            ),
            201 => 
            array (
                'id' => 202,
                'section_number' => '3037',
                'kecamatan' => 'Kuta Utara',
            ),
            202 => 
            array (
                'id' => 203,
                'section_number' => '3038',
                'kecamatan' => 'Kuta Utara',
            ),
            203 => 
            array (
                'id' => 204,
                'section_number' => '3039',
                'kecamatan' => 'Kuta Utara',
            ),
            204 => 
            array (
                'id' => 205,
                'section_number' => '3040',
                'kecamatan' => 'Kuta Utara',
            ),
            205 => 
            array (
                'id' => 206,
                'section_number' => '3041',
                'kecamatan' => 'Kuta Utara',
            ),
            206 => 
            array (
                'id' => 207,
                'section_number' => '3042',
                'kecamatan' => 'Kuta Utara',
            ),
            207 => 
            array (
                'id' => 208,
                'section_number' => '3043',
                'kecamatan' => 'Kuta Utara',
            ),
            208 => 
            array (
                'id' => 209,
                'section_number' => '3044',
                'kecamatan' => 'Kuta Utara',
            ),
            209 => 
            array (
                'id' => 210,
                'section_number' => '3045',
                'kecamatan' => 'Kuta Utara',
            ),
            210 => 
            array (
                'id' => 211,
                'section_number' => '3046',
                'kecamatan' => 'Kuta Utara',
            ),
            211 => 
            array (
                'id' => 212,
                'section_number' => '3047',
                'kecamatan' => 'Kuta Utara',
            ),
            212 => 
            array (
                'id' => 213,
                'section_number' => '3048',
                'kecamatan' => 'Kuta Utara',
            ),
            213 => 
            array (
                'id' => 214,
                'section_number' => '3049',
                'kecamatan' => 'Kuta Utara',
            ),
            214 => 
            array (
                'id' => 215,
                'section_number' => '3050',
                'kecamatan' => 'Kuta Utara',
            ),
            215 => 
            array (
                'id' => 216,
                'section_number' => '3051',
                'kecamatan' => 'Kuta Utara',
            ),
            216 => 
            array (
                'id' => 217,
                'section_number' => '3052',
                'kecamatan' => 'Kuta Utara',
            ),
            217 => 
            array (
                'id' => 218,
                'section_number' => '3053',
                'kecamatan' => 'Kuta Utara',
            ),
            218 => 
            array (
                'id' => 219,
                'section_number' => '3054',
                'kecamatan' => 'Kuta Utara',
            ),
            219 => 
            array (
                'id' => 220,
                'section_number' => '3055',
                'kecamatan' => 'Kuta Utara',
            ),
            220 => 
            array (
                'id' => 221,
                'section_number' => '3056',
                'kecamatan' => 'Kuta Utara',
            ),
            221 => 
            array (
                'id' => 222,
                'section_number' => '3057',
                'kecamatan' => 'Kuta Utara',
            ),
            222 => 
            array (
                'id' => 223,
                'section_number' => '3058',
                'kecamatan' => 'Kuta Utara',
            ),
            223 => 
            array (
                'id' => 224,
                'section_number' => '3059',
                'kecamatan' => 'Kuta Utara',
            ),
            224 => 
            array (
                'id' => 225,
                'section_number' => '3060',
                'kecamatan' => 'Kuta Utara',
            ),
            225 => 
            array (
                'id' => 226,
                'section_number' => '3061',
                'kecamatan' => 'Kuta Utara',
            ),
            226 => 
            array (
                'id' => 227,
                'section_number' => '3062',
                'kecamatan' => 'Kuta Utara',
            ),
            227 => 
            array (
                'id' => 228,
                'section_number' => '3063',
                'kecamatan' => 'Kuta Utara',
            ),
            228 => 
            array (
                'id' => 229,
                'section_number' => '3064',
                'kecamatan' => 'Kuta Utara',
            ),
            229 => 
            array (
                'id' => 230,
                'section_number' => '3065',
                'kecamatan' => 'Kuta Utara',
            ),
            230 => 
            array (
                'id' => 231,
                'section_number' => '3066',
                'kecamatan' => 'Kuta Utara',
            ),
            231 => 
            array (
                'id' => 232,
                'section_number' => '3067',
                'kecamatan' => 'Kuta Utara',
            ),
            232 => 
            array (
                'id' => 233,
                'section_number' => '3068',
                'kecamatan' => 'Kuta Utara',
            ),
            233 => 
            array (
                'id' => 234,
                'section_number' => '3069',
                'kecamatan' => 'Kuta Utara',
            ),
            234 => 
            array (
                'id' => 235,
                'section_number' => '3070',
                'kecamatan' => 'Kuta Utara',
            ),
            235 => 
            array (
                'id' => 236,
                'section_number' => '3071',
                'kecamatan' => 'Kuta Utara',
            ),
            236 => 
            array (
                'id' => 237,
                'section_number' => '3072',
                'kecamatan' => 'Kuta Utara',
            ),
            237 => 
            array (
                'id' => 238,
                'section_number' => '3073',
                'kecamatan' => 'Kuta Utara',
            ),
            238 => 
            array (
                'id' => 239,
                'section_number' => '3074',
                'kecamatan' => 'Kuta Utara',
            ),
            239 => 
            array (
                'id' => 240,
                'section_number' => '3075',
                'kecamatan' => 'Kuta Utara',
            ),
            240 => 
            array (
                'id' => 241,
                'section_number' => '3076',
                'kecamatan' => 'Kuta Utara',
            ),
            241 => 
            array (
                'id' => 242,
                'section_number' => '3077',
                'kecamatan' => 'Kuta Utara',
            ),
            242 => 
            array (
                'id' => 243,
                'section_number' => '3078',
                'kecamatan' => 'Kuta Utara',
            ),
            243 => 
            array (
                'id' => 244,
                'section_number' => '3079',
                'kecamatan' => 'Kuta Utara',
            ),
            244 => 
            array (
                'id' => 245,
                'section_number' => '3080',
                'kecamatan' => 'Kuta Utara',
            ),
            245 => 
            array (
                'id' => 246,
                'section_number' => '3081',
                'kecamatan' => 'Kuta Utara',
            ),
            246 => 
            array (
                'id' => 247,
                'section_number' => '3082',
                'kecamatan' => 'Kuta Utara',
            ),
            247 => 
            array (
                'id' => 248,
                'section_number' => '3083',
                'kecamatan' => 'Kuta Utara',
            ),
            248 => 
            array (
                'id' => 249,
                'section_number' => '3084',
                'kecamatan' => 'Kuta Utara',
            ),
            249 => 
            array (
                'id' => 250,
                'section_number' => '3085',
                'kecamatan' => 'Kuta Utara',
            ),
            250 => 
            array (
                'id' => 251,
                'section_number' => '3086',
                'kecamatan' => 'Kuta Utara',
            ),
            251 => 
            array (
                'id' => 252,
                'section_number' => '3087',
                'kecamatan' => 'Kuta Utara',
            ),
            252 => 
            array (
                'id' => 253,
                'section_number' => '3088',
                'kecamatan' => 'Kuta Utara',
            ),
            253 => 
            array (
                'id' => 254,
                'section_number' => '3089',
                'kecamatan' => 'Kuta Utara',
            ),
            254 => 
            array (
                'id' => 255,
                'section_number' => '4001',
                'kecamatan' => 'Mengwi',
            ),
            255 => 
            array (
                'id' => 256,
                'section_number' => '4002',
                'kecamatan' => 'Mengwi',
            ),
            256 => 
            array (
                'id' => 257,
                'section_number' => '4003',
                'kecamatan' => 'Mengwi',
            ),
            257 => 
            array (
                'id' => 258,
                'section_number' => '4004',
                'kecamatan' => 'Mengwi',
            ),
            258 => 
            array (
                'id' => 259,
                'section_number' => '4005',
                'kecamatan' => 'Mengwi',
            ),
            259 => 
            array (
                'id' => 260,
                'section_number' => '4006',
                'kecamatan' => 'Mengwi',
            ),
            260 => 
            array (
                'id' => 261,
                'section_number' => '4007',
                'kecamatan' => 'Mengwi',
            ),
            261 => 
            array (
                'id' => 262,
                'section_number' => '4008',
                'kecamatan' => 'Mengwi',
            ),
            262 => 
            array (
                'id' => 263,
                'section_number' => '4009',
                'kecamatan' => 'Mengwi',
            ),
            263 => 
            array (
                'id' => 264,
                'section_number' => '4010',
                'kecamatan' => 'Mengwi',
            ),
            264 => 
            array (
                'id' => 265,
                'section_number' => '4011',
                'kecamatan' => 'Mengwi',
            ),
            265 => 
            array (
                'id' => 266,
                'section_number' => '4012',
                'kecamatan' => 'Mengwi',
            ),
            266 => 
            array (
                'id' => 267,
                'section_number' => '4013',
                'kecamatan' => 'Mengwi',
            ),
            267 => 
            array (
                'id' => 268,
                'section_number' => '4014',
                'kecamatan' => 'Mengwi',
            ),
            268 => 
            array (
                'id' => 269,
                'section_number' => '4015',
                'kecamatan' => 'Mengwi',
            ),
            269 => 
            array (
                'id' => 270,
                'section_number' => '4016',
                'kecamatan' => 'Mengwi',
            ),
            270 => 
            array (
                'id' => 271,
                'section_number' => '4017',
                'kecamatan' => 'Mengwi',
            ),
            271 => 
            array (
                'id' => 272,
                'section_number' => '4018',
                'kecamatan' => 'Mengwi',
            ),
            272 => 
            array (
                'id' => 273,
                'section_number' => '4019',
                'kecamatan' => 'Mengwi',
            ),
            273 => 
            array (
                'id' => 274,
                'section_number' => '4020',
                'kecamatan' => 'Mengwi',
            ),
            274 => 
            array (
                'id' => 275,
                'section_number' => '4021',
                'kecamatan' => 'Mengwi',
            ),
            275 => 
            array (
                'id' => 276,
                'section_number' => '4022',
                'kecamatan' => 'Mengwi',
            ),
            276 => 
            array (
                'id' => 277,
                'section_number' => '4023',
                'kecamatan' => 'Mengwi',
            ),
            277 => 
            array (
                'id' => 278,
                'section_number' => '4024',
                'kecamatan' => 'Mengwi',
            ),
            278 => 
            array (
                'id' => 279,
                'section_number' => '4025',
                'kecamatan' => 'Mengwi',
            ),
            279 => 
            array (
                'id' => 280,
                'section_number' => '4026',
                'kecamatan' => 'Mengwi',
            ),
            280 => 
            array (
                'id' => 281,
                'section_number' => '4027',
                'kecamatan' => 'Mengwi',
            ),
            281 => 
            array (
                'id' => 282,
                'section_number' => '4028',
                'kecamatan' => 'Mengwi',
            ),
            282 => 
            array (
                'id' => 283,
                'section_number' => '4029',
                'kecamatan' => 'Mengwi',
            ),
            283 => 
            array (
                'id' => 284,
                'section_number' => '4030',
                'kecamatan' => 'Mengwi',
            ),
            284 => 
            array (
                'id' => 285,
                'section_number' => '4031',
                'kecamatan' => 'Mengwi',
            ),
            285 => 
            array (
                'id' => 286,
                'section_number' => '4032',
                'kecamatan' => 'Mengwi',
            ),
            286 => 
            array (
                'id' => 287,
                'section_number' => '4033',
                'kecamatan' => 'Mengwi',
            ),
            287 => 
            array (
                'id' => 288,
                'section_number' => '4034',
                'kecamatan' => 'Mengwi',
            ),
            288 => 
            array (
                'id' => 289,
                'section_number' => '4035',
                'kecamatan' => 'Mengwi',
            ),
            289 => 
            array (
                'id' => 290,
                'section_number' => '4036',
                'kecamatan' => 'Mengwi',
            ),
            290 => 
            array (
                'id' => 291,
                'section_number' => '4037',
                'kecamatan' => 'Mengwi',
            ),
            291 => 
            array (
                'id' => 292,
                'section_number' => '4038',
                'kecamatan' => 'Mengwi',
            ),
            292 => 
            array (
                'id' => 293,
                'section_number' => '4039',
                'kecamatan' => 'Mengwi',
            ),
            293 => 
            array (
                'id' => 294,
                'section_number' => '4040',
                'kecamatan' => 'Mengwi',
            ),
            294 => 
            array (
                'id' => 295,
                'section_number' => '4041',
                'kecamatan' => 'Mengwi',
            ),
            295 => 
            array (
                'id' => 296,
                'section_number' => '4042',
                'kecamatan' => 'Mengwi',
            ),
            296 => 
            array (
                'id' => 297,
                'section_number' => '4043',
                'kecamatan' => 'Mengwi',
            ),
            297 => 
            array (
                'id' => 298,
                'section_number' => '4044',
                'kecamatan' => 'Mengwi',
            ),
            298 => 
            array (
                'id' => 299,
                'section_number' => '4045',
                'kecamatan' => 'Mengwi',
            ),
            299 => 
            array (
                'id' => 300,
                'section_number' => '4046',
                'kecamatan' => 'Mengwi',
            ),
            300 => 
            array (
                'id' => 301,
                'section_number' => '4047',
                'kecamatan' => 'Mengwi',
            ),
            301 => 
            array (
                'id' => 302,
                'section_number' => '4048',
                'kecamatan' => 'Mengwi',
            ),
            302 => 
            array (
                'id' => 303,
                'section_number' => '4049',
                'kecamatan' => 'Mengwi',
            ),
            303 => 
            array (
                'id' => 304,
                'section_number' => '4050',
                'kecamatan' => 'Mengwi',
            ),
            304 => 
            array (
                'id' => 305,
                'section_number' => '4051',
                'kecamatan' => 'Mengwi',
            ),
            305 => 
            array (
                'id' => 306,
                'section_number' => '4052',
                'kecamatan' => 'Mengwi',
            ),
            306 => 
            array (
                'id' => 307,
                'section_number' => '4053',
                'kecamatan' => 'Mengwi',
            ),
            307 => 
            array (
                'id' => 308,
                'section_number' => '4054',
                'kecamatan' => 'Mengwi',
            ),
            308 => 
            array (
                'id' => 309,
                'section_number' => '4055',
                'kecamatan' => 'Mengwi',
            ),
            309 => 
            array (
                'id' => 310,
                'section_number' => '4056',
                'kecamatan' => 'Mengwi',
            ),
            310 => 
            array (
                'id' => 311,
                'section_number' => '4057',
                'kecamatan' => 'Mengwi',
            ),
            311 => 
            array (
                'id' => 312,
                'section_number' => '4058',
                'kecamatan' => 'Mengwi',
            ),
            312 => 
            array (
                'id' => 313,
                'section_number' => '4059',
                'kecamatan' => 'Mengwi',
            ),
            313 => 
            array (
                'id' => 314,
                'section_number' => '4060',
                'kecamatan' => 'Mengwi',
            ),
            314 => 
            array (
                'id' => 315,
                'section_number' => '4061',
                'kecamatan' => 'Mengwi',
            ),
            315 => 
            array (
                'id' => 316,
                'section_number' => '4062',
                'kecamatan' => 'Mengwi',
            ),
            316 => 
            array (
                'id' => 317,
                'section_number' => '4063',
                'kecamatan' => 'Mengwi',
            ),
            317 => 
            array (
                'id' => 318,
                'section_number' => '4064',
                'kecamatan' => 'Mengwi',
            ),
            318 => 
            array (
                'id' => 319,
                'section_number' => '4065',
                'kecamatan' => 'Mengwi',
            ),
            319 => 
            array (
                'id' => 320,
                'section_number' => '4066',
                'kecamatan' => 'Mengwi',
            ),
            320 => 
            array (
                'id' => 321,
                'section_number' => '4067',
                'kecamatan' => 'Mengwi',
            ),
            321 => 
            array (
                'id' => 322,
                'section_number' => '4068',
                'kecamatan' => 'Mengwi',
            ),
            322 => 
            array (
                'id' => 323,
                'section_number' => '4069',
                'kecamatan' => 'Mengwi',
            ),
            323 => 
            array (
                'id' => 324,
                'section_number' => '4070',
                'kecamatan' => 'Mengwi',
            ),
            324 => 
            array (
                'id' => 325,
                'section_number' => '4071',
                'kecamatan' => 'Mengwi',
            ),
            325 => 
            array (
                'id' => 326,
                'section_number' => '4072',
                'kecamatan' => 'Mengwi',
            ),
            326 => 
            array (
                'id' => 327,
                'section_number' => '4073',
                'kecamatan' => 'Mengwi',
            ),
            327 => 
            array (
                'id' => 328,
                'section_number' => '4074',
                'kecamatan' => 'Mengwi',
            ),
            328 => 
            array (
                'id' => 329,
                'section_number' => '4075',
                'kecamatan' => 'Mengwi',
            ),
            329 => 
            array (
                'id' => 330,
                'section_number' => '4076',
                'kecamatan' => 'Mengwi',
            ),
            330 => 
            array (
                'id' => 331,
                'section_number' => '4077',
                'kecamatan' => 'Mengwi',
            ),
            331 => 
            array (
                'id' => 332,
                'section_number' => '4078',
                'kecamatan' => 'Mengwi',
            ),
            332 => 
            array (
                'id' => 333,
                'section_number' => '4079',
                'kecamatan' => 'Mengwi',
            ),
            333 => 
            array (
                'id' => 334,
                'section_number' => '4080',
                'kecamatan' => 'Mengwi',
            ),
            334 => 
            array (
                'id' => 335,
                'section_number' => '4081',
                'kecamatan' => 'Mengwi',
            ),
            335 => 
            array (
                'id' => 336,
                'section_number' => '4082',
                'kecamatan' => 'Mengwi',
            ),
            336 => 
            array (
                'id' => 337,
                'section_number' => '4083',
                'kecamatan' => 'Mengwi',
            ),
            337 => 
            array (
                'id' => 338,
                'section_number' => '4084',
                'kecamatan' => 'Mengwi',
            ),
            338 => 
            array (
                'id' => 339,
                'section_number' => '4085',
                'kecamatan' => 'Mengwi',
            ),
            339 => 
            array (
                'id' => 340,
                'section_number' => '4086',
                'kecamatan' => 'Mengwi',
            ),
            340 => 
            array (
                'id' => 341,
                'section_number' => '4087',
                'kecamatan' => 'Mengwi',
            ),
            341 => 
            array (
                'id' => 342,
                'section_number' => '4088',
                'kecamatan' => 'Mengwi',
            ),
            342 => 
            array (
                'id' => 343,
                'section_number' => '4089',
                'kecamatan' => 'Mengwi',
            ),
            343 => 
            array (
                'id' => 344,
                'section_number' => '4090',
                'kecamatan' => 'Mengwi',
            ),
            344 => 
            array (
                'id' => 345,
                'section_number' => '4091',
                'kecamatan' => 'Mengwi',
            ),
            345 => 
            array (
                'id' => 346,
                'section_number' => '4092',
                'kecamatan' => 'Mengwi',
            ),
            346 => 
            array (
                'id' => 347,
                'section_number' => '4093',
                'kecamatan' => 'Mengwi',
            ),
            347 => 
            array (
                'id' => 348,
                'section_number' => '4094',
                'kecamatan' => 'Mengwi',
            ),
            348 => 
            array (
                'id' => 349,
                'section_number' => '4095',
                'kecamatan' => 'Mengwi',
            ),
            349 => 
            array (
                'id' => 350,
                'section_number' => '4096',
                'kecamatan' => 'Mengwi',
            ),
            350 => 
            array (
                'id' => 351,
                'section_number' => '4097',
                'kecamatan' => 'Mengwi',
            ),
            351 => 
            array (
                'id' => 352,
                'section_number' => '4098',
                'kecamatan' => 'Mengwi',
            ),
            352 => 
            array (
                'id' => 353,
                'section_number' => '4099',
                'kecamatan' => 'Mengwi',
            ),
            353 => 
            array (
                'id' => 354,
                'section_number' => '4100',
                'kecamatan' => 'Mengwi',
            ),
            354 => 
            array (
                'id' => 355,
                'section_number' => '4101',
                'kecamatan' => 'Mengwi',
            ),
            355 => 
            array (
                'id' => 356,
                'section_number' => '4102',
                'kecamatan' => 'Mengwi',
            ),
            356 => 
            array (
                'id' => 357,
                'section_number' => '4103',
                'kecamatan' => 'Mengwi',
            ),
            357 => 
            array (
                'id' => 358,
                'section_number' => '4104',
                'kecamatan' => 'Mengwi',
            ),
            358 => 
            array (
                'id' => 359,
                'section_number' => '4105',
                'kecamatan' => 'Mengwi',
            ),
            359 => 
            array (
                'id' => 360,
                'section_number' => '4106',
                'kecamatan' => 'Mengwi',
            ),
            360 => 
            array (
                'id' => 361,
                'section_number' => '4107',
                'kecamatan' => 'Mengwi',
            ),
            361 => 
            array (
                'id' => 362,
                'section_number' => '4108',
                'kecamatan' => 'Mengwi',
            ),
            362 => 
            array (
                'id' => 363,
                'section_number' => '4109',
                'kecamatan' => 'Mengwi',
            ),
            363 => 
            array (
                'id' => 364,
                'section_number' => '4110',
                'kecamatan' => 'Mengwi',
            ),
            364 => 
            array (
                'id' => 365,
                'section_number' => '4111',
                'kecamatan' => 'Mengwi',
            ),
            365 => 
            array (
                'id' => 366,
                'section_number' => '4112',
                'kecamatan' => 'Mengwi',
            ),
            366 => 
            array (
                'id' => 367,
                'section_number' => '4113',
                'kecamatan' => 'Mengwi',
            ),
            367 => 
            array (
                'id' => 368,
                'section_number' => '4114',
                'kecamatan' => 'Mengwi',
            ),
            368 => 
            array (
                'id' => 369,
                'section_number' => '4115',
                'kecamatan' => 'Mengwi',
            ),
            369 => 
            array (
                'id' => 370,
                'section_number' => '4116',
                'kecamatan' => 'Mengwi',
            ),
            370 => 
            array (
                'id' => 371,
                'section_number' => '4117',
                'kecamatan' => 'Mengwi',
            ),
            371 => 
            array (
                'id' => 372,
                'section_number' => '4118',
                'kecamatan' => 'Mengwi',
            ),
            372 => 
            array (
                'id' => 373,
                'section_number' => '4119',
                'kecamatan' => 'Mengwi',
            ),
            373 => 
            array (
                'id' => 374,
                'section_number' => '4120',
                'kecamatan' => 'Mengwi',
            ),
            374 => 
            array (
                'id' => 375,
                'section_number' => '4121',
                'kecamatan' => 'Mengwi',
            ),
            375 => 
            array (
                'id' => 376,
                'section_number' => '4122',
                'kecamatan' => 'Mengwi',
            ),
            376 => 
            array (
                'id' => 377,
                'section_number' => '4123',
                'kecamatan' => 'Mengwi',
            ),
            377 => 
            array (
                'id' => 378,
                'section_number' => '4124',
                'kecamatan' => 'Mengwi',
            ),
            378 => 
            array (
                'id' => 379,
                'section_number' => '4125',
                'kecamatan' => 'Mengwi',
            ),
            379 => 
            array (
                'id' => 380,
                'section_number' => '4126',
                'kecamatan' => 'Mengwi',
            ),
            380 => 
            array (
                'id' => 381,
                'section_number' => '4127',
                'kecamatan' => 'Mengwi',
            ),
            381 => 
            array (
                'id' => 382,
                'section_number' => '4128',
                'kecamatan' => 'Mengwi',
            ),
            382 => 
            array (
                'id' => 383,
                'section_number' => '4129',
                'kecamatan' => 'Mengwi',
            ),
            383 => 
            array (
                'id' => 384,
                'section_number' => '4130',
                'kecamatan' => 'Mengwi',
            ),
            384 => 
            array (
                'id' => 385,
                'section_number' => '4131',
                'kecamatan' => 'Mengwi',
            ),
            385 => 
            array (
                'id' => 386,
                'section_number' => '4132',
                'kecamatan' => 'Mengwi',
            ),
            386 => 
            array (
                'id' => 387,
                'section_number' => '4133',
                'kecamatan' => 'Mengwi',
            ),
            387 => 
            array (
                'id' => 388,
                'section_number' => '4134',
                'kecamatan' => 'Mengwi',
            ),
            388 => 
            array (
                'id' => 389,
                'section_number' => '4135',
                'kecamatan' => 'Mengwi',
            ),
            389 => 
            array (
                'id' => 390,
                'section_number' => '5001',
                'kecamatan' => 'Abiansemal',
            ),
            390 => 
            array (
                'id' => 391,
                'section_number' => '5002',
                'kecamatan' => 'Abiansemal',
            ),
            391 => 
            array (
                'id' => 392,
                'section_number' => '5003',
                'kecamatan' => 'Abiansemal',
            ),
            392 => 
            array (
                'id' => 393,
                'section_number' => '5004',
                'kecamatan' => 'Abiansemal',
            ),
            393 => 
            array (
                'id' => 394,
                'section_number' => '5005',
                'kecamatan' => 'Abiansemal',
            ),
            394 => 
            array (
                'id' => 395,
                'section_number' => '5006',
                'kecamatan' => 'Abiansemal',
            ),
            395 => 
            array (
                'id' => 396,
                'section_number' => '5007',
                'kecamatan' => 'Abiansemal',
            ),
            396 => 
            array (
                'id' => 397,
                'section_number' => '5008',
                'kecamatan' => 'Abiansemal',
            ),
            397 => 
            array (
                'id' => 398,
                'section_number' => '5009',
                'kecamatan' => 'Abiansemal',
            ),
            398 => 
            array (
                'id' => 399,
                'section_number' => '5010',
                'kecamatan' => 'Abiansemal',
            ),
            399 => 
            array (
                'id' => 400,
                'section_number' => '5011',
                'kecamatan' => 'Abiansemal',
            ),
            400 => 
            array (
                'id' => 401,
                'section_number' => '5012',
                'kecamatan' => 'Abiansemal',
            ),
            401 => 
            array (
                'id' => 402,
                'section_number' => '5013',
                'kecamatan' => 'Abiansemal',
            ),
            402 => 
            array (
                'id' => 403,
                'section_number' => '5014',
                'kecamatan' => 'Abiansemal',
            ),
            403 => 
            array (
                'id' => 404,
                'section_number' => '5015',
                'kecamatan' => 'Abiansemal',
            ),
            404 => 
            array (
                'id' => 405,
                'section_number' => '5016',
                'kecamatan' => 'Abiansemal',
            ),
            405 => 
            array (
                'id' => 406,
                'section_number' => '5017',
                'kecamatan' => 'Abiansemal',
            ),
            406 => 
            array (
                'id' => 407,
                'section_number' => '5018',
                'kecamatan' => 'Abiansemal',
            ),
            407 => 
            array (
                'id' => 408,
                'section_number' => '5019',
                'kecamatan' => 'Abiansemal',
            ),
            408 => 
            array (
                'id' => 409,
                'section_number' => '5020',
                'kecamatan' => 'Abiansemal',
            ),
            409 => 
            array (
                'id' => 410,
                'section_number' => '5021',
                'kecamatan' => 'Abiansemal',
            ),
            410 => 
            array (
                'id' => 411,
                'section_number' => '5022',
                'kecamatan' => 'Abiansemal',
            ),
            411 => 
            array (
                'id' => 412,
                'section_number' => '5023',
                'kecamatan' => 'Abiansemal',
            ),
            412 => 
            array (
                'id' => 413,
                'section_number' => '5024',
                'kecamatan' => 'Abiansemal',
            ),
            413 => 
            array (
                'id' => 414,
                'section_number' => '5025',
                'kecamatan' => 'Abiansemal',
            ),
            414 => 
            array (
                'id' => 415,
                'section_number' => '5026',
                'kecamatan' => 'Abiansemal',
            ),
            415 => 
            array (
                'id' => 416,
                'section_number' => '5027',
                'kecamatan' => 'Abiansemal',
            ),
            416 => 
            array (
                'id' => 417,
                'section_number' => '5028',
                'kecamatan' => 'Abiansemal',
            ),
            417 => 
            array (
                'id' => 418,
                'section_number' => '5029',
                'kecamatan' => 'Abiansemal',
            ),
            418 => 
            array (
                'id' => 419,
                'section_number' => '5030',
                'kecamatan' => 'Abiansemal',
            ),
            419 => 
            array (
                'id' => 420,
                'section_number' => '5031',
                'kecamatan' => 'Abiansemal',
            ),
            420 => 
            array (
                'id' => 421,
                'section_number' => '5032',
                'kecamatan' => 'Abiansemal',
            ),
            421 => 
            array (
                'id' => 422,
                'section_number' => '5033',
                'kecamatan' => 'Abiansemal',
            ),
            422 => 
            array (
                'id' => 423,
                'section_number' => '5034',
                'kecamatan' => 'Abiansemal',
            ),
            423 => 
            array (
                'id' => 424,
                'section_number' => '5035',
                'kecamatan' => 'Abiansemal',
            ),
            424 => 
            array (
                'id' => 425,
                'section_number' => '5036',
                'kecamatan' => 'Abiansemal',
            ),
            425 => 
            array (
                'id' => 426,
                'section_number' => '5037',
                'kecamatan' => 'Abiansemal',
            ),
            426 => 
            array (
                'id' => 427,
                'section_number' => '5038',
                'kecamatan' => 'Abiansemal',
            ),
            427 => 
            array (
                'id' => 428,
                'section_number' => '5039',
                'kecamatan' => 'Abiansemal',
            ),
            428 => 
            array (
                'id' => 429,
                'section_number' => '5040',
                'kecamatan' => 'Abiansemal',
            ),
            429 => 
            array (
                'id' => 430,
                'section_number' => '5041',
                'kecamatan' => 'Abiansemal',
            ),
            430 => 
            array (
                'id' => 431,
                'section_number' => '5042',
                'kecamatan' => 'Abiansemal',
            ),
            431 => 
            array (
                'id' => 432,
                'section_number' => '5043',
                'kecamatan' => 'Abiansemal',
            ),
            432 => 
            array (
                'id' => 433,
                'section_number' => '5044',
                'kecamatan' => 'Abiansemal',
            ),
            433 => 
            array (
                'id' => 434,
                'section_number' => '5045',
                'kecamatan' => 'Abiansemal',
            ),
            434 => 
            array (
                'id' => 435,
                'section_number' => '5046',
                'kecamatan' => 'Abiansemal',
            ),
            435 => 
            array (
                'id' => 436,
                'section_number' => '5047',
                'kecamatan' => 'Abiansemal',
            ),
            436 => 
            array (
                'id' => 437,
                'section_number' => '5048',
                'kecamatan' => 'Abiansemal',
            ),
            437 => 
            array (
                'id' => 438,
                'section_number' => '5049',
                'kecamatan' => 'Abiansemal',
            ),
            438 => 
            array (
                'id' => 439,
                'section_number' => '5050',
                'kecamatan' => 'Abiansemal',
            ),
            439 => 
            array (
                'id' => 440,
                'section_number' => '5051',
                'kecamatan' => 'Abiansemal',
            ),
            440 => 
            array (
                'id' => 441,
                'section_number' => '5052',
                'kecamatan' => 'Abiansemal',
            ),
            441 => 
            array (
                'id' => 442,
                'section_number' => '5053',
                'kecamatan' => 'Abiansemal',
            ),
            442 => 
            array (
                'id' => 443,
                'section_number' => '5054',
                'kecamatan' => 'Abiansemal',
            ),
            443 => 
            array (
                'id' => 444,
                'section_number' => '5055',
                'kecamatan' => 'Abiansemal',
            ),
            444 => 
            array (
                'id' => 445,
                'section_number' => '5056',
                'kecamatan' => 'Abiansemal',
            ),
            445 => 
            array (
                'id' => 446,
                'section_number' => '5057',
                'kecamatan' => 'Abiansemal',
            ),
            446 => 
            array (
                'id' => 447,
                'section_number' => '5058',
                'kecamatan' => 'Abiansemal',
            ),
            447 => 
            array (
                'id' => 448,
                'section_number' => '5059',
                'kecamatan' => 'Abiansemal',
            ),
            448 => 
            array (
                'id' => 449,
                'section_number' => '5060',
                'kecamatan' => 'Abiansemal',
            ),
            449 => 
            array (
                'id' => 450,
                'section_number' => '5061',
                'kecamatan' => 'Abiansemal',
            ),
            450 => 
            array (
                'id' => 451,
                'section_number' => '5062',
                'kecamatan' => 'Abiansemal',
            ),
            451 => 
            array (
                'id' => 452,
                'section_number' => '5063',
                'kecamatan' => 'Abiansemal',
            ),
            452 => 
            array (
                'id' => 453,
                'section_number' => '5064',
                'kecamatan' => 'Abiansemal',
            ),
            453 => 
            array (
                'id' => 454,
                'section_number' => '5065',
                'kecamatan' => 'Abiansemal',
            ),
            454 => 
            array (
                'id' => 455,
                'section_number' => '5066',
                'kecamatan' => 'Abiansemal',
            ),
            455 => 
            array (
                'id' => 456,
                'section_number' => '5067',
                'kecamatan' => 'Abiansemal',
            ),
            456 => 
            array (
                'id' => 457,
                'section_number' => '5068',
                'kecamatan' => 'Abiansemal',
            ),
            457 => 
            array (
                'id' => 458,
                'section_number' => '5069',
                'kecamatan' => 'Abiansemal',
            ),
            458 => 
            array (
                'id' => 459,
                'section_number' => '5070',
                'kecamatan' => 'Abiansemal',
            ),
            459 => 
            array (
                'id' => 460,
                'section_number' => '5071',
                'kecamatan' => 'Abiansemal',
            ),
            460 => 
            array (
                'id' => 461,
                'section_number' => '5072',
                'kecamatan' => 'Abiansemal',
            ),
            461 => 
            array (
                'id' => 462,
                'section_number' => '5073',
                'kecamatan' => 'Abiansemal',
            ),
            462 => 
            array (
                'id' => 463,
                'section_number' => '5074',
                'kecamatan' => 'Abiansemal',
            ),
            463 => 
            array (
                'id' => 464,
                'section_number' => '5075',
                'kecamatan' => 'Abiansemal',
            ),
            464 => 
            array (
                'id' => 465,
                'section_number' => '5076',
                'kecamatan' => 'Abiansemal',
            ),
            465 => 
            array (
                'id' => 466,
                'section_number' => '5077',
                'kecamatan' => 'Abiansemal',
            ),
            466 => 
            array (
                'id' => 467,
                'section_number' => '5078',
                'kecamatan' => 'Abiansemal',
            ),
            467 => 
            array (
                'id' => 468,
                'section_number' => '5079',
                'kecamatan' => 'Abiansemal',
            ),
            468 => 
            array (
                'id' => 469,
                'section_number' => '5080',
                'kecamatan' => 'Abiansemal',
            ),
            469 => 
            array (
                'id' => 470,
                'section_number' => '5081',
                'kecamatan' => 'Abiansemal',
            ),
            470 => 
            array (
                'id' => 471,
                'section_number' => '5082',
                'kecamatan' => 'Abiansemal',
            ),
            471 => 
            array (
                'id' => 472,
                'section_number' => '5083',
                'kecamatan' => 'Abiansemal',
            ),
            472 => 
            array (
                'id' => 473,
                'section_number' => '5084',
                'kecamatan' => 'Abiansemal',
            ),
            473 => 
            array (
                'id' => 474,
                'section_number' => '5085',
                'kecamatan' => 'Abiansemal',
            ),
            474 => 
            array (
                'id' => 475,
                'section_number' => '5086',
                'kecamatan' => 'Abiansemal',
            ),
            475 => 
            array (
                'id' => 476,
                'section_number' => '5087',
                'kecamatan' => 'Abiansemal',
            ),
            476 => 
            array (
                'id' => 477,
                'section_number' => '5088',
                'kecamatan' => 'Abiansemal',
            ),
            477 => 
            array (
                'id' => 478,
                'section_number' => '5089',
                'kecamatan' => 'Abiansemal',
            ),
            478 => 
            array (
                'id' => 479,
                'section_number' => '5090',
                'kecamatan' => 'Abiansemal',
            ),
            479 => 
            array (
                'id' => 480,
                'section_number' => '5091',
                'kecamatan' => 'Abiansemal',
            ),
            480 => 
            array (
                'id' => 481,
                'section_number' => '5092',
                'kecamatan' => 'Abiansemal',
            ),
            481 => 
            array (
                'id' => 482,
                'section_number' => '5093',
                'kecamatan' => 'Abiansemal',
            ),
            482 => 
            array (
                'id' => 483,
                'section_number' => '5094',
                'kecamatan' => 'Abiansemal',
            ),
            483 => 
            array (
                'id' => 484,
                'section_number' => '5095',
                'kecamatan' => 'Abiansemal',
            ),
            484 => 
            array (
                'id' => 485,
                'section_number' => '5096',
                'kecamatan' => 'Abiansemal',
            ),
            485 => 
            array (
                'id' => 486,
                'section_number' => '5097',
                'kecamatan' => 'Abiansemal',
            ),
            486 => 
            array (
                'id' => 487,
                'section_number' => '5098',
                'kecamatan' => 'Abiansemal',
            ),
            487 => 
            array (
                'id' => 488,
                'section_number' => '5099',
                'kecamatan' => 'Abiansemal',
            ),
            488 => 
            array (
                'id' => 489,
                'section_number' => '5100',
                'kecamatan' => 'Abiansemal',
            ),
            489 => 
            array (
                'id' => 490,
                'section_number' => '5101',
                'kecamatan' => 'Abiansemal',
            ),
            490 => 
            array (
                'id' => 491,
                'section_number' => '5102',
                'kecamatan' => 'Abiansemal',
            ),
            491 => 
            array (
                'id' => 492,
                'section_number' => '6001',
                'kecamatan' => 'Petang',
            ),
            492 => 
            array (
                'id' => 493,
                'section_number' => '6002',
                'kecamatan' => 'Petang',
            ),
            493 => 
            array (
                'id' => 494,
                'section_number' => '6003',
                'kecamatan' => 'Petang',
            ),
            494 => 
            array (
                'id' => 495,
                'section_number' => '6004',
                'kecamatan' => 'Petang',
            ),
            495 => 
            array (
                'id' => 496,
                'section_number' => '6005',
                'kecamatan' => 'Petang',
            ),
            496 => 
            array (
                'id' => 497,
                'section_number' => '6006',
                'kecamatan' => 'Petang',
            ),
            497 => 
            array (
                'id' => 498,
                'section_number' => '6007',
                'kecamatan' => 'Petang',
            ),
            498 => 
            array (
                'id' => 499,
                'section_number' => '6008',
                'kecamatan' => 'Petang',
            ),
            499 => 
            array (
                'id' => 500,
                'section_number' => '6009',
                'kecamatan' => 'Petang',
            ),
            500 => 
            array (
                'id' => 501,
                'section_number' => '6010',
                'kecamatan' => 'Petang',
            ),
            501 => 
            array (
                'id' => 502,
                'section_number' => '6011',
                'kecamatan' => 'Petang',
            ),
            502 => 
            array (
                'id' => 503,
                'section_number' => '6012',
                'kecamatan' => 'Petang',
            ),
            503 => 
            array (
                'id' => 504,
                'section_number' => '6013',
                'kecamatan' => 'Petang',
            ),
            504 => 
            array (
                'id' => 505,
                'section_number' => '6014',
                'kecamatan' => 'Petang',
            ),
            505 => 
            array (
                'id' => 506,
                'section_number' => '6015',
                'kecamatan' => 'Petang',
            ),
            506 => 
            array (
                'id' => 507,
                'section_number' => '6016',
                'kecamatan' => 'Petang',
            ),
            507 => 
            array (
                'id' => 508,
                'section_number' => '6017',
                'kecamatan' => 'Petang',
            ),
            508 => 
            array (
                'id' => 509,
                'section_number' => '6018',
                'kecamatan' => 'Petang',
            ),
            509 => 
            array (
                'id' => 510,
                'section_number' => '6019',
                'kecamatan' => 'Petang',
            ),
            510 => 
            array (
                'id' => 511,
                'section_number' => '6020',
                'kecamatan' => 'Petang',
            ),
            511 => 
            array (
                'id' => 512,
                'section_number' => '6021',
                'kecamatan' => 'Petang',
            ),
            512 => 
            array (
                'id' => 513,
                'section_number' => '6022',
                'kecamatan' => 'Petang',
            ),
            513 => 
            array (
                'id' => 514,
                'section_number' => '6023',
                'kecamatan' => 'Petang',
            ),
            514 => 
            array (
                'id' => 515,
                'section_number' => '6024',
                'kecamatan' => 'Petang',
            ),
            515 => 
            array (
                'id' => 516,
                'section_number' => '6025',
                'kecamatan' => 'Petang',
            ),
            516 => 
            array (
                'id' => 517,
                'section_number' => '6026',
                'kecamatan' => 'Petang',
            ),
            517 => 
            array (
                'id' => 518,
                'section_number' => '6027',
                'kecamatan' => 'Petang',
            ),
            518 => 
            array (
                'id' => 519,
                'section_number' => '6028',
                'kecamatan' => 'Petang',
            ),
            519 => 
            array (
                'id' => 520,
                'section_number' => '6029',
                'kecamatan' => 'Petang',
            ),
            520 => 
            array (
                'id' => 521,
                'section_number' => '6030',
                'kecamatan' => 'Petang',
            ),
            521 => 
            array (
                'id' => 522,
                'section_number' => '6031',
                'kecamatan' => 'Petang',
            ),
            522 => 
            array (
                'id' => 523,
                'section_number' => '6032',
                'kecamatan' => 'Petang',
            ),
            523 => 
            array (
                'id' => 524,
                'section_number' => '6033',
                'kecamatan' => 'Petang',
            ),
            524 => 
            array (
                'id' => 525,
                'section_number' => '6034',
                'kecamatan' => 'Petang',
            ),
            525 => 
            array (
                'id' => 526,
                'section_number' => '6035',
                'kecamatan' => 'Petang',
            ),
            526 => 
            array (
                'id' => 527,
                'section_number' => '6036',
                'kecamatan' => 'Petang',
            ),
            527 => 
            array (
                'id' => 528,
                'section_number' => '6037',
                'kecamatan' => 'Petang',
            ),
            528 => 
            array (
                'id' => 529,
                'section_number' => '6038',
                'kecamatan' => 'Petang',
            ),
            529 => 
            array (
                'id' => 530,
                'section_number' => '6039',
                'kecamatan' => 'Petang',
            ),
            530 => 
            array (
                'id' => 531,
                'section_number' => '6040',
                'kecamatan' => 'Petang',
            ),
            531 => 
            array (
                'id' => 532,
                'section_number' => '6041',
                'kecamatan' => 'Petang',
            ),
            532 => 
            array (
                'id' => 533,
                'section_number' => '6042',
                'kecamatan' => 'Petang',
            ),
            533 => 
            array (
                'id' => 534,
                'section_number' => '6043',
                'kecamatan' => 'Petang',
            ),
            534 => 
            array (
                'id' => 535,
                'section_number' => '6044',
                'kecamatan' => 'Petang',
            ),
            535 => 
            array (
                'id' => 536,
                'section_number' => '6045',
                'kecamatan' => 'Petang',
            ),
            536 => 
            array (
                'id' => 537,
                'section_number' => '6046',
                'kecamatan' => 'Petang',
            ),
            537 => 
            array (
                'id' => 538,
                'section_number' => '6047',
                'kecamatan' => 'Petang',
            ),
        );

        foreach ($streetDistrictData as $key => $streetDistrict) {
            $street = App\Street::where('section_number', '=', $streetDistrict['section_number'])->first();
            $kabupatenId = config('app.kabupaten');
            $district = District::where([
                ['city_id', '=', $kabupatenId],
                ['name', 'like', sprintf('%%%s%%', $streetDistrict['kecamatan'])],
            ])->first();

            if (! empty($street) && ! empty($district)) {
                $street->kabupaten_id = $kabupatenId;
                $street->kecamatan_id = $district->id;
                $street->save();
            }

        }
    }
}
