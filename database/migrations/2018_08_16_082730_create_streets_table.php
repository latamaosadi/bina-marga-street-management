<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStreetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('streets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('section_number');
            $table->string('base_section_name');
            $table->string('end_section_name');
            $table->char('kabupaten_id', 4)->nullable();
            $table->char('kecamatan_id', 7)->nullable();
            $table->char('kelurahan_id', 10)->nullable();
            $table->year('validity_year')->nullable();
            $table->string('base_section_latitude')->nullable();
            $table->string('base_section_longitude')->nullable();
            $table->string('end_section_latitude')->nullable();
            $table->string('end_section_longitude')->nullable();
            $table->string('color')->default('#3490DC');
            $table->integer('street_type_id')->nullable();
            $table->text('polyline')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('streets');
    }
}
