<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('street_id');
            $table->year('year');
            $table->integer('length');
            $table->integer('width');
            $table->integer('lhrt');
            $table->integer('vcr');
            $table->integer('street_type_id');
            $table->integer('mst');
            $table->integer('pavement_id');
            $table->integer('gravel');
            $table->integer('macadam');
            $table->integer('asphalt');
            $table->integer('rigid');
            $table->year('last_maintained_at');
            $table->integer('maintenance_type_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
}
