<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBridgeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bridge_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bridge_id');
            $table->year('year');
            $table->integer('length');
            $table->integer('width');
            $table->integer('bent_amount');
            $table->string('top_type');
            $table->string('top_condition');
            $table->string('floor_type');
            $table->string('floor_material');
            $table->string('floor_condition');
            $table->string('support_type');
            $table->string('support_material');
            $table->string('support_condition');
            $table->string('foundation_type');
            $table->string('foundation_material');
            $table->string('foundation_condition');
            $table->string('head_type');
            $table->string('head_material');
            $table->string('head_condition');
            $table->string('pillar_type');
            $table->string('pillar_material');
            $table->string('pillar_condition');
            $table->text('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bridge_details');
    }
}
