<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sreet Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'street_name' => 'Street Name',
    'street' => 'Street',
    'street_number' => 'Street Number',
    'base_section_name' => 'Base Section Name',

];
