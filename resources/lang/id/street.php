<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sreet Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'name' => 'Nama Jalan',
    'street' => 'Jalan',
    'section_number' => 'Nomor Ruas',
    'base_section_name' => 'Nama Pangkal Ruas',
    'end_section_name' => 'Nama Ujung Ruas',
    'kecamatan' => 'Kecamatan',
    'kabupaten' => 'Kabupaten',
    'kelurahan' => 'Desa / Kelurahan',
    'base_coordinate_section' => 'Koordinat Pangkal Ruas',
    'end_coordinate_section' => 'Koordinat Ujung Ruas',
    'condition' => 'Kondisi',
    'technical' => 'Teknis',
    'location' => 'Gambar Jalan',
    'add' => 'Tambah Jalan',
    'edit' => 'Ubah Data Jalan',
    'management' => 'Manajemen Jalan',
    'all' => 'Semua Jalan',
    'new' => 'Jalan Baru',
    'search' => 'Cari Jalan',
    'see_all' => 'Lihat Semua',
    'empty' => 'Belum ada jalan yang terdaftar, silahkan memulai dengan klik Tambah Jalan.',
    'submit' => 'Simpan Jalan',
    'choose_kabupaten' => 'Pilih Kabupaten',
    'choose_kecamatan' => 'Pilih Kecamatan',
    'choose_kelurahan' => 'Pilih Desa / Kelurahan',
    'delete_point' => 'Hapus Titik',
    'manual' => 'Gambar Manual',
    'auto' => 'Otomatis',
    'preview' => 'Lihat Hasil',
    'snap_to_road' => 'Tempel ke jalan',
    'edit_mode' => 'Mode Edit',
    'start_point' => 'Titik Awal',
    'end_point' => 'Titik Akhir',
    'update_sub' => 'Ubah :street_name',
    'delete' => 'Hapus Jalan',
    'delete_confirmation_title' => 'Hapus Jalan!',
    'delete_confirmation_content' => 'Apakah Anda yakin ingin menghapus data jalan ini?',
    'area' => 'Wilayah Jalan',
    'manage_bridges' => 'Kelola Jembatan',
    'manage_conditions' => 'Kelola Kondisi Jalan',
    'manage_technicals' => 'Kelola Data Teknis',
    'manage_gallery' => 'Kelola Galeri',
    'basic_information' => 'Informasi Dasar',
    'gallery' => 'Galeri',
    'google_base_street_name' => 'Nama pangkal ruas (Google)',
    'google_end_street_name' => 'Nama ujung ruas (Google)',
    'google_middle_street_name' => 'Nama tengah ruas (Google)',
    'video_url' => 'ID Video',
    'video_title' => 'Judul Video',
    'videos' => 'Video',
    'manage_videos' => 'Kelola Video',
    'submit_video' => 'Simpan Video',

];
