<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'signin' => 'Masuk',
    'signup' => 'Daftar',

    'username' => 'Nama Pengguna',
    'username_help' => 'Nama Pengguna harus diisi',
    'password' => 'Password',
    'password_help' => 'Password harus diisi',
    'password_confirmation' => 'Konfirmasi Password',
    'password_confirmation_help' => 'Konfirmasi Password harus diisi',
    'name' => 'Nama',
    'name_help' => 'Nama harus diisi',
    'email' => 'Email',
    'email_help' => 'Email harus diisi',

    'already_signup' => 'Sudah punya akun?',

];
