<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sreet Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'dimension' => 'Dimensi',
    'top_building' => 'Bangunan Atas',
    'lower_building' => 'Bangunan Bawah',

    'upper_structure' => 'Struktur Bangunan Atas',
    'floor' => 'Lantai',
    'support' => 'Sandaran',
    'foundation' => 'Pondasi',
    'bridge_head' => 'Kepala Jembatan',
    'pillar' => 'Pilar',

    'type' => 'Tipe',
    'condition' => 'Kondisi',
    'material' => 'Bahan',

    'year' => 'Tahun',
    'length' => 'Panjang Jembatan (m)',
    'width' => 'Lebar Jalur (m)',
    'bent_amount' => 'Jumlah Bent',

    'notes' => 'Keterangan',
    'submit' => 'Simpan',
    'edit' => 'Edit',
    'delete' => 'Delete',

];
