<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sreet Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'file' => 'Pilih Gambar',
    'add_files' => 'Tambah Gambar',
    'start_upload' => 'Upload Semua',
    'processing' => 'Sedang diproses',
    'upload' => 'Upload',
    'name' => 'Judul Gambar',
    'submit' => 'Simpan Gambar',
    'edit' => 'Ubah Judul',
    'delete' => 'Hapus Gambar',
    'empty' => 'Belum ada data gambar, silahkan input gambar.',
    'use_as_avatar' => 'Gunakan Sebagai Avatar',
    'use_as_cover' => 'Gunakan Sebagai Cover',

];
