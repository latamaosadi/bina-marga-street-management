<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sreet Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'good' => 'Baik',
    'average' => 'Sedang',
    'light' => 'Ringan',
    'heavy' => 'Rusak',
    'year' => 'Tahun',
    'submit' => 'Simpan Kondisi',
    'edit' => 'Ubah Kondisi',
    'delete' => 'Hapus Kondisi',
    'empty' => 'Belum ada data kondisi, silahkan menambahkan kondisi dengan memilih tahun.',

];
