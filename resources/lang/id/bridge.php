<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sreet Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'name' => 'Nama Jembatan',
    'add' => 'Tambah Jembatan',
    'management' => 'Manajemen Jembatan',
    'all' => 'Semua Jembatan',
    'new' => 'Jembatan Baru',
    'search' => 'Cari Jembatan',
    'see' => 'Lihat Semua',
    'empty' => 'Belum ada Jembatan yang terdaftar, silahkan memulai dengan klik Tambah Jembatan.',
    'submit' => 'Simpan Jembatan',
    'see_all' => 'Semua Jembatan',
    'edit' => 'Edit Jembatan',
    'manage_details' => 'Kelola Detail',
    'delete' => 'Hapus Jembatan',
    'manage_gallery' => 'Kelola Galeri',
    'latitude' => 'Latitude',
    'longitude' => 'Longitude',
    'location' => 'Lokasi',
    'update_sub' => 'Ubah :bridge_name',
    'street' => 'Jalan',
    'delete_confirmation_title' => 'Hapus Jembatan!',
    'delete_confirmation_content' => 'Apakah Anda yakin ingin menghapus data jembatan ini?',

];
