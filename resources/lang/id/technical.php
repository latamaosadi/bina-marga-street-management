<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sreet Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'year' => 'Tahun',
    'length' => 'Panjang',
    'width' => 'Lebar',
    'lhrt' => 'LHRT',
    'vcr' => 'VCR',
    'street_type' => 'Tipe Jalan',
    'mst' => 'MST',
    'pavement' => 'Tipe Perkerasan',
    'gravel' => 'Tanah Kerikil',
    'macadam' => 'Macadam',
    'asphalt' => 'Aspal',
    'rigid' => 'Rigid',
    'last_maintained_at' => 'Tahun Penanganan Terakhir',
    'maintenance_type' => 'Jenis Penanganan',
    'submit' => 'Simpan Teknis',
    'edit' => 'Ubah Teknis',
    'delete' => 'Hapus Teknis',
    'empty' => 'Belum ada data teknis, silahkan menambahkan teknis dengan memilih tahun.',

];
