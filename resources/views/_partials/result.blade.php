@foreach ($streets as $street)
<li class="hover:bg-grey-lighter text-sm">
  <a href="{{ route('streets.show', ['id' => $street->id]) }}" target="_blank" class="py-2 px-4 block" data-street-id="{{ $street->id }}">
    <p>
      {!! preg_replace('/' . preg_quote($query, '/') . '/i', '<strong>$0</strong>', sprintf('#%s %s, %s-%s', $street->section_number, $street->name, $street->base_section_name, $street->end_section_name)) !!}
    </p>
    
    @if (! empty($street->google_base_street_name))
    <p class="ml-2">
      <small class="bold text-grey-dark"> {!! preg_replace('/' . preg_quote($query, '/') . '/i', '<strong>$0</strong>', sprintf('%s', $street->google_base_street_name)) !!}</small>
      <small class="bold text-grey-dark"> - {!! preg_replace('/' . preg_quote($query, '/') . '/i', '<strong>$0</strong>', sprintf('%s', $street->google_end_street_name)) !!}</small>
    </p>
    @endif

    
  </a>
</li>
@endforeach