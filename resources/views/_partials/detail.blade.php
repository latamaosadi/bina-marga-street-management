<div class="bg-white overflow-hidden">
  <div class="m-b-1 h-32 overflow-hidden">
    <img class="block w-full" src="{{ ! empty($street->cover->filename) ? asset('storage/' . $street->cover->filename) : asset('images/cover.jpg') }}" alt="">
  </div>
  <div class="sm:flex sm:items-center px-6 py-4 mx-auto">
    <div class="text-center sm:text-left sm:flex-grow">
      <div class="mb-4">
        <p class="text-xl leading-tight text-center">{{ $street->name }} - {{ $street->section_number }}</p>
        <p class="text-sm leading-tight text-grey-dark text-center">{{ $street->base_section_name }} - {{ $street->end_section_name }}</p>
      </div>
      <div class="text-center">
        <a href="{{ route('streets.show', ['id' => $street->id]) }}" class="text-xs font-semibold rounded-full px-4 py-1 leading-normal bg-white border border-purple text-purple hover:bg-purple hover:text-white">@lang('home.see_detail')</a>
      </div>
    </div>
  </div>
</div>