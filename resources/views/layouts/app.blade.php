<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <title>Bina Marga Street Management - @yield('title', 'Home Page')</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet">

    <link href="/css/vendor.css" rel="stylesheet" type="text/css" />
    <link href="/vendor/jquery-confirm/jquery-confirm.min.css" rel="stylesheet" type="text/css" />
    <link href="/vendor/lightbox/css/lightbox.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/app.css" rel="stylesheet" type="text/css" />

  </head>
  <body class="font-sans">
    @include('layouts.header')
    <div class="application-container">
      @yield('content')
    </div>
    @include('layouts.footer')

    @include('layouts.auth')
    
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="/vendor/lodash/lodash.min.js"></script>
    <script src="/vendor/jquery-confirm/jquery-confirm.min.js"></script>
    <script src="/vendor/micromodal/micromodal.min.js"></script>
    <script src="/vendor/lightbox/js/lightbox.min.js"></script>
    <script src="/js/app.js" type="javascript"></script>
    <script src="/js/auth.js"></script>
    @yield('customScripts')
  </body>
</html>