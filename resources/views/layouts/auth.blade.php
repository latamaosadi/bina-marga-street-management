<div class="modal micromodal-slide" id="modal-auth" aria-hidden="true">
  <div class="modal__overlay p-4" tabindex="-1" data-custom-close>
    <div class="modal__container w-full max-w-xs modal-auth" role="dialog" aria-modal="true" aria-labelledby="modal-2-title">
      <header class="modal__header">
        <button class="modal__close" aria-label="Close modal" data-custom-close></button>
      </header>
      <div class="w-full">

        <div class="login-container">
          <h3 class="modal__title text-red font-bold">
            🔒 @lang('auth.signin')
          </h3>
          <form class="pt-5 mb-4" role="form" action="{{ route('auth.authenticate') }}" method="POST">
            {{ csrf_field() }}
            <div class="mb-4">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="username">
                @lang('auth.username')
              </label>
              <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" type="text" placeholder="@lang('auth.username')" name="username">
              <p class="text-red text-xs italic mt-3 hidden">@lang('auth.username_help')</p>
            </div>
            <div class="mb-6">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
                @lang('auth.password')
              </label>
              <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" type="password" placeholder="•••••••" name="password">
              <p class="text-red text-xs italic mt-3 hidden">@lang('auth.password_help')</p>
            </div>
            <div class="flex items-center justify-between">
              <button class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                @lang('auth.signin')
              </button>
              <a class="inline-block align-baseline font-bold text-sm text-blue hover:text-blue-darker signup" href="#">
                @lang('auth.signup')
              </a>
            </div>
          </form>
        </div>

        <div class="signup-container">
          <h3 class="modal__title text-red font-bold">
            🔏 @lang('auth.signup')
          </h3>
          <form class="pt-5 mb-4" role="form" action="{{ route('auth.register') }}" method="POST">
            {{ csrf_field() }}
            <div class="mb-4">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="name">
                @lang('auth.name')
              </label>
              <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" type="text" placeholder="@lang('auth.name')" name="name">
              <p class="text-red text-xs italic mt-3 hidden">@lang('auth.name_help')</p>
            </div>

            <div class="mb-4">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="username">
                @lang('auth.username')
              </label>
              <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" type="text" placeholder="@lang('auth.username')" name="username">
              <p class="text-red text-xs italic mt-3 hidden">@lang('auth.username_help')</p>
            </div>

            <div class="mb-4">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="email">
                @lang('auth.email')
              </label>
              <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" type="text" placeholder="@lang('auth.email')" name="email">
              <p class="text-red text-xs italic mt-3 hidden">@lang('auth.email_email')</p>
            </div>

            <div class="mb-4">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
                @lang('auth.password')
              </label>
              <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" type="password" placeholder="•••••••" name="password">
              <p class="text-red text-xs italic mt-3 hidden">@lang('auth.password_help')</p>
            </div>

            <div class="mb-6">
              <label class="block text-grey-darker text-sm font-bold mb-2" for="password_confirmation">
                @lang('auth.password_confirmation')
              </label>
              <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" type="password" placeholder="•••••••" name="password_confirmation">
              <p class="text-red text-xs italic mt-3 hidden">@lang('auth.password_confirmation_help')</p>
            </div>

            <div class="flex items-center justify-between">
              <button class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                @lang('auth.signup')
              </button>
              <a class="inline-block align-baseline font-bold text-sm text-blue hover:text-blue-darker signin" href="#">
                @lang('auth.already_signup')
              </a>
            </div>
          </form>
        </div>
        
      </div>
    </div>
  </div>
</div>