<nav class="flex items-center justify-between flex-wrap bg-red p-4">

  <div class="md:items-center md:flex-no-shrink text-white mr-6 md:flex">
    <a href="{{ route('home') }}" class="text-white">
      <span class="font-semibold text-base md:text-xl tracking-tight mr-0 md:mr-2">
        <i class="fas fa-map-marker-alt"></i>
      </span>
      <span class="font-semibold text-xl tracking-tight hidden md:inline-block">BMSM</span>
    </a>
  </div>

  {{--  <div class="block sm:hidden">
    <button class="flex items-center px-3 py-2 border rounded text-red-lighter border-red-light hover:text-white hover:border-white">
      <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
    </button>
  </div>  --}}

  <div class="block flex-grow flex items-center w-auto">

    {{--  <div class="text-sm lg:flex-grow">
      <a href="#responsive-header" class="block mt-4 lg:inline-block lg:mt-0 text-red-lighter hover:text-white mr-4">
        About
      </a>
    </div>  --}}

    @yield('extra-content')

    @if (false)
    <div class="">
      @auth
        <a href="{{ route('admin.streets.list') }}" class="inline-block text-sm px-3 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-red-light hover:bg-white"><i class="fas fa-toolbox mr-1"></i><span class="hidden md:inline"> Portal</span></a>
      @else
        <a href="{{ route('auth.signin') }}" class="inline-block text-sm px-3 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-red-light hover:bg-white signin"><i class="fas fa-sign-in-alt mr-1"></i> <span class="hidden md:inline"> Login</span></a>
      @endauth
    </div>
    @endif

  </div>
</nav>