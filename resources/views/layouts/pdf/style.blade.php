<style>
  body {
    font-family: 'Helvetica';
    font-size: 12px;
  }

  p {
    margin: 0;
  }

  .h1 {
    font-size: 18px;
  }

  .h2 {
    font-size: 17px;
  }

  .h3 {
    font-size: 16px;
  }

  .h4 {
    font-size: 15px;
  }

  .h5 {
    font-size: 14px;
  }

  .h6 {
    font-size: 13px;
  }

  .p-2 {
    padding: 0.5rem;
  }

  .px-2 {
    padding-left: 0.5rem;
    padding-right: 0.5rem;
  }

  .py-2 {
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
  }

  .mb-2 {
    margin-bottom: 0.5rem;
  }

  .mb-3 {
    margin-bottom: 0.75rem;
  }

  .mb-4 {
    margin-bottom: 1rem;
  }

  .font-bold {
    font-weight: 700;
  }

  .text-center {
    text-align: center;
  }

  table {
    border-collapse: collapse;
  }

  .table-bordered td {
    border: 1px solid #000;
  }

  .horizontal-list {
  }

</style>