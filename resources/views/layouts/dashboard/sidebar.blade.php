<aside class="main-sidebar">
  <section class="sidebar">

    {{--
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="/admin/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Alexander Pierce</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    --}}

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MENU NAVIGATION</li>
      <li class="{{ strpos(\Route::current()->getName(), 'dashboard') !== false ? 'active' : '' }}">
        <a href="{{ route('admin.dashboard') }}">
          <i class="fas fa-home"></i>
          <span>Summary</span>
        </a>
      </li>
      <li class="{{ strpos(\Route::current()->getName(), 'streets.') !== false ? 'active' : '' }}">
        <a href="{{ route('admin.streets.list') }}">
          <i class="fas fa-road"></i>
          <span>Streets</span>
        </a>
      </li>
      <li class="{{ strpos(\Route::current()->getName(), 'bridges.') !== false ? 'active' : '' }}">
        <a href="{{ route('admin.bridges.list') }}">
          <i class="fas fa-archway"></i>
          <span>Bridges</span>
        </a>
      </li>
      {{--
      <li class="treeview">
        <a href="#">
          <i class="fa fa-edit"></i> <span>Forms</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="active"><a href="general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
          <li><a href="advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
          <li><a href="editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
        </ul>
      </li>
      --}}
    </ul>
  </section>
</aside>