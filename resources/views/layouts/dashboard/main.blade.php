<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Bina Marga Street Management - @yield('pageTitle', 'Summary')</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/vendor/select2/css/select2.min.css">
    <link href="/vendor/jquery-confirm/jquery-confirm.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/css/admin-vendor.css">
    <link rel="stylesheet" href="/admin/css/AdminLTE.min.css">
    <link rel="stylesheet" href="/admin/css/skins/skin-red-light.min.css">
    <link rel="stylesheet" href="/css/admin-custom.css">
    <link rel="stylesheet" href="/vendor/jquery-file-upload/css/jquery.fileupload.css">
    <link rel="stylesheet" href="/vendor/jquery-file-upload/css/jquery.fileupload-ui.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="hold-transition skin-red-light sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <a href="{{ route('home') }}" class="logo">
          <span class="logo-mini"><b>BM</b>SM</span>
          <span class="logo-lg"><b>Bina Marga</b> SM</span>
        </a>
        @include('layouts.dashboard.header')
      </header>

      @include('layouts.dashboard.sidebar')
      
      <div class="content-wrapper">
        <section class="content-header">
          @section('title')
            <h1>
              Bina Marga Street Management
              <small> - Dashboard</small>
            </h1>
          @show
        </section>

        <section class="content">
          @yield('content')
        </section>
      </div>

      @include('layouts.dashboard.footer')
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="/vendor/lodash/lodash.min.js"></script>
    <script src="/vendor/fastclick/js/fastclick.js"></script>
    <script src="/vendor/urijs/js/uri.all.min.js"></script>
    <script src="/vendor/select2/js/select2.full.min.js"></script>
    <script src="/vendor/jquery-confirm/jquery-confirm.min.js"></script>
    <script src="/vendor/infinite-scroll/infinite-scroll.min.js"></script>
    <script src="/admin/js/adminlte.min.js"></script>
    <script src="/vendor/micromodal/micromodal.min.js"></script>
    <script src="/vendor/chartjs/chart.bundle.min.js"></script>
    <script>
      $(function() {
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $('.select2').select2({
          width: '100%',
        });
      });
    </script>
    @yield('customScripts')
  </body>
</html>
