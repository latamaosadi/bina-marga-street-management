<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15"/>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet">

    <link href="/css/vendor.css" rel="stylesheet" type="text/css" />
    <link href="/css/app.css" rel="stylesheet" type="text/css" />
    <link href="/css/pdf.css" rel="stylesheet" type="text/css" />

  </head>
  <body class="font-sans">
    @include('layouts.print.header')
    <div class="container mx-auto p-4 pb-12">
      @yield('content')
    </div>
    @include('layouts.print.footer')
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="/vendor/lodash/lodash.min.js"></script>
    <script src="/js/app.js" type="javascript"></script>
    @yield('customScripts')
  </body>
</html>