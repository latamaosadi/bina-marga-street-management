@extends('layouts.dashboard.main')

@section('title')
<h1>
  @lang('bridge.management')
  <small> - @lang('bridge.all')</small>
</h1>
@stop

@section('content')
<div class="box box-primary">

  <div class="box-header with-border">
    <div class="row">
      <div class="col-xs-3 col-lg-2">
        <a href="{{ route('admin.bridges.create') }}" class="btn btn-primary btn-block">
          <i class="fas fa-plus"></i><span class="hidden-xs"> @lang('bridge.add')</span> 
        </a>
      </div>

      <div class="col-xs-9 col-lg-10">
        <form action="{{ route('admin.bridges.list') }}" method="GET">
          <div class="input-group">
            <input type="text" class="form-control" name="query" value="{{ $query }}">
            <div class="input-group-btn">
              <button type="button" class="btn btn-primary"><i class="fas fa-search"></i><span class="hidden-xs"> @lang('bridge.search')</span></button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="box-body table-responsive bridge-table-container">
    <div id="bridge-map" class="map-container"></div>
    @if ($bridges->count() > 0)
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th style="width: 10px">#</th>
          <th>@lang('bridge.name')</th>
          <th>@lang('bridge.street')</th>
          <th style="width: 40px">@lang('label.action')</th>
        </tr>
      </thead>

      <tbody class="bridge-container">
      @include('bridge.table-records', compact('bridges', 'query'))
      </tbody>

    </table>
    @else
    <p class="text-muted">
      @lang('bridge.empty')
    </p>
    @endif
  </div>
  @if ($bridges->lastPage() > 1)
  <div class="box-footer clearfix">
    {{ $bridges->appends(['query' => Request::get('query')])->links('vendor.pagination.default') }}
  </div>
  @endif

</div> 
@stop

@section('customScripts')
<script>
  var map;
  var bridgeMarkers = [];
  function initMap() {
    var icon = {
      url: '{{ asset('images/bridge-b.png') }}', // url
      scaledSize: new google.maps.Size(24, 24), // scaled size
    };
    map = new google.maps.Map(document.getElementById('bridge-map'), {
      center: {lat: -8.670458, lng: 115.212631},
      zoom: 12,
      streetViewControl: false,
      mapTypeControl: false,
    });

    $.ajax({
      method: 'GET',
      url: '{{ route('loadBridges') }}',
    }).done(function (response) {
      var bridges = response;
      _.forEach(bridges, function (bridge) {
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(bridge.coord.lat, bridge.coord.lng),
          icon: icon,
          map: map,
          title: bridge.name,
        });
        google.maps.event.addListener(marker, 'click', function() {
          window.location.href = '{{ route('admin.bridges.list') }}/' + bridge.id;
        });
        bridgeMarkers[bridge.id] = marker;
      });
    });
  };
$(function() {
  function loadBridges() {
    $.ajax({
      url: '{{ route('admin.bridges.list') }}',
      method: 'GET',
      dataType: 'html',
    }).done(function(response) {
      $('.bridge-container').html(response);
    });
  };

  $('.bridge-container').on('click', '.delete-bridge', function(event) {
    event.preventDefault();
    var bridgeId = $(this).data('id');
    var recordEl = $(this).closest('tr');
    $.confirm({
      title: '@lang('bridge.delete_confirmation_title')',
      content: '@lang('bridge.delete_confirmation_content')',
      buttons: {
        confirm: function () {
          $.ajax({
            url: '{{ route('admin.bridges.destroy') }}/' + bridgeId,
            method: 'POST',
            dataType: 'json',
          }).done(function(response) {
            window.location.reload(true);
          });
        },
        cancel: function () {
        }
      }
    });
  });
});
</script>
<script src="http://maps.googleapis.com/maps/api/js?key={{ config('app.gMapKey') }}&callback=initMap" async defer></script>
@stop