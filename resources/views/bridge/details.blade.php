@extends('layouts.dashboard.main')

@section('title')
<h1>
  @lang('bridge.manage_details')
  <small>{{ $bridge->name }}</small>
</h1>
@stop

@section('content')
<div class="box box-primary">

  <div class="box-header with-border">
    <div class="row">
      <div class="col-xs-12">
        <div class="btn-group">
          <a href="{{ route('admin.bridges.list') }}" class="btn btn-danger">
            <i class="fas fa-arrow-left"></i><span class="hidden-xs"> @lang('bridge.see_all')</span>
          </a>
          <a href="{{ route('admin.bridges.edit', ['id' => $bridge->id]) }}" type="button" class="btn btn-default">
            <i class="fas fa-edit"></i><span class="hidden-xs"> @lang('bridge.edit')</span>
          </a>
        </div>
      </div>
    </div>
  </div>

  <div class="box-body">
    <div class="row">
      <div class="col-xs-12 detail-form-container">
        <form role="form" action="{{ route('admin.bridges.details.new', ['id' => $bridge->id]) }}" method="POST">
          <div class="box-body">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $bridge->id }}" id="bridge_id" name="bridge_id">

            <div class="row">
              <div class="col-xs-12">

                <div class="row">
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="year">@lang('bridge_detail.year')</label>
                      <select class="form-control select2" id="year" name="year">
                        @foreach ($years as $year)
                        <option value="{{ $year }}" {{ ! empty($currentDetail) ? ($currentDetail->year == $year ? 'selected="selected"' : '') : ($year == $selectedYear ? 'selected="selected"' : '') }}>{{ $year }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>

                <h3>@lang('bridge_detail.dimension')</h3>
                <div class="row">
                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="length">@lang('bridge_detail.length')</label>
                      <input type="number" class="form-control" id="length" name="length" placeholder="@lang('bridge_detail.length')" value="{{ $currentDetail->length or '' }}">
                    </div>
                  </div>

                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="width">@lang('bridge_detail.width')</label>
                      <input type="number" class="form-control" id="width" name="width" placeholder="@lang('bridge_detail.width')" value="{{ $currentDetail->width or '' }}">
                    </div>
                  </div>

                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="bent_amount">@lang('bridge_detail.bent_amount')</label>
                      <input type="number" class="form-control" id="bent_amount" name="bent_amount" placeholder="@lang('bridge_detail.bent_amount')" value="{{ $currentDetail->bent_amount or '' }}">
                    </div>
                  </div>
                </div>

                <h3>@lang('bridge_detail.top_building')</h3>
                <h4>@lang('bridge_detail.upper_structure')</h4>
                <div class="row">
                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="top_type">@lang('bridge_detail.type')</label>
                      <input type="text" class="form-control" id="top_type" name="top_type" placeholder="@lang('bridge_detail.type')" value="{{ $currentDetail->top_type or '' }}">
                    </div>
                  </div>

                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="top_condition">@lang('bridge_detail.condition')</label>
                      <input type="text" class="form-control" id="top_condition" name="top_condition" placeholder="@lang('bridge_detail.condition')" value="{{ $currentDetail->top_condition or '' }}">
                    </div>
                  </div>
                </div>

                <h4>@lang('bridge_detail.floor')</h4>
                <div class="row">
                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="floor_type">@lang('bridge_detail.type')</label>
                      <input type="text" class="form-control" id="floor_type" name="floor_type" placeholder="@lang('bridge_detail.type')" value="{{ $currentDetail->floor_type or '' }}">
                    </div>
                  </div>

                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="floor_material">@lang('bridge_detail.material')</label>
                      <input type="text" class="form-control" id="floor_material" name="floor_material" placeholder="@lang('bridge_detail.material')" value="{{ $currentDetail->floor_material or '' }}">
                    </div>
                  </div>

                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="floor_condition">@lang('bridge_detail.condition')</label>
                      <input type="text" class="form-control" id="floor_condition" name="floor_condition" placeholder="@lang('bridge_detail.condition')" value="{{ $currentDetail->floor_condition or '' }}">
                    </div>
                  </div>
                </div>

                <h4>@lang('bridge_detail.support')</h4>
                <div class="row">
                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="support_type">@lang('bridge_detail.type')</label>
                      <input type="text" class="form-control" id="support_type" name="support_type" placeholder="@lang('bridge_detail.type')" value="{{ $currentDetail->support_type or '' }}">
                    </div>
                  </div>

                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="support_material">@lang('bridge_detail.material')</label>
                      <input type="text" class="form-control" id="support_material" name="support_material" placeholder="@lang('bridge_detail.material')" value="{{ $currentDetail->support_material or '' }}">
                    </div>
                  </div>

                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="support_condition">@lang('bridge_detail.condition')</label>
                      <input type="text" class="form-control" id="support_condition" name="support_condition" placeholder="@lang('bridge_detail.condition')" value="{{ $currentDetail->support_condition or '' }}">
                    </div>
                  </div>
                </div>

                <h4>@lang('bridge_detail.foundation')</h4>
                <div class="row">
                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="foundation_type">@lang('bridge_detail.type')</label>
                      <input type="text" class="form-control" id="foundation_type" name="foundation_type" placeholder="@lang('bridge_detail.type')" value="{{ $currentDetail->foundation_type or '' }}">
                    </div>
                  </div>

                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="foundation_material">@lang('bridge_detail.material')</label>
                      <input type="text" class="form-control" id="foundation_material" name="foundation_material" placeholder="@lang('bridge_detail.material')" value="{{ $currentDetail->foundation_material or '' }}">
                    </div>
                  </div>

                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="foundation_condition">@lang('bridge_detail.condition')</label>
                      <input type="text" class="form-control" id="foundation_condition" name="foundation_condition" placeholder="@lang('bridge_detail.condition')" value="{{ $currentDetail->foundation_condition or '' }}">
                    </div>
                  </div>
                </div>

                <h3>@lang('bridge_detail.lower_building')</h3>
                <h4>@lang('bridge_detail.bridge_head')</h4>
                <div class="row">
                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="head_type">@lang('bridge_detail.type')</label>
                      <input type="text" class="form-control" id="head_type" name="head_type" placeholder="@lang('bridge_detail.type')" value="{{ $currentDetail->head_type or '' }}">
                    </div>
                  </div>

                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="head_material">@lang('bridge_detail.material')</label>
                      <input type="text" class="form-control" id="head_material" name="head_material" placeholder="@lang('bridge_detail.material')" value="{{ $currentDetail->head_material or '' }}">
                    </div>
                  </div>

                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="head_condition">@lang('bridge_detail.condition')</label>
                      <input type="text" class="form-control" id="head_condition" name="head_condition" placeholder="@lang('bridge_detail.condition')" value="{{ $currentDetail->head_condition or '' }}">
                    </div>
                  </div>
                </div>

                <h4>@lang('bridge_detail.pillar')</h4>
                <div class="row">
                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="pillar_type">@lang('bridge_detail.type')</label>
                      <input type="text" class="form-control" id="pillar_type" name="pillar_type" placeholder="@lang('bridge_detail.type')" value="{{ $currentDetail->pillar_type or '' }}">
                    </div>
                  </div>

                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="pillar_material">@lang('bridge_detail.material')</label>
                      <input type="text" class="form-control" id="pillar_material" name="pillar_material" placeholder="@lang('bridge_detail.material')" value="{{ $currentDetail->pillar_material or '' }}">
                    </div>
                  </div>

                  <div class="col-xs-4 col-sm-3">
                    <div class="form-group">
                      <label for="pillar_condition">@lang('bridge_detail.condition')</label>
                      <input type="text" class="form-control" id="pillar_condition" name="pillar_condition" placeholder="@lang('bridge_detail.condition')" value="{{ $currentDetail->pillar_condition or '' }}">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      <label for="notes">@lang('bridge_detail.notes')</label>
                      <textarea class="form-control" id="notes" name="notes" placeholder="@lang('bridge_detail.notes')">{{ $currentDetail->notes or '' }}</textarea>
                    </div>
                  </div>
                </div>

              </div>
            </div>

          </div>
          <div class="box-footer clearfix">
            <button type="submit" class="btn btn-primary pull-left"><i class="fas fa-save"></i> @lang('bridge_detail.submit')</button>
          </div>
        </form>
      </div>
      <div class="col-xs-12 detail-container">
        @if ($details->count() > 0)
        <div class="table-responsive">
          <table class="table table-bordered table-condensed table-hover">
            <thead>
              <tr>
                <th rowspan="3" valign="middle">@lang('bridge_detail.year')</th>
                <th colspan="3">@lang('bridge_detail.dimension')</th>
                <th colspan="11">@lang('bridge_detail.top_building')</th>
                <th colspan="6">@lang('bridge_detail.lower_building')</th>
                <th rowspan="3" valign="middle">@lang('bridge_detail.notes')</th>
                <th rowspan="3" valign="middle" style="width: 40px">@lang('label.action')</th>
              </tr>
              <tr>
                <th rowspan="2" valign="middle">@lang('bridge_detail.length')</th>
                <th rowspan="2" valign="middle">@lang('bridge_detail.width')</th>
                <th rowspan="2" valign="middle">@lang('bridge_detail.bent_amount')</th>
                <th colspan="2">@lang('bridge_detail.upper_structure')</th>
                <th colspan="3">@lang('bridge_detail.floor')</th>
                <th colspan="3">@lang('bridge_detail.support')</th>
                <th colspan="3">@lang('bridge_detail.foundation')</th>
                <th colspan="3">@lang('bridge_detail.bridge_head')</th>
                <th colspan="3">@lang('bridge_detail.pillar')</th>
              </tr>
              <tr>
                <th>@lang('bridge_detail.type')</th>
                <th>@lang('bridge_detail.condition')</th>
                <th>@lang('bridge_detail.type')</th>
                <th>@lang('bridge_detail.material')</th>
                <th>@lang('bridge_detail.condition')</th>
                <th>@lang('bridge_detail.type')</th>
                <th>@lang('bridge_detail.material')</th>
                <th>@lang('bridge_detail.condition')</th>
                <th>@lang('bridge_detail.type')</th>
                <th>@lang('bridge_detail.material')</th>
                <th>@lang('bridge_detail.condition')</th>
                <th>@lang('bridge_detail.type')</th>
                <th>@lang('bridge_detail.material')</th>
                <th>@lang('bridge_detail.condition')</th>
                <th>@lang('bridge_detail.type')</th>
                <th>@lang('bridge_detail.material')</th>
                <th>@lang('bridge_detail.condition')</th>
              </tr>
            </thead>

            <tbody class="bridge-container">
              @foreach ($details as $detail)
              <tr class="{{ ! empty($currentDetail) && $currentDetail->id == $detail->id ? 'active' : '' }}">
                <td>{{ $detail->year or '-' }}</td>
                <td>{{ $detail->length or '-' }}</td>
                <td>{{ $detail->width or '-' }}</td>
                <td>{{ $detail->bent_amount or '-' }}</td>
                <td>{{ $detail->top_type or '-' }}</td>
                <td>{{ $detail->top_condition or '-' }}</td>
                <td>{{ $detail->floor_type or '-' }}</td>
                <td>{{ $detail->floor_material or '-' }}</td>
                <td>{{ $detail->floor_condition or '-' }}</td>
                <td>{{ $detail->support_type or '-' }}</td>
                <td>{{ $detail->support_material or '-' }}</td>
                <td>{{ $detail->support_condition or '-' }}</td>
                <td>{{ $detail->foundation_type or '-' }}</td>
                <td>{{ $detail->foundation_material or '-' }}</td>
                <td>{{ $detail->foundation_condition or '-' }}</td>
                <td>{{ $detail->head_type or '-' }}</td>
                <td>{{ $detail->head_material or '-' }}</td>
                <td>{{ $detail->head_condition or '-' }}</td>
                <td>{{ $detail->pillar_type or '-' }}</td>
                <td>{{ $detail->pillar_material or '-' }}</td>
                <td>{{ $detail->pillar_condition or '-' }}</td>
                <td>{{ $detail->notes or '-' }}</td>
                <td>
                  <div class="btn-group">
                    <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-cog"></i>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                      <li>
                        <a href="{{ route('admin.bridges.details.list', ['id' => $bridge->id, 'year' => $detail->year]) }}" class=""><i class="fas fa-edit"></i> @lang('bridge_detail.edit')</a>
                      </li>
                      <li>
                        <a href="#" class="delete-detail" data-id="{{ $detail->id }}"><i class="fas fa-trash"></i> @lang('bridge_detail.delete')</a>
                      </li>
                    </ul>
                  </div>
                </td>
              </tr> 
              @endforeach
            </tbody>

          </table>
        </div>
        
        @else
        <p class="text-muted">
          @lang('bridge_detail.empty')
        </p>
        @endif
        @if ($details->lastPage() > 1)
        <div class="box-footer clearfix">
          {{ $details->appends(['query' => Request::get('query')])->links('vendor.pagination.default') }}
        </div>
        @endif
      </div>
    </div>
  </div>
</div> 
@stop

@section('customScripts')
<script>
$(function () {
  var currentUrl = window.location.href;
  var parsedUrl = new URI(currentUrl);

  $('#year').select2({
    placeholder: '@lang('bridge_detail.choose_year')',
    width: '100%',
    minimumResultsForSearch: -1,
  });

  $('#year').on('select2:select', function (e) {
    var year = e.params.data.id;
    parsedUrl.setSearch('year', year);
    window.location.href = parsedUrl.toString();
  });

  $('.detail-container').on('click', '.delete-detail', function(event) {
    event.preventDefault();
    var detailId = $(this).data('id');
    var recordEl = $(this).closest('tr');
    $.confirm({
      title: '@lang('bridge.delete_confirmation_title')',
      content: '@lang('bridge.delete_confirmation_content')',
      buttons: {
        confirm: function () {
          $.ajax({
            url: '{{ route('admin.details.destroy') }}/' + detailId,
            method: 'POST',
            dataType: 'json',
          }).done(function(response) {
            window.location.reload(true);
          });
        },
        cancel: function () {
        }
      }
    });
  });

});
</script>
@stop