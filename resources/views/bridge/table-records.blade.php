@foreach ($bridges as $bridge)
<tr>
  <td><strong>{{ $loop->iteration + (($bridges->currentPage() - 1) * $bridges->perPage()) }}</strong></td>
  <td>{!! ! empty($bridge->name) ? preg_replace('/' . preg_quote($query, '/') . '/i', '<strong>$0</strong>', $bridge->name) : '-' !!}</td>
  <td>{{ $bridge->street->name or '-' }}</td>
  <td>
    <div class="btn-group">
      <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
        <i class="fas fa-cog"></i>
      </button>
      <ul class="dropdown-menu dropdown-menu-right">
        <li>
          <a href="{{ route('admin.bridges.edit', ['id' => $bridge->id]) }}" class=""><i class="fas fa-edit"></i> @lang('bridge.edit')</a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="{{ route('admin.bridges.details.list', ['id' => $bridge->id]) }}" class=""><i class="fas fa-clock"></i> @lang('bridge.manage_details')</a>
        </li>
        <li>
          <a href="{{ route('admin.bridges.images.list', ['id' => $bridge->id]) }}" class=""><i class="fas fa-images"></i> @lang('bridge.manage_gallery')</a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="#" class="delete-bridge" data-id="{{ $bridge->id }}"><i class="fas fa-trash"></i> @lang('bridge.delete')</a>
        </li>
      </ul>
    </div>
  </td>
</tr> 
@endforeach