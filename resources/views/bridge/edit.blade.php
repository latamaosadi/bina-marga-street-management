@extends('layouts.dashboard.main')

@section('title')
<h1>
  @lang('bridge.management')
  <small>@lang('bridge.update_sub', ['bridge_name' => $bridge->name])</small>
</h1>
@stop

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <div class="row">
          <div class="col-xs-3 col-lg-2">
            <a href="{{ route('admin.bridges.list') }}" class="btn btn-danger btn-block">
              <i class="fas fa-arrow-left"></i><span class="hidden-xs"> @lang('bridge.see_all')</span> 
            </a>
          </div>
          <div class="col-xs-9 col-lg-10">
          </div>
        </div>
      </div>

      @include('bridge.form', [
        'url' => route('admin.bridges.update', ['id' => $bridge->id]),
        'bridge' => $bridge,
      ])

    </div>
  </div>
</div> 
@stop

@section('customScripts')
@include('bridge.form-js', compact('bridge'))
@stop
