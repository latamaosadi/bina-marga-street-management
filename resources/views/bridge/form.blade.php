<form role="form" action="{{ $url }}" method="POST">
  {{ csrf_field() }}
  <div class="box-body">

    <div class="row">
      <div class="col-sm-12 col-md-6 col-lg-5">

        <div class="form-group">
          <label for="name">@lang('bridge.name')</label>
          <input type="text" class="form-control" id="name" name="name" placeholder="@lang('bridge.name')" value="{{ $bridge->name or '' }}">
        </div>

        <div class="form-group">
          <label for="street_id">@lang('bridge.street')</label>
          <select class="form-control street-selection" id="street" name="street_id" value="{{ $bridge->street_id or '' }}">
            @foreach ($streets as $street)
            <option value="{{ $street->id }}">{{ $street->name }}</option>
            @endforeach
          </select>
        </div>

        <div class="row">
          <div class="col-xs-6">
            <div class="form-group">
              <label for="latitude">@lang('bridge.latitude')</label>
              <input type="text" class="form-control" id="latitude" name="latitude" placeholder="@lang('bridge.latitude')" value="{{ $bridge->latitude or '' }}">
            </div>
          </div>

          <div class="col-xs-6">
            <div class="form-group">
              <label for="longitude">@lang('bridge.longitude')</label>
              <input type="text" class="form-control" id="longitude" name="longitude" placeholder="@lang('bridge.longitude')" value="{{ $bridge->longitude or '' }}">
            </div>
          </div>
        </div>

      </div>

      <div class="col-sm-12 col-md-6 col-lg-7">
        <label for="name">@lang('bridge.location')</label>
        <div id="map" class="map-container"></div>
      </div>
    </div>

  </div>

  <div class="box-footer clearfix">
    <button type="submit" class="btn btn-primary pull-right"><i class="fas fa-save"></i> @lang('bridge.submit')</button>
  </div>
</form>