@extends('layouts.dashboard.main')

@section('content')
<div class="dashboard-page">
  <div class="row">
    <div class="col-xs-12 col-sm-6">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fas fa-road"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Jalan Terdaftar di Sistem</span>
          <span class="info-box-number">{{ $street['count'] }} <small>Jalan</small></span>
        </div>
      </div>
    </div>

    <div class="col-xs-12 col-sm-6">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fas fa-archway"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Jembatan Terdaftar di Sistem</span>
          <span class="info-box-number">{{ $bridge['count'] }} <small>Jembatan</small></span>
        </div>
      </div>
    </div>

    <div class="col-xs-12 col-sm-6">
      <div class="info-box">
        <span class="info-box-icon bg-maroon"><i class="fas fa-road"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Jalan Tanpa Kecamatan</span>
          <span class="info-box-number">{{ $street['noDistricts'] }} <small>Jalan</small></span>
        </div>
      </div>
    </div>

    <div class="col-xs-12 col-sm-6">
      <div class="info-box">
        <span class="info-box-icon bg-maroon"><i class="fas fa-archway"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Jembatan Tanpa Jalan</span>
          <span class="info-box-number">{{ $bridge['noStreets'] }} <small>Jembatan</small></span>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12">
      
      @if (count($street['notMaintained']) > 0)
      <h3 class="">Data Pemeliharaan Jalan - Tahun {{ $year }}</h3>

      <div class="row maintenance-container">
        @foreach ($street['notMaintained'] as $road)
          <div class="col-xs-12 col-sm-4 col-md-3">
            <div class="info-box bg-red">
              <span class="info-box-icon">
                <img src="http://maps.googleapis.com/maps/api/staticmap?size=400x400&path={{ $road->polyline }}&sensor=false&key={{ config('app.gMapKey') }}" alt="">
              </span>

              <div class="info-box-content">
                <span class="info-box-text">{{ $road->name }}</span>
                <span class="info-box-number">{{ $road->last_maintained_at }}</span>

                <span class="progress-description">
                  {{ $road->kecamatan }}
                </span>
              </div>
            </div>
          </div>
        @endforeach
      </div>
      @endif
      
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12 col-sm-8">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Perkembangan Total Panjang Jalan Tahunan</h3>
        </div>
        <div class="box-body">
          <div class="chart-container">
            <canvas class="chart"></canvas>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-4">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fas fa-road"></i> Jalan per Kecamatan</h3>
            </div>
            <div class="box-body">
              <div class="box-list-container">
                <ul class="products-list product-list-in-box">
                  
                  @foreach ($street['perDistrict'] as $district)
                  <li class="item">
                    <div class="product-img">
                      {{ $district->total }}
                    </div>
                    <div class="product-info">
                      <span class="product-title">{{ $district->name }}</span>
                      {{--  <span class="product-description">
                        Samsung 32 1080p 60Hz LED Smart HDTV.
                      </span>  --}}
                    </div>
                  </li>  
                  @endforeach

                  @if (count($street['perDistrict']) < 1)
                    <p>Tidak ada data</p>
                  @endif
                </ul>
              </div>
            </div>
          </div> 
        </div>

        <div class="col-xs-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fas fa-archway"></i> Jembatan per Kecamatan</h3>
            </div>
            <div class="box-body">
              <div class="box-list-container">
                <ul class="products-list product-list-in-box">
                  
                  @foreach ($bridge['perDistrict'] as $district)
                  <li class="item">
                    <div class="product-img">
                      {{ $district->total }}
                    </div>
                    <div class="product-info">
                      <span class="product-title">{{ $district->name }}</span>
                      {{--  <span class="product-description">
                        Samsung 32 1080p 60Hz LED Smart HDTV.
                      </span>  --}}
                    </div>
                  </li>  
                  @endforeach

                  @if (count($bridge['perDistrict']) < 1)
                    <p>Tidak ada data</p>
                  @endif

                </ul>
              </div>
            </div>
          </div> 
        </div>
      </div>
    </div>
  </div>

  
</div>
@stop

@section('customScripts')
<script type="text/javascript">
$(function () {
  var canvas = document.querySelector('.chart');
  var ctx = canvas.getContext('2d');

  new Chart(ctx, {
    type: 'line',
    data: {
      labels: [
        @foreach ($street['lengthPerYear'] as $road)
          '{{ $road->year }}',
        @endforeach
      ],
      datasets: [{
        label: 'Panjang Jalan (m)',
        steppedLine: false,
        data: [
          @foreach ($street['lengthPerYear'] as $road)
            {{ $road->total_length }},
          @endforeach
        ],
        borderColor: 'rgb(255, 99, 132)',
        fill: false,
      }]
    },
    options: {
      responsive: true,
      title: {
        display: true,
        text: 'Perkembangan Panjang Jalan pertahun',
      }
    }
  });
  
});
</script>
@stop
