<script>
  var tag = document.createElement("script");

  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName("script")[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  var player;
  var videoEl = document.querySelectorAll('.video-item');

  var currentVideo = videoEl[0].dataset.videoId;
  var activityRecorded = false;

  function onYouTubeIframeAPIReady() {
    player = new YT.Player("player", {
      videoId: currentVideo,
      playerVars: {
        controls: 0,
        fs: 0,
        rel: 0,
      },
      events: {
        onReady: onPlayerReady,
        onStateChange: onPlayerStateChange,
      }
    });
  }

  function cleanThumbs() {
    var thumbsEl = document.querySelectorAll('.video-item');
    thumbsEl.forEach(function(thumb) {
      thumb.classList.remove("active");
      if (thumb.dataset.videoId === currentVideo) {
        thumb.classList.add("active");
      }
    });
  }

  function loadThumbs(videoList) {
    videoList.forEach(function(video) {
      video.addEventListener("click", function() {
        var vidId = this.dataset.videoId;
        playVideo(vidId);
      });
    });
    cleanThumbs();
  }

  function playVideo(id) {
    currentVideo = id;
    if (player) {
      player.loadVideoById({
        videoId: id,
        suggestedQuality: "large",
      });
      cleanThumbs();
    }
  }

  function onPlayerReady(event) {
    console.log('ready');
    loadThumbs(videoEl);
  }

  function onPlayerStateChange(event) {
    if (event.data === YT.PlayerState.PLAYING && ! activityRecorded) {
      activityRecorded = true;
    }
  }

  function stopVideo() {
    player.stopVideo();
  }

</script>
