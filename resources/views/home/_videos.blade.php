<div class="video-container w-full mt-8 p-2">
  <h1 class="text-center mb-2">@lang('street.videos')</h1>
  <div class="video-player-container w-full">
    <div id="player" class="w-full"></div>
  </div>
  <div class="video-thumbnails-container w-full overflow-x-auto">
    <div class="thumb-inner-container flex">
      @foreach ($videos as $video)
      <div class="video-item flex-no-grow flex-no-shrink" data-video-id="{{ $video->video_url }}">
        <div class="image-box">
          <img class="cover" src="{{ sprintf('https://img.youtube.com/vi/%s/mqdefault.jpg', $video->video_url) }}" alt="{{ $video->title }}">
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>
