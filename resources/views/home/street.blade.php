@extends('layouts.app')

@section('extra-content')
  <div class="mr-4 flex-grow">
  </div>
@stop

@section('content')
  <div class="container mx-auto p-4 pb-12">
    <div class="flex flex-wrap">
      <div class="w-full p-2">
        <a href="{{ route('streets.show', ['id' => $street->id, 'view' => 'print']) }}" class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded" target="_blank"><i class="fas fa-print"></i> @lang('label.print')</a>
        <a href="{{ route('streets.show', ['id' => $street->id, 'view' => 'pdf']) }}" class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded" target="_blank"><i class="fas fa-download"></i> @lang('label.download')</a>
      </div>

      <div class="w-full md:w-2/5 lg:w-1/4 p-2">
        <div class="rounded-lg overflow-hidden shadow pb-8 bg-indigo-darker">
          <div class="cover-container">
            <div class="image-box">
              <img src="{{ ! empty($street->cover->filename) ? asset('storage/' . $street->cover->filename) : asset('images/cover.jpg') }}" alt="" class="cover">
            </div>
          </div>
          <div class="avatar-container mx-auto mb-2">
            <div class="image-box rounded-full overflow-hidden shadow-md border-2 border-white">
              <img src="{{ ! empty($street->avatar->filename) ? asset('storage/' . $street->avatar->filename) : asset('images/avatar.png') }}" alt="" class="cover">
            </div>
          </div>
          <div class="street-detail-container p-4">
            <h2 class="font-bold text-center text-white mb-4 text-2xl">{{ $street->section_number }}</h2>
            <h4 class="font-light text-center text-white mb-1 text-base">{{ $street->base_section_name }} - {{ $street->end_section_name}}</h4>
            <h3 class="font-light text-center text-white text-base">{{ $street->name }}</h3>
          </div>
        </div>
      </div>

      <div class="information-container w-full md:w-3/5 lg:w-3/4 p-2">
        <div class="rounded overflow-hidden shadow">
          <div class="bg-red-light pt-4 px-4">
            <h1 class="text-white">@lang('street.basic_information')</h1>
          </div>
          <div class="p-4 flex flex-wrap">
            <div class="w-full lg:w-2/5">
              <dl>
                <dt class="font-bold">@lang('street.section_number')</dt>
                <dd class="mb-2 text-sm">{{ $street->section_number or '-' }}</dd>

                <dt class="font-bold">@lang('street.base_section_name')</dt>
                <dd class="mb-2 text-sm">{{ $street->base_section_name or '-' }}</dd>

                <dt class="font-bold">@lang('street.end_section_name')</dt>
                <dd class="mb-2 text-sm">{{ $street->end_section_name or '-' }}</dd>

                <dt class="font-bold">@lang('street.name')</dt>
                <dd class="mb-2 text-sm">{{ $street->name or '-' }}</dd>

                <dt class="font-bold">@lang('street.kecamatan')</dt>
                <dd class="mb-2 text-sm">{{ $street->village->district->name or '-' }}</dd>

                <dt class="font-bold">@lang('street.kelurahan')</dt>
                <dd class="mb-2 text-sm">{{ $street->village->name or '-' }}</dd>

                <dt class="font-bold">@lang('street.base_coordinate_section')</dt>
                <dd class="mb-2 text-sm"><code>{{ $street->base_coordinate_section or '-' }}</code></dd>

                <dt class="font-bold">@lang('street.end_coordinate_section')</dt>
                <dd class="mb-2 text-sm"><code>{{ $street->end_coordinate_section or '-' }}</code></dd>
              </dl>
            </div>
            <div class="w-full lg:w-3/5 mt-4 lg:mt-0">
              <dl>
                {{-- <dt class="font-bold">@lang('street.location')</dt> --}}
                <dd>
                  <div class="map-container" id="map"></div>
                </dd>
              </dl>
            </div>
          </div>
        </div>
      </div>

      @if (count($images) > 0)
      <div class="w-full mt-8 p-2">
        <h1 class="text-center mb-2">@lang('street.gallery')</h1>
        <div class="flex flex-wrap justify-center">
          @foreach ($images as $image)
          <div class="image-item-container m-2 rounded-lg shadow-md border-2 lg:border-4 border-blue w-1/4 lg:w-1/6">
            <a href="{{ asset('storage/' . $image->filename) }}" data-lightbox="street-gallery" class="block image-box bg-grey-light">
              <img class="cover" src="{{ asset('storage/' . $image->filename) }}" alt="{{ $image->name }}">
            </a>
            <div class="image-title-container bg-grey pt-3 pb-2 px-2 hidden">
              <h4 class="image-title">{{ $image->name }}</h4>
            </div>
          </div>
          @endforeach
        </div>
      </div>
      @endif

      
      @if (count($videos) > 0)
      @include('home._videos', compact('videos'))
      @endif

      <div class="condition-container w-full mt-8 p-2">
        <div class="rounded overflow-hidden shadow">
          <div class="bg-red-light pt-4 px-4">
            <h1 class="text-white">@lang('street.condition')</h1>
          </div>
          <div class="p-4 overflow-auto">

            @if ($conditions->count() > 0)
            <table class="min-w-full">
              <thead>
                <tr>
                  <th class="text-left px-2">@lang('condition.year')</th>
                  <th class="text-left px-2">@lang('condition.good')</th>
                  <th class="text-left px-2">@lang('condition.average')</th>
                  <th class="text-left px-2">@lang('condition.light')</th>
                  <th class="text-left px-2">@lang('condition.heavy')</th>
                </tr>
              </thead>

              <tbody class="street-container">
                @foreach ($conditions as $condition)
                <tr>
                  <td class="p-2">{{ $condition->year }}</td>
                  <td class="p-2">{{ $condition->good }}</td>
                  <td class="p-2">{{ $condition->average }}</td>
                  <td class="p-2">{{ $condition->light }}</td>
                  <td class="p-2">{{ $condition->heavy }}</td>
                </tr> 
                @endforeach
              </tbody>

            </table>
            @else
            <p class="text-muted">
              @lang('condition.empty')
            </p>
            @endif
      
          </div>
        </div>
      </div>

      <div class="technical-container w-full mt-8 p-2">
        <div class="rounded overflow-hidden shadow">
          <div class="bg-red-light pt-4 px-4">
            <h1 class="text-white">@lang('street.technical')</h1>
          </div>
          <div class="p-4 overflow-auto">

            @if ($technicals->count() > 0)
            <table class="min-w-full">
              <thead>
                <tr>
                  <th class="text-left px-2">@lang('technical.year')</th>
                  <th class="text-left px-2">@lang('technical.length')</th>
                  <th class="text-left px-2">@lang('technical.width')</th>
                  <th class="text-left px-2">@lang('technical.lhrt')</th>
                  <th class="text-left px-2">@lang('technical.vcr')</th>
                  <th class="text-left px-2">@lang('technical.street_type')</th>
                  <th class="text-left px-2">@lang('technical.mst')</th>
                  <th class="text-left px-2">@lang('technical.pavement')</th>
                  <th class="text-left px-2">@lang('technical.gravel')</th>
                  <th class="text-left px-2">@lang('technical.macadam')</th>
                  <th class="text-left px-2">@lang('technical.asphalt')</th>
                  <th class="text-left px-2">@lang('technical.rigid')</th>
                  <th class="text-left px-2">@lang('technical.last_maintained_at')</th>
                  <th class="text-left px-2">@lang('technical.maintenance_type')</th>
                </tr>
              </thead>

              <tbody class="street-container">
                @foreach ($technicals as $technical)
                <tr class="{{ ! empty($currentTechnical) && $currentTechnical->id == $technical->id ? 'active' : '' }}">
                  <td class="p-2">{{ $technical->year or '-' }}</td>
                  <td class="p-2">{{ $technical->length or '-' }}</td>
                  <td class="p-2">{{ $technical->width or '-' }}</td>
                  <td class="p-2">{{ $technical->lhrt or '-' }}</td>
                  <td class="p-2">{{ $technical->vcr or '-' }}</td>
                  <td class="p-2">{{ $technical->street_type_id or '-' }}</td>
                  <td class="p-2">{{ $technical->mst or '-' }}</td>
                  <td class="p-2">{{ $technical->pavement_id or '-' }}</td>
                  <td class="p-2">{{ $technical->gravel or '-' }}</td>
                  <td class="p-2">{{ $technical->macadam or '-' }}</td>
                  <td class="p-2">{{ $technical->asphalt or '-' }}</td>
                  <td class="p-2">{{ $technical->rigid or '-' }}</td>
                  <td class="p-2">{{ $technical->last_maintained_at or '-' }}</td>
                  <td class="p-2">{{ $technical->maintenance_type_id or '-' }}</td>
                </tr> 
                @endforeach
              </tbody>

            </table>
            @else
            <p class="text-muted">
              @lang('technical.empty')
            </p>
            @endif
      
          </div>
        </div>
      </div>

    </div>

  </div>
@stop

@section('customScripts')

@include('home.js.video')

<script>
  var map;
  var poly;
  var bridgeMarkers = {};
  function initMap () {
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -8.670458, lng: 115.212631},
      zoom: 12,
      streetViewControl: false,
      mapTypeControl: false,
      {{--  styles: mapStyle,  --}}
    });

    var lineSymbol = {
      path: google.maps.SymbolPath.CIRCLE,
      {{--  scale: 3,  --}}
      fillColor: '#E3342F',
      strokeColor: '#E3342F',
      fillOpacity: 0.8,
    };

    poly = new google.maps.Polyline({
      strokeColor: '#3c40c6',
      strokeOpacity: 0.8,
      strokeWeight: 4,
      icons: [{
        icon: lineSymbol,
        offset: '100%'
      }],
      @if (! empty($street->polyline))
      path: {!! $street->polyline !!},
      @endif
    });
    poly.setMap(map);

    @if (! empty($street->polyline))
    var bounds = new google.maps.LatLngBounds();
    var points = poly.getPath().getArray();
    for (var n = 0; n < points.length ; n++){
      bounds.extend(points[n]);
    }
    map.fitBounds(bounds);
    @endif

    animateCircle(poly);

    var bridges = [
    @foreach ($street->bridges as $bridge)
      {'id': '{{ $bridge->id }}', 'name': '{{ $bridge->name }}', 'lat': '{{ $bridge->latitude }}', 'lng': '{{ $bridge->longitude }}'},
    @endforeach
    ];

    var icon = {
      url: '{{ asset('images/bridge-b.png') }}', // url
      scaledSize: new google.maps.Size(48, 48), // scaled size
    };

    _.forEach(bridges, function (bridge) {
      bridgeMarkers[bridge.id] = new google.maps.Marker({
        position: new google.maps.LatLng(bridge.lat, bridge.lng),
        icon: icon,
        map: map,
      });
    });
  };
  function animateCircle(line) {
    var count = 0;
    var moveset = 5;
    window.setInterval(function() {
      count = (count + 1) % (moveset * 100);

      var icons = line.get('icons');
      icons[0].offset = (count / moveset) + '%';
      line.set('icons', icons);
    }, 25);
  };
</script>
<script src="http://maps.googleapis.com/maps/api/js?key={{ config('app.gMapKey') }}&callback=initMap" async defer></script>
@stop
