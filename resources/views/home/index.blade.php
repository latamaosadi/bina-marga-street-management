@extends('layouts.app')

@section('extra-content')
  <div class="mr-4 flex-grow">
    <form class="w-full max-w-sm">
      <div class="flex items-center rounded bg-white relative">
        <input id="search-input" class="appearance-none bg-transparent border-none w-full text-red mr-3 py-1 px-2 leading-tight focus:outline-none" type="text" placeholder="E.g. Jalan Sudirman" aria-label="Search Street Name" name="q">
        <button class="flex-no-shrink text-sm text-red py-2 px-2 rounded" type="button">
          <i class="fas fa-search"></i>
        </button>

        <div id="street-result" class="search-result-container rounded shadow-md absolute pin-l min-w-full overflow-hidden hidden">
          <ul class="bg-white list-reset"></ul>
        </div>

      </div>
    </form>
  </div>
@stop

@section('content')
<div class="relative">
  <div id="map" class="map-container"></div>
  <div class="absolute pin-t pin-l mt-2 ml-2">
    <div class="inline-block relative w-64">
      <select class="block appearance-none w-full bg-white border border-grey-light hover:border-grey px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline kecamatan-selection">
        <option value="">@lang('home.all_kecamatan')</option>
        @foreach ($kecamatans->districts as $kecamatan)
        <option value="{{ $kecamatan->id }}">{{ $kecamatan->name }}</option>
        @endforeach
      </select>
      <div class="pointer-events-none absolute pin-y pin-r flex items-center px-2 text-grey-darker">
        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
      </div>
    </div>
  </div>
</div>
@stop

@section('customScripts')
<script>
  var bridgeMarkers = {};
  var paths;
  var polylines = [];
  var colors = {
    'blue': '#3c40c6',
    'red': '#f53b57',
  };
  var mapStyle = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#ebe3cd"
        }
      ]
    },
    {
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#523735"
        }
      ]
    },
    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#f5f1e6"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#c9b2a6"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#dcd2be"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#ae9e90"
        }
      ]
    },
    {
      "featureType": "administrative.neighborhood",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "landscape.natural",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "poi",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#93817c"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#a5b076"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#447530"
        }
      ]
    },
    {
      "featureType": "road",
      "stylers": [
        {
          "visibility": "simplified"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#f5f1e6"
        },
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#fcffef"
        },
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "weight": 0.5
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "stylers": [
        {
          "visibility": "simplified"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#fdfcf8"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#f8c967"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#e9bc62"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#e98d58"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#db8555"
        }
      ]
    },
    {
      "featureType": "road.local",
      "stylers": [
        {
          "weight": 0.5
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#fdffec"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#806b63"
        }
      ]
    },
    {
      "featureType": "transit",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#8f7d77"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#ebe3cd"
        }
      ]
    },
    {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#b9d3c2"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#92998d"
        }
      ]
    }
  ];

  function loadComponents(kecamatanId) {
    var icon = {
      url: '{{ asset('images/bridge-b.png') }}', // url
      scaledSize: new google.maps.Size(24, 24), // scaled size
    };
    var bounds = new google.maps.LatLngBounds();

    _.forOwn(bridgeMarkers, function (bridge) {
      bridge.setMap(null);
    });
    bridgeMarkers = {};

    _.forEach(polylines, function (line) {
      line.setMap(null);
    });
    polylines.length = 0;

    $.ajax({
      method: 'GET',
      url: '{{ route('loadBridges') }}',
      data: {
        'kecamatan': kecamatanId,
      }
    }).done(function (response) {
      var bridges = response;
      _.forEach(bridges, function (bridge) {
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(bridge.coord.lat, bridge.coord.lng),
          icon: icon,
          map: map,
          title: bridge.name,
        });
        bridgeMarkers[bridge.id] = marker;
      });
    });

    $.ajax({
      method: 'GET',
      url: '{{ route('loadStreets') }}',
      data: {
        'kecamatan': kecamatanId,
      }
    }).done(function(response) {
      var streets = response;
      _.each(streets, function(street) {
        var path = _.map(street.path, function(coordinate) {
          return {'lat': parseFloat(coordinate.latitude), 'lng': parseFloat(coordinate.longitude)};
        });
        var polyline = new google.maps.Polyline({
          strokeColor: colors.blue,
          strokeOpacity: 0.5,
          strokeWeight: 3,
          path: path,
          streetId: street.id,
        });
        polyline.setMap(map);
        var points = polyline.getPath().getArray();
        for (var n = 0; n < points.length ; n++){
          bounds.extend(points[n]);
        }
        polylines.push(polyline);

        google.maps.event.addListener(polyline, 'click', function(e) {
          var streetId = this.streetId;

          $.confirm({
            title: false,
            columnClass: 'w-3/4 md:w-1/2 lg:w-1/4 ml-auto mr-auto',
            bootstrapClasses: {
              container: 'w-full street-detail-container',
              row: 'flex',
            },
            buttons: {
              close: function () {
              }
            },
            content: function () {
              var self = this;
              return $.ajax({
                url: '{{ route('streetDetail') }}/' + streetId,
                dataType: 'html',
                method: 'get'
              }).done(function (response) {
                self.setContent(response);
              }).fail(function(){
                self.setContent('Something went wrong.');
              });
            }
          });
        });

        google.maps.event.addListener(polyline, 'mouseover', function(e) {
          this.setOptions({
            strokeWeight: 5,
            strokeColor: colors.red,
          });
        });

        google.maps.event.addListener(polyline, 'mouseout', function(e) {
          this.setOptions({
            strokeWeight: 3,
            strokeColor: colors.blue,
          });
        });
      });
      if (streets.length > 0) {
        map.fitBounds(bounds);
      }
    });
  };

  function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -8.670458, lng: 115.212631},
      zoom: 12,
      streetViewControl: false,
      mapTypeControl: false,
      styles: mapStyle,
    });

    loadComponents();

  };
  $(function () {

    var map;
    var searchInput = document.getElementById('search-input');
    var streetResult = $('#street-result');

    function searchStreets(query) {
      return $.ajax({
        method: 'GET',
        url: '{{ route('streetSearch') }}',
        data: { name: query }
      });
    };

    searchInput.addEventListener('keyup', function (event) {
      if (! this.value) {
        streetResult.addClass('hidden');
        return;
      }
      searchStreets(this.value).done(function (response) {
        streetResult.find('ul').html(response);
        streetResult.removeClass('hidden');
      });
    });

    $('.kecamatan-selection').on('change', function () {
      loadComponents($(this).val());
    });

  });
</script>
<script src="http://maps.googleapis.com/maps/api/js?key={{ config('app.gMapKey') }}&callback=initMap" async defer></script>
@stop
