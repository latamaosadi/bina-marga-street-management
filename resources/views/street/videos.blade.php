@extends('layouts.dashboard.main')

@section('title')
<h1>
  @lang('street.manage_videos')
  <small>{{ $street->name }}</small>
</h1>
@stop

@section('content')
<div class="box box-primary">

  <div class="box-header with-border">
    <div class="row">
      <div class="col-xs-12">
        <div class="btn-group">
          <a href="{{ route('admin.streets.list') }}" class="btn btn-danger">
            <i class="fas fa-arrow-left"></i><span class="hidden-xs"> @lang('street.see_all')</span>
          </a>
          <a href="{{ route('admin.streets.edit', ['id' => $street->id]) }}" type="button" class="btn btn-default">
            <i class="fas fa-edit"></i><span class="hidden-xs"> @lang('street.edit')</span>
          </a>
        </div>
      </div>
    </div>
  </div>

  <div class="box-body">
    <div class="row">
      <div class="col-xs-12 col-md-5 col-lg-4 video-form-container">
        <form action="{{ route('admin.streets.videos.new', ['id' => $street->id]) }}" method="POST">
          {{ csrf_field() }}
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
                <div class="form-group">
                  <label for="video_url">@lang('street.video_url')</label>
                  <input type="text" class="form-control" id="video_url" name="video_url" placeholder="@lang('street.video')">
                </div>
                <div class="form-group">
                  <label for="title">@lang('street.video_title')</label>
                  <input type="text" class="form-control" id="title" name="title" placeholder="@lang('street.video_title')">
                </div>
              </div>
            </div>
          </div>
          <div class="box-footer clearfix">
            <button type="submit" class="btn btn-primary pull-right"><i class="fas fa-save"></i> @lang('street.submit_video')</button>
          </div>
        </form>

      </div>
      <div class="col-xs-12 col-md-7 col-lg-8 video-container">
        @if ($videos->count() > 0)

        @foreach ($videos as $video)
          <div class="video-item-container">
            <div class="video-box">
              <img class="contain" src="{{ sprintf('https://img.youtube.com/vi/%s/mqdefault.jpg', $video->video_url) }}" alt="{{ $video->title }}">
            </div>
            <div class="video-title-container">
              <h4 class="video-title">{{ $video->title }}</h4>
            </div>
            <div class="action-container">
              <div class="btn-group">
                <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                  <i class="fas fa-cog"></i>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                  @if ($street->avatar_id != $video->id)
                  <li>
                    <a href="#" class="use-avatar" data-id="{{ $video->id }}"><i class="fas fa-video"></i> @lang('gallery.use_as_avatar')</a>
                  </li>
                  @endif
                  @if ($street->cover_id != $video->id)
                  <li>
                    <a href="#" class="use-cover" data-id="{{ $video->id }}"><i class="fas fa-video"></i> @lang('gallery.use_as_cover')</a>
                  </li>
                  @endif
                  <li class="divider"></li>
                  <li>
                    <a href="#" class="update-title" data-id="{{ $video->id }}" data-title="{{ $video->title }}"><i class="fas fa-edit"></i> @lang('gallery.edit')</a>
                  </li>
                  <li>
                    <a href="#" class="delete-video" data-id="{{ $video->id }}"><i class="fas fa-trash"></i> @lang('gallery.delete')</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        @endforeach

        @else
        <p class="text-muted">
          @lang('gallery.empty')
        </p>
        @endif
        @if ($videos->lastPage() > 1)
        <div class="box-footer clearfix">
          {{ $videos->appends(['query' => Request::get('query')])->links('vendor.pagination.default') }}
        </div>
        @endif
      </div>
    </div>
  </div>
</div> 
@stop

@section('customScripts')

<script>
$(function () {

  $('.video-container').on('click', '.delete-video', function(event) {
    event.preventDefault();
    var videoId = $(this).data('id');
    var recordEl = $(this).closest('tr');
    $.confirm({
      title: '@lang('street.delete_confirmation_title')',
      content: '@lang('street.delete_confirmation_content')',
      buttons: {
        confirm: function () {
          $.ajax({
            url: '{{ route('admin.streets.videos.destroy', ['id' => $street->id]) }}/' + videoId,
            method: 'POST',
            dataType: 'json',
          }).done(function(response) {
            window.location.reload(true);
          });
        },
        cancel: function () {
        }
      }
    });
  });

});
</script>
@stop