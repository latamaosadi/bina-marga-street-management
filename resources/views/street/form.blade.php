<form role="form" action="{{ $url }}" method="POST">
  {{ csrf_field() }}
  <div class="box-body">

    <div class="row">
      <div class="col-sm-12 col-md-6 col-lg-5">
        <div class="row">
          <div class="col-xs-4">
            <div class="form-group">
              <label for="number">@lang('street.section_number')</label>
              <input type="text" class="form-control" id="number" name="number" placeholder="@lang('street.section_number')" value="{{ $street->section_number or '' }}">
            </div>
          </div>

          <div class="col-xs-8">
            <div class="form-group">
              <label for="name">@lang('street.name')</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="@lang('street.name')" value="{{ $street->name or '' }}">
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="baseName">@lang('street.base_section_name')</label>
          <input type="text" class="form-control" id="baseName" name="base_section_name" placeholder="@lang('street.base_section_name')" value="{{ $street->base_section_name or '' }}">
        </div>

        <div class="form-group">
          <label for="endName">@lang('street.end_section_name')</label>
          <input type="text" class="form-control" id="endName" name="end_section_name" placeholder="@lang('street.end_section_name')" value="{{ $street->end_section_name or '' }}">
        </div>

        <input type="hidden" value="{{ config('app.kabupaten') }}" id="kabupaten" name="kabupaten">

        <div class="form-group">
          <label for="kecamatan">@lang('street.kecamatan')</label>
          <select class="form-control kecamatan-selection" id="kecamatan" name="kecamatan" value="{{ $street->kecamatan_id or '' }}"></select>
        </div>

        <div class="form-group">
          <label for="kelurahan">@lang('street.kelurahan')</label>
          <select class="form-control kelurahan-selection" id="kelurahan" name="kelurahan" value="{{ $street->kelurahan_id or '' }}"></select>
        </div>

        <div class="row">
          <div class="col-xs-5">
            <div class="form-group">
              <label for="base_coordinate_section">@lang('street.base_coordinate_section')</label>
              <input type="text" class="form-control" id="base_coordinate_section" name="base_coordinate_section" placeholder="@lang('street.base_coordinate_section')" readonly value="{{ $street->base_coordinate_section or '' }}">
            </div>
          </div>

          <div class="col-xs-2">
            <a href="#" class="btn btn-default btn-block btn-xs reverse-path"><i class="fas fa-exchange-alt"></i></a>
          </div>

          <div class="col-xs-5">
            <div class="form-group">
              <label for="name">@lang('street.end_coordinate_section')</label>
              <input type="text" class="form-control" id="end_coordinate_section" name="end_coordinate_section" placeholder="@lang('street.end_coordinate_section')" readonly value="{{ $street->end_coordinate_section or '' }}">
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="google_base_street_name">@lang('street.google_base_street_name')</label>
          <input type="text" class="form-control" id="google_base_street_name" name="google_base_street_name" placeholder="@lang('street.google_base_street_name')" readonly value="{{ $street->google_base_street_name or '' }}">
        </div>

        <div class="form-group">
          <label for="google_end_street_name">@lang('street.google_end_street_name')</label>
          <input type="text" class="form-control" id="google_end_street_name" name="google_end_street_name" placeholder="@lang('street.google_end_street_name')" readonly value="{{ $street->google_end_street_name or '' }}">
        </div>

        <div class="form-group">
          <label for="google_middle_street_name">@lang('street.google_middle_street_name')</label>
          <input type="text" class="form-control" id="google_middle_street_name" name="google_middle_street_name" placeholder="@lang('street.google_middle_street_name')" readonly value="{{ $street->google_middle_street_name or '' }}">
        </div>

      </div>

      <div class="col-sm-12 col-md-6 col-lg-7">
        <label for="name">@lang('street.location')</label>
        <div class="row map-buttons-container">
          <div class="col-xs-12">
            <div class="checkbox inline">
              <label>
                <input type="checkbox" class="snap-road-check" checked="checked"> @lang('street.snap_to_road')
              </label>
            </div>
            <div class="checkbox inline">
              <label>
                <input type="checkbox" class="editable-check"> @lang('street.edit_mode')
              </label>
            </div>
          </div>
        </div>
        <div id="map" class="map-container"></div>
        <textarea name="polyline" id="polyline" style="display: none;"></textarea>
      </div>
    </div>

  </div>

  <div class="box-footer clearfix">
    <button type="submit" class="btn btn-primary pull-right"><i class="fas fa-save"></i> @lang('street.submit')</button>
  </div>
</form>