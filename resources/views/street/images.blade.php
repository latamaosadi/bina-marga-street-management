@extends('layouts.dashboard.main')

@section('title')
<h1>
  @lang('street.manage_gallery')
  <small>{{ $street->name }}</small>
</h1>
@stop

@section('content')
<div class="box box-primary">

  <div class="box-header with-border">
    <div class="row">
      <div class="col-xs-12">
        <div class="btn-group">
          <a href="{{ route('admin.streets.list') }}" class="btn btn-danger">
            <i class="fas fa-arrow-left"></i><span class="hidden-xs"> @lang('street.see_all')</span>
          </a>
          <a href="{{ route('admin.streets.edit', ['id' => $street->id]) }}" type="button" class="btn btn-default">
            <i class="fas fa-edit"></i><span class="hidden-xs"> @lang('street.edit')</span>
          </a>
        </div>
      </div>
    </div>
  </div>

  <div class="box-body">
    <div class="row">
      <div class="col-xs-12 col-md-5 col-lg-4 image-form-container">
        <form id="fileupload" action="{{ route('admin.streets.images.new', ['id' => $street->id]) }}" method="POST" enctype="multipart/form-data">
          <div class="row fileupload-buttonbar">
            <div class="col-xs-12">
              <div class="btn-group">
                <button type="button" class="btn btn-success fileinput-button">
                  <i class="fas fa-plus"></i>
                  <span class="hidden-xs">@lang('gallery.add_files')</span>
                  <input type="file" name="files[]" multiple>
                </button>
                <button type="submit" class="btn btn-primary start">
                  <i class="fas fa-upload"></i>
                  <span class="hidden-xs">@lang('gallery.start_upload')</span>
                </button>
              </div>
            </div>
            <div class="col-xs-12">
              <span class="fileupload-process"></span>
            </div>
            <div class="col-xs-12 fileupload-progress fade">
              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                  <div class="progress-bar progress-bar-success" style="width:0%;"></div>
              </div>
              <div class="progress-extended">&nbsp;</div>
            </div>
          </div>
          <div class="box box-primary">
            <div class="box-body table-responsive">
              <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
            </div>
          </div>
        </form>

      </div>
      <div class="col-xs-12 col-md-7 col-lg-8 image-container">
        @if ($images->count() > 0)

        @foreach ($images as $image)
          <div class="image-item-container">
            <div class="image-box">
              <img class="contain" src="{{ asset('storage/' . $image->filename) }}" alt="{{ $image->name }}">
            </div>
            <div class="image-title-container">
              <h4 class="image-title">{{ $image->name }}</h4>
            </div>
            <div class="action-container">
              <div class="btn-group">
                <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                  <i class="fas fa-cog"></i>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                  @if ($street->avatar_id != $image->id)
                  <li>
                    <a href="#" class="use-avatar" data-id="{{ $image->id }}"><i class="fas fa-image"></i> @lang('gallery.use_as_avatar')</a>
                  </li>
                  @endif
                  @if ($street->cover_id != $image->id)
                  <li>
                    <a href="#" class="use-cover" data-id="{{ $image->id }}"><i class="fas fa-image"></i> @lang('gallery.use_as_cover')</a>
                  </li>
                  @endif
                  <li class="divider"></li>
                  <li>
                    <a href="#" class="update-name" data-id="{{ $image->id }}" data-title="{{ $image->name }}"><i class="fas fa-edit"></i> @lang('gallery.edit')</a>
                  </li>
                  <li>
                    <a href="#" class="delete-image" data-id="{{ $image->id }}"><i class="fas fa-trash"></i> @lang('gallery.delete')</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        @endforeach

        @else
        <p class="text-muted">
          @lang('gallery.empty')
        </p>
        @endif
        @if ($images->lastPage() > 1)
        <div class="box-footer clearfix">
          {{ $images->appends(['query' => Request::get('query')])->links('vendor.pagination.default') }}
        </div>
        @endif
      </div>
    </div>
  </div>
</div> 
@stop

@section('customScripts')

<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
  <tr class="template-upload fade">
    <td>
      <span class="preview"></span>
    </td>
    <td>
      <p class="name">{%=file.name%}</p>
      <strong class="error text-danger"></strong>
    </td>
    <td>
      <p class="size">@lang('gallery.processing')</p>
      <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
    </td>
    <td>
      {% if (!i && !o.options.autoUpload) { %}
        <button class="btn btn-xs btn-primary start" disabled>
          <i class="fas fa-upload"></i>
          <span>@lang('gallery.upload')</span>
        </button>
      {% } %}
    </td>
  </tr>
{% } %}
</script>

<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
  <tr class="template-download fade">
    <td>
      <span class="preview">
        {% if (file.thumbnailUrl) { %}
          <a href="#" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.filename%}"></a>
        {% } %}
      </span>
    </td>
    <td>
      <p class="name">
        <span>{%=file.name%}</span>
      </p>
      {% if (file.error) { %}
        <div><span class="label label-danger">Error</span> {%=file.error%}</div>
      {% } %}
    </td>
    <td>
      <span class="size">{%=o.formatFileSize(file.size)%}</span>
    </td>
  </tr>
{% } %}
</script>

<script src="{{ asset('vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}"></script>
<script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<script src="{{ asset('vendor/jquery-file-upload/js/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('vendor/jquery-file-upload/js/jquery.fileupload.js') }}"></script>
<script src="{{ asset('vendor/jquery-file-upload/js/jquery.fileupload-process.js') }}"></script>
<script src="{{ asset('vendor/jquery-file-upload/js/jquery.fileupload-image.js') }}"></script>
<script src="{{ asset('vendor/jquery-file-upload/js/jquery.fileupload-validate.js') }}"></script>
<script src="{{ asset('vendor/jquery-file-upload/js/jquery.fileupload-ui.js') }}"></script>

<script>
$(function () {

  $('#fileupload').fileupload({
    dataType: 'json',
    url: '{{ route('admin.streets.images.new', ['id' => $street->id]) }}',
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      if (progress >= 100) {
        setTimeout(function () {
          window.location.reload();
        }, 1000);
      }
    }
  });

  $('.image-container').on('click', '.delete-image', function(event) {
    event.preventDefault();
    var imageId = $(this).data('id');
    var recordEl = $(this).closest('tr');
    $.confirm({
      title: '@lang('street.delete_confirmation_title')',
      content: '@lang('street.delete_confirmation_content')',
      buttons: {
        confirm: function () {
          $.ajax({
            url: '{{ route('admin.images.destroy') }}/' + imageId,
            method: 'POST',
            dataType: 'json',
          }).done(function(response) {
            window.location.reload(true);
          });
        },
        cancel: function () {
        }
      }
    });
  });

  $('.image-container').on('click', '.update-name', function(event) {
    event.preventDefault();
    var imageId = $(this).data('id');
    var recordEl = $(this).closest('tr');
    var imageTitle = $(this).data('title');
    $.confirm({
      title: '@lang('gallery.edit')',
      content: '' +
      '<form action="" class="formName">' +
      '<div class="form-group">' +
      '<input type="text" placeholder="@lang('gallery.name')" class="name form-control" value="' + imageTitle + '" required />' +
      '</div>' +
      '</form>',
      buttons: {
        formSubmit: {
          text: 'Submit',
          btnClass: 'btn-blue',
          action: function () {
            var name = this.$content.find('.name').val();
            if(!name){
              $.alert('@lang('gallery.validate.name')');
              return false;
            }
            $.ajax({
              url: '{{ route('admin.images.update') }}/' + imageId,
              method: 'POST',
              dataType: 'json',
              data: {
                'name': name
              }
            }).done(function(response) {
              window.location.reload(true);
            });
          }
        },
        cancel: function () {
            //close
        },
      },
      onContentReady: function () {
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
          // if the user submits the form by pressing enter in the field.
          e.preventDefault();
          jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
      }
    });

  });

  $('.image-container').on('click', '.use-avatar', function(event) {
    event.preventDefault();
    var imageId = $(this).data('id');
    var recordEl = $(this).closest('tr');
    $.confirm({
      title: '@lang('gallery.avatar_confirmation_title')',
      content: '@lang('gallery.avatar_confirmation_content')',
      buttons: {
        confirm: function () {
          $.ajax({
            url: '{{ route('admin.streets.setAvatar', ['id' => $street->id]) }}',
            method: 'POST',
            dataType: 'json',
            data: {
              'imageId': imageId
            }
          }).done(function(response) {
            window.location.reload(true);
          });
        },
        cancel: function () {
        }
      }
    });
  });

  $('.image-container').on('click', '.use-cover', function(event) {
    event.preventDefault();
    var imageId = $(this).data('id');
    var recordEl = $(this).closest('tr');
    $.confirm({
      title: '@lang('gallery.cover_confirmation_title')',
      content: '@lang('gallery.cover_confirmation_content')',
      buttons: {
        confirm: function () {
          $.ajax({
            url: '{{ route('admin.streets.setCover', ['id' => $street->id]) }}',
            method: 'POST',
            dataType: 'json',
            data: {
              'imageId': imageId
            }
          }).done(function(response) {
            window.location.reload(true);
          });
        },
        cancel: function () {
        }
      }
    });
  });

});
</script>
@stop