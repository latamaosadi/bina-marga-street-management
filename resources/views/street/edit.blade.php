@extends('layouts.dashboard.main')

@section('title')
<h1>
  @lang('street.management')
  <small>@lang('street.update_sub', ['street_name' => $street->name])</small>
</h1>
@stop

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <div class="row">
          <div class="col-xs-3 col-lg-2">
            <a href="{{ route('admin.streets.list') }}" class="btn btn-danger btn-block">
              <i class="fas fa-arrow-left"></i><span class="hidden-xs"> @lang('street.see_all')</span> 
            </a>
          </div>
          <div class="col-xs-9 col-lg-10">
          </div>
        </div>
      </div>

      @include('street.form', [
        'url' => route('admin.streets.update', ['id' => $street->id]),
        'street' => $street,
      ])

    </div>
  </div>
</div> 
@stop

@section('customScripts')
@include('street.form-js', compact('street'))
@stop
