<script>
  var map;
  var poly;
  var mapListener;
  var service;
  var startMarker;
  var endMarker;
  var deleteMenu;
  var mapStyle = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#ebe3cd"
        }
      ]
    },
    {
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#523735"
        }
      ]
    },
    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#f5f1e6"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#c9b2a6"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#dcd2be"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#ae9e90"
        }
      ]
    },
    {
      "featureType": "administrative.neighborhood",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "landscape.natural",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "poi",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#93817c"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#a5b076"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#447530"
        }
      ]
    },
    {
      "featureType": "road",
      "stylers": [
        {
          "visibility": "simplified"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#f5f1e6"
        },
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#fcffef"
        },
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "weight": 0.5
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "stylers": [
        {
          "visibility": "simplified"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#fdfcf8"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#f8c967"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#e9bc62"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#e98d58"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#db8555"
        }
      ]
    },
    {
      "featureType": "road.local",
      "stylers": [
        {
          "weight": 0.5
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#fdffec"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#806b63"
        }
      ]
    },
    {
      "featureType": "transit",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#8f7d77"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#ebe3cd"
        }
      ]
    },
    {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#b9d3c2"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#92998d"
        }
      ]
    }
  ];
  var geocoder;
  function initMap() {
    geocoder = new google.maps.Geocoder;
    service = new google.maps.DirectionsService();
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -8.670458, lng: 115.212631},
      zoom: 12,
      streetViewControl: false,
      mapTypeControl: false,
      clickableIcons: false,
      styles: mapStyle,
    });

    startMarker = new google.maps.Marker({
      map: map,
      draggable:true,
      title: '@lang('street.start_point')',
      label: {
        'text': 'A',
        'color': '#fff',
      },
      zIndex: 5,
    });

    endMarker = new google.maps.Marker({
      map: map,
      draggable:true,
      title: '@lang('street.end_point')',
      label: {
        'text': 'B',
        'color': '#fff',
      },
      zIndex: 5,
    });

    startMarker.addListener('dragend', function(event) {
      drawPath(event.latLng, endMarker.getPosition(), true);
    });

    endMarker.addListener('dragend', function(event) {
      drawPath(startMarker.getPosition(), event.latLng, true);
    });

    poly = new google.maps.Polyline({
      strokeColor: '#3c40c6',
      strokeOpacity: 0.8,
      strokeWeight: 4,
      editable: false,
      @if (! empty($street->polyline))
      path: {!! $street->polyline !!},
      @endif
    });
    poly.setMap(map);

    @if (! empty($street->polyline))
    compilePathToForm(poly.getPath().getArray());
    var bounds = new google.maps.LatLngBounds();
    var points = poly.getPath().getArray();
    for (var n = 0; n < points.length ; n++){
      bounds.extend(points[n]);
    }
    map.fitBounds(bounds);
    
    @if (empty($street->google_base_street_name))
    getGeoAddress();
    @endif
    
    @endif

    /**
      * A menu that lets a user delete a selected vertex of a path.
      * @constructor
      */
    function DeleteMenu() {
      this.div_ = document.createElement('div');
      this.div_.className = 'delete-menu';
      this.div_.innerHTML = '<i class="fas fa-trash"></i> @lang('street.delete_point')';

      var menu = this;
      google.maps.event.addDomListener(this.div_, 'click', function(event) {
        event.preventDefault();
        menu.removeVertex();
      });
    };

    DeleteMenu.prototype = new google.maps.OverlayView();

    DeleteMenu.prototype.onAdd = function() {
      var deleteMenu = this;
      var map = this.getMap();
      this.getPanes().floatPane.appendChild(this.div_);

      // mousedown anywhere on the map except on the menu div will close the
      // menu.
      this.divListener_ = google.maps.event.addDomListener(map.getDiv(), 'mousedown', function(e) {
        if (e.target != deleteMenu.div_) {
          deleteMenu.close();
        }
      }, true);
    };

    DeleteMenu.prototype.onRemove = function() {
      google.maps.event.removeListener(this.divListener_);
      this.div_.parentNode.removeChild(this.div_);

      // clean up
      this.set('position');
      this.set('path');
      this.set('vertex');
    };

    DeleteMenu.prototype.close = function() {
      this.setMap(null);
    };

    DeleteMenu.prototype.draw = function() {
      var position = this.get('position');
      var projection = this.getProjection();

      if (!position || !projection) {
        return;
      }

      var point = projection.fromLatLngToDivPixel(position);
      this.div_.style.top = point.y + 'px';
      this.div_.style.left = point.x + 'px';
    };

    /**
      * Opens the menu at a vertex of a given path.
      */
    DeleteMenu.prototype.open = function(map, path, vertex) {
      this.set('position', path.getAt(vertex));
      this.set('path', path);
      this.set('vertex', vertex);
      this.setMap(map);
      this.draw();
    };

    /**
      * Deletes the vertex from the path.
      */
    DeleteMenu.prototype.removeVertex = function() {
      var path = this.get('path');
      var vertex = this.get('vertex');

      if (!path || vertex == undefined) {
        this.close();
        return;
      }

      path.removeAt(vertex);
      google.maps.event.removeListener(mapListener);
      this.close();
      setTimeout(function(){
        mapListener = map.addListener('click', addLatLng);
      });
    };

    deleteMenu = new DeleteMenu();

    google.maps.event.addListener(poly, 'rightclick', function(e) {
      // Check if click was on a vertex control point
      if (e.vertex == undefined) {
        return;
      }
      deleteMenu.open(map, poly.getPath(), e.vertex);
    });

    google.maps.event.addListener(poly.getPath(), 'insert_at', function(e) {
      compilePathToForm(poly.getPath().getArray());
    });
    google.maps.event.addListener(poly.getPath(), 'remove_at', function(e) {
      compilePathToForm(poly.getPath().getArray());
    });
    google.maps.event.addListener(poly.getPath(), 'set_at', function(e) {
      compilePathToForm(poly.getPath().getArray());
    });

    mapListener = map.addListener('click', addLatLng);
  };

  function drawPath(origin, destination, resetPath) {
    var path = poly.getPath();
    if (resetPath && $('.snap-road-check').is(':checked')) {
      path.clear();
    } else if (resetPath && ! $('.snap-road-check').is(':checked')) {
      path.removeAt(path.getLength() - 1);
    }
    if (! $('.snap-road-check').is(':checked')) {
      path.push(destination);
      compilePathToForm(path.getArray());
      return;
    }

    service.route({
      origin: origin,
      destination: destination,
      travelMode: google.maps.DirectionsTravelMode.DRIVING
    }, function(result, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
          path.push(result.routes[0].overview_path[i]);
        }
        compilePathToForm(path.getArray());
      }
    });
  };

  // Handles click events on a map, and adds a new point to the Polyline.
  function addLatLng(event) {
    var path = poly.getPath();
    var clickPoint = event.latLng;
    if (path.getLength() > 0) {
      clickPoint = path.getAt(path.getLength() - 1);
    }
    drawPath(clickPoint, event.latLng);
  };

  var geocodeTimeout = null;

  function compilePathToForm(polyline) {
    var pathText = '';
    var startPoint = polyline[0];
    var endPoint = polyline[polyline.length - 1];
    _.each(polyline, function(point, index) {
      var pointText = point.lat() + ',' + point.lng();
      if (index > 0) {
        pathText += ';';
      }
      pathText += pointText;
    });
    document.getElementById('polyline').value = pathText;
    document.getElementById('base_coordinate_section').value = '';
    document.getElementById('end_coordinate_section').value = '';
    startMarker.setPosition(startPoint);
    if (polyline.length < 2) {
      return;
    }
    endMarker.setPosition(endPoint);
    document.getElementById('base_coordinate_section').value = startPoint.lat() + ',' + startPoint.lng();
    document.getElementById('end_coordinate_section').value = endPoint.lat() + ',' + endPoint.lng();

    if (geocodeTimeout) {
      clearTimeout(geocodeTimeout);
    }
    geocodeTimeout = setTimeout(function () {
      getGeoAddress();
    }, 500);
  };

  function loadGeocode(geocoder, point, callbackFn) {
    geocoder.geocode({'location': point}, function (results, status) {
      var googleAddress = results[0];
      console.log(googleAddress);
      if (status === 'OK' && googleAddress) {
        var route = _.find(googleAddress.address_components, function(address) {
          return _.includes(address.types, 'route') || _.includes(address.types, 'premise') || _.includes(address.types, 'administrative_area_level_4');
        });
        if (typeof callbackFn === 'function') {
          callbackFn(route);
        }
      }
    });
  };

  function getGeoAddress() {
    var path = poly.getPath().getArray();
    if (path.length < 2) {
      return;
    }
    var firstPoint = path[0];
    var lastPoint = path[path.length - 1];
    var middlePoint = path[Math.floor((path.length - 1) / 2)];

    loadGeocode(geocoder, firstPoint, function (route) {
      $('#google_base_street_name').val(route.long_name);
    });

    loadGeocode(geocoder, lastPoint, function (route) {
      $('#google_end_street_name').val(route.long_name);
    });

    loadGeocode(geocoder, middlePoint, function (route) {
      $('#google_middle_street_name').val(route.long_name);
    });
  };

  function reversePath(polyline) {
    var newPath = [];
    _.eachRight(polyline.getPath().getArray(), function(point, index) {
      var reversedPoint = {lat: point.lat(), lng: point.lng()};
      newPath.push(reversedPoint);
    });
    polyline.setPath(newPath);
    compilePathToForm(polyline.getPath().getArray());
  };

  $(document).ready(function() {
    var kabupatenEl = $('.kabupaten-selection');
    var kabupatenVal = $('#kabupaten').val();
    var kecamatanEl = $('.kecamatan-selection');
    var kelurahanEl = $('.kelurahan-selection');

    {{--  kabupatenEl.select2({
      placeholder: '@lang('street.choose_kabupaten')',
      width: '100%',
    }).val('{{ $street->kabupaten_id or ''}}').trigger('change');  --}}

    @if (! empty($street->kabupaten_id))
      loadKecamatan('{{ $street->kabupaten_id }}', function() {
        kecamatanEl.val('{{ $street->kecamatan_id }}').trigger('change');
      });
    @else
      loadKecamatan(kabupatenVal);
    @endif

    kecamatanEl.select2({
      placeholder: '@lang('street.choose_kecamatan')',
      width: '100%',
    }).prop('disabled', true);

    @if (! empty($street->kecamatan_id))
      loadKelurahan('{{ $street->kecamatan_id }}', function() {
        kelurahanEl.val('{{ $street->kelurahan_id }}').trigger('change');
      });
    @endif

    kelurahanEl.select2({
      placeholder: '@lang('street.choose_kelurahan')',
      width: '100%',
    }).prop('disabled', true);

    function loadKecamatan(kabupatenId, callbackFn) {
      kecamatanEl.empty().trigger('change').prop('disabled', true);

      $.ajax({
        method: 'GET',
        url: '{{ route('administrative.districts') }}',
        data: {
          cityId: kabupatenId
        }
      }).done(function (response) {
        kecamatanEl.select2({
          placeholder: '@lang('street.choose_kecamatan')',
          width: '100%',
          data: _.map(response, function(value) {
            return {
              'id': value.id,
              'text': value.name,
            };
          })
        }).val('').trigger('change').prop('disabled', false);
        kelurahanEl.val('').trigger('change');

        if (callbackFn) {
          callbackFn();
        }
      });
    };

    {{--  kabupatenEl.on('select2:select', function (e) {
      var cityId = e.params.data.id;
      loadKecamatan(cityId);
    });  --}}

    function loadKelurahan(kecamatanId, callbackFn) {
      kelurahanEl.empty().trigger('change').prop('disabled', true);

      $.ajax({
        method: 'GET',
        url: '{{ route('administrative.villages') }}',
        data: {
          districtId: kecamatanId
        }
      }).done(function (response) {
        kelurahanEl.select2({
          placeholder: '@lang('street.choose_kelurahan')',
          width: '100%',
          data: _.map(response, function(value) {
            return {
              'id': value.id,
              'text': value.name,
            };
          })
        }).val('').trigger('change').prop('disabled', false);

        if (callbackFn) {
          callbackFn();
        }
      });

    };

    kecamatanEl.on('select2:select', function (e) {
      var districtId = e.params.data.id;
      loadKelurahan(districtId);
    });

    // Map Section
    // initMap();
    $('.preview').on({
      'mouseover': function(event) {
        poly.setEditable(false);
      },
      'mouseout': function(event) {
        poly.setEditable(true);
      },
      'click': function(event) {
        event.preventDefault();
      },
    });

    $('.editable-check').on('change', function() {
      poly.setEditable(this.checked);
      deleteMenu.close();
    });

    $('.reverse-path').on('click', function(event) {
      event.preventDefault();
      reversePath(poly);
    });

  });
</script>
<script src="http://maps.googleapis.com/maps/api/js?key={{ config('app.gMapKey') }}&callback=initMap" async defer></script>