<div class="modal micromodal-slide" id="bridge-modal" aria-hidden="true">
  <div class="modal__overlay" tabindex="-1" data-custom-close>
    <div class="modal__container modal-admin bridge-modal" role="dialog" aria-modal="true" aria-labelledby="modal-2-title">
      <header class="modal__header">
        <button class="modal__close" aria-label="Close modal" data-custom-close></button>
      </header>
      <div class="">

        <form role="form" action="{{ route('admin.bridges.store') }}" method="POST">
          {{ csrf_field() }}
          <div class="box-body">

            <div class="row">

              <div class="col-sm-12">

                <div class="form-group">
                  <label for="name">@lang('bridge.name')</label>
                  <input type="text" class="form-control" id="name" name="name" placeholder="@lang('bridge.name')" value="">
                </div>

                <input type="hidden" value="{{ $street->id }}" id="street_id" name="street_id">
                <input type="hidden" value="" id="bridge_id" name="bridge_id">
                <input type="hidden" value="" id="latitude" name="latitude">
                <input type="hidden" value="" id="longitude" name="longitude">

                <div id="bridge-map" class="map-container"></div>

              </div>

            </div>

          </div>

          <div class="box-footer clearfix">
            <button type="submit" class="btn btn-primary pull-right"><i class="fas fa-save"></i> @lang('street.submit')</button>
          </div>
        </form>
        
      </div>
    </div>
  </div>
</div>