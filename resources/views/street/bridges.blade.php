@extends('layouts.dashboard.main')

@section('title')
<h1>
  @lang('street.management')
  <small> - @lang('street.all')</small>
</h1>
@stop

@section('content')
<div class="box box-primary">

  <div class="box-header with-border">
    <div class="row">
      <div class="col-xs-12">
        <div class="btn-group">
          <a href="{{ route('admin.streets.list') }}" class="btn btn-danger">
            <i class="fas fa-arrow-left"></i><span class="hidden-xs"> @lang('street.see_all')</span>
          </a>
          <a href="{{ route('admin.streets.edit', ['id' => $street->id]) }}" type="button" class="btn btn-default">
            <i class="fas fa-edit"></i><span class="hidden-xs"> @lang('street.edit')</span>
          </a>
        </div>
      </div>
    </div>
  </div>

  <div class="box-body">
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-4">

        @if (count($street->bridges) > 0)
        <div class="bridges-container">
        @foreach ($street->bridges as $bridge)
          <div class="bridge-item-container clearfix" data-id="{{ $bridge->id }}" data-name="{{ $bridge->name }}" data-latitude="{{ $bridge->latitude }}" data-longitude="{{ $bridge->longitude }}">
            <strong>{{ $bridge->name }}</strong>
            <div class="action-group btn-group pull-right">
              <button type="button" class="btn btn-xs open-bridge" data-mode="edit"><i class="fas fa-external-link-alt"></i></button>
              <button type="button" class="btn btn-xs delete-bridge"><i class="fas fa-trash"></i></button>
            </div>
          </div>
        @endforeach
        </div>
        @else
        <p class="text-center">@lang('bridge.empty')</p>
        @endif

        <a href="javascript:void(0)" class="btn btn-block btn-primary btn-sm open-bridge" data-mode="add">@lang('bridge.add')</a>
      
      </div>
      <div class="col-xs-12 col-sm-6 col-md-8">
        <div id="map" class="map-container"></div>
      </div>
    </div>
  </div>
</div> 

@include('street.modals._bridge', compact('street'))

@stop

@section('customScripts')
<script>
  var map;
  var poly;
  var mapListener;
  var service;
  var startMarker;
  var endMarker;
  var deleteMenu;
  var bounds;
  var bridgeMarkers = {};
  var polyObject;

  var mapStyle = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#ebe3cd"
        }
      ]
    },
    {
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#523735"
        }
      ]
    },
    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#f5f1e6"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#c9b2a6"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#dcd2be"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#ae9e90"
        }
      ]
    },
    {
      "featureType": "administrative.neighborhood",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "landscape.natural",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "poi",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#93817c"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#a5b076"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#447530"
        }
      ]
    },
    {
      "featureType": "road",
      "stylers": [
        {
          "visibility": "simplified"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#f5f1e6"
        },
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#fcffef"
        },
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "weight": 0.5
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "stylers": [
        {
          "visibility": "simplified"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#fdfcf8"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#f8c967"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#e9bc62"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#e98d58"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#db8555"
        }
      ]
    },
    {
      "featureType": "road.local",
      "stylers": [
        {
          "weight": 0.5
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#fdffec"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#806b63"
        }
      ]
    },
    {
      "featureType": "transit",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#8f7d77"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#ebe3cd"
        }
      ]
    },
    {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#b9d3c2"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#92998d"
        }
      ]
    }
  ];

  function toggleBounce() {
    if (marker.getAnimation() !== null) {
      marker.setAnimation(null);
    } else {
      marker.setAnimation(google.maps.Animation.BOUNCE);
    }
  };

  function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -8.670458, lng: 115.212631},
      zoom: 12,
      streetViewControl: false,
      mapTypeControl: false,
      clickableIcons: false,
      styles: mapStyle,
    });

    polyObject = {
      strokeColor: '#3c40c6',
      strokeOpacity: 0.8,
      strokeWeight: 4,
      editable: false,
      @if (! empty($street->polyline))
      path: {!! $street->polyline !!},
      @endif
    };

    poly = new google.maps.Polyline(polyObject);
    poly.setMap(map);

    @if (! empty($street->polyline))
    bounds = new google.maps.LatLngBounds();
    var points = poly.getPath().getArray();
    for (var n = 0; n < points.length ; n++){
      bounds.extend(points[n]);
    }
    map.fitBounds(bounds);
    @endif

    var bridgePoint;
    var bridges = [
    @foreach ($street->bridges as $bridge)
      {'id': '{{ $bridge->id }}', 'name': '{{ $bridge->name }}', 'lat': '{{ $bridge->latitude }}', 'lng': '{{ $bridge->longitude }}'},
    @endforeach
    ];

    _.forEach(bridges, function (bridge) {
      bridgeMarkers[bridge.id] = new google.maps.Marker({
        position: new google.maps.LatLng(bridge.lat, bridge.lng),
        map: map,
      });
    });

  };

  $(function() {

    $('.bridges-container').on('click', '.delete-bridge', function(event) {
      event.preventDefault();
      var bridgeId = $(this).closest('.bridge-item-container').data('id');
      var recordEl = $(this).closest('tr');
      $.confirm({
        title: '@lang('street.delete_confirmation_title')',
        content: '@lang('street.delete_confirmation_content')',
        buttons: {
          confirm: function () {
            $.ajax({
              url: '{{ route('admin.bridges.destroy') }}/' + bridgeId,
              method: 'POST',
              dataType: 'json',
            }).done(function(response) {
              window.location.reload(true);
            });
          },
          cancel: function () {
          }
        }
      });
    });

    $('.open-bridge').on('click', function (event) {
      event.preventDefault();
      var self = $(this);
      var bridgeContainer = self.closest('.bridge-item-container');
      var mode = self.data('mode');
      var bridge = {
        id: bridgeContainer.data('id'),
        name: bridgeContainer.data('name'),
        latitude: bridgeContainer.data('latitude'),
        longitude: bridgeContainer.data('longitude')
      };

      MicroModal.show('bridge-modal', {
        debugMode: true,
        disableScroll: true,
        onShow: function (modal) {

          function setInput (latLng) {
            $('#latitude').val(latLng.lat());
            $('#longitude').val(latLng.lng());
          };

          var bridgeMap = new google.maps.Map(document.getElementById('bridge-map'), {
            center: {lat: -8.670458, lng: 115.212631},
            zoom: 12,
            streetViewControl: false,
            mapTypeControl: false,
            clickableIcons: false,
            styles: mapStyle,
          });

          var streetPoly = new google.maps.Polyline(polyObject);
          streetPoly.setMap(bridgeMap);

          @if (! empty($street->polyline))
          bridgeMap.fitBounds(bounds);
          @endif

          var startPoint = streetPoly.getPath().getArray()[0];
          var marker = new google.maps.Marker({
            position: startPoint,
            map: bridgeMap,
            title: 'Bridge'
          });

          $('#name').val('');
          $('#bridge_id').val('');
          setInput(startPoint);

          if (mode === 'edit') {
            var bridgePosition = new google.maps.LatLng(bridge.latitude, bridge.longitude);
            marker.setPosition(bridgePosition);

            $('#name').val(bridge.name);
            $('#bridge_id').val(bridge.id);
            setInput(bridgePosition);
          }

          google.maps.event.addListener(streetPoly, 'click', function (event) {
            marker.setPosition(event.latLng);
            setInput(event.latLng);
          });

        },
        onClose: function (modal) {},
        closeTrigger: 'data-custom-close',
        awaitCloseAnimation: true
      });
    });

    $('.bridges-container').on({
      'mouseover': function (event) { 
        var self = $(this);
        bridgeMarkers[self.data('id')].setAnimation(google.maps.Animation.BOUNCE);
      },
      'mouseleave': function (event) { 
        var self = $(this);
        bridgeMarkers[self.data('id')].setAnimation(null);
      },
    }, '.bridge-item-container');

  });
</script>
<script src="http://maps.googleapis.com/maps/api/js?key={{ config('app.gMapKey') }}&callback=initMap" async defer></script>
@stop