@extends('layouts.dashboard.main')

@section('title')
<h1>
  @lang('street.manage_conditions')
  <small>{{ $street->name }}</small>
</h1>
@stop

@section('content')
<div class="box box-primary">

  <div class="box-header with-border">
    <div class="row">
      <div class="col-xs-12">
        <div class="btn-group">
          <a href="{{ route('admin.streets.list') }}" class="btn btn-danger">
            <i class="fas fa-arrow-left"></i><span class="hidden-xs"> @lang('street.see_all')</span>
          </a>
          <a href="{{ route('admin.streets.edit', ['id' => $street->id]) }}" type="button" class="btn btn-default">
            <i class="fas fa-edit"></i><span class="hidden-xs"> @lang('street.edit')</span>
          </a>
        </div>
      </div>
    </div>
  </div>

  <div class="box-body">
    <div class="row">
      <div class="col-xs-12 col-md-5 col-lg-4 condition-form-container">
        <form role="form" action="{{ route('admin.streets.conditions.new', ['id' => $street->id]) }}" method="POST">
          <div class="box-body">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $street->id }}" id="street_id" name="street_id">

            <div class="form-group">
              <label for="year">@lang('condition.year')</label>
              <select class="form-control select2" id="year" name="year">
                @foreach ($years as $year)
                <option value="{{ $year }}" {{ ! empty($currentCondition) ? ($currentCondition->year == $year ? 'selected="selected"' : '') : ($year == $selectedYear ? 'selected="selected"' : '') }}>{{ $year }}</option>
                @endforeach
              </select>
            </div>

            <div class="row">
              <div class="col-xs-6">
                <div class="form-group">
                  <label for="good">@lang('condition.good')</label>
                  <input type="number" class="form-control" id="good" name="good" placeholder="@lang('condition.good')" value="{{ $currentCondition->good or '' }}">
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group">
                  <label for="average">@lang('condition.average')</label>
                  <input type="number" class="form-control" id="average" name="average" placeholder="@lang('condition.average')" value="{{ $currentCondition->average or '' }}">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-6">
                <div class="form-group">
                  <label for="light">@lang('condition.light')</label>
                  <input type="number" class="form-control" id="light" name="light" placeholder="@lang('condition.light')" value="{{ $currentCondition->light or '' }}">
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group">
                  <label for="heavy">@lang('condition.heavy')</label>
                  <input type="number" class="form-control" id="heavy" name="heavy" placeholder="@lang('condition.heavy')" value="{{ $currentCondition->heavy or '' }}">
                </div>
              </div>
            </div>

          </div>
          <div class="box-footer clearfix">
            <button type="submit" class="btn btn-primary pull-left"><i class="fas fa-save"></i> @lang('condition.submit')</button>
          </div>
        </form>
      </div>
      <div class="col-xs-12 col-md-7 col-lg-8 condition-container">
        @if ($conditions->count() > 0)
        <table class="table table-bordered table-condensed table-hover">
          <thead>
            <tr>
              <th>@lang('condition.year')</th>
              <th>@lang('condition.good')</th>
              <th>@lang('condition.average')</th>
              <th>@lang('condition.light')</th>
              <th>@lang('condition.heavy')</th>
              <th style="width: 40px">@lang('label.action')</th>
            </tr>
          </thead>

          <tbody class="street-container">
            @foreach ($conditions as $condition)
            <tr class="{{ ! empty($currentCondition) && $currentCondition->id == $condition->id ? 'active' : '' }}">
              <td>{{ $condition->year }}</td>
              <td class="text-right">{{ $condition->good }}</td>
              <td class="text-right">{{ $condition->average }}</td>
              <td class="text-right">{{ $condition->light }}</td>
              <td class="text-right">{{ $condition->heavy }}</td>
              <td>
                <div class="btn-group">
                  <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="fas fa-cog"></i>
                  </button>
                  <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                      <a href="{{ route('admin.streets.conditions.list', ['id' => $street->id, 'year' => $condition->year]) }}" class=""><i class="fas fa-edit"></i> @lang('condition.edit')</a>
                    </li>
                    <li>
                      <a href="#" class="delete-condition" data-id="{{ $condition->id }}"><i class="fas fa-trash"></i> @lang('condition.delete')</a>
                    </li>
                  </ul>
                </div>
              </td>
            </tr> 
            @endforeach
          </tbody>

        </table>
        @else
        <p class="text-muted">
          @lang('condition.empty')
        </p>
        @endif
        @if ($conditions->lastPage() > 1)
        <div class="box-footer clearfix">
          {{ $conditions->appends(['query' => Request::get('query')])->links('vendor.pagination.default') }}
        </div>
        @endif
      </div>
    </div>
  </div>
</div> 
@stop

@section('customScripts')
<script>
$(function () {
  var currentUrl = window.location.href;
  var parsedUrl = new URI(currentUrl);

  $('#year').select2({
    placeholder: '@lang('condition.choose_year')',
    width: '100%',
    minimumResultsForSearch: -1,
  });

  $('#year').on('select2:select', function (e) {
    var year = e.params.data.id;
    parsedUrl.setSearch('year', year);
    window.location.href = parsedUrl.toString();
  });

  $('.condition-container').on('click', '.delete-condition', function(event) {
    event.preventDefault();
    var conditionId = $(this).data('id');
    var recordEl = $(this).closest('tr');
    $.confirm({
      title: '@lang('street.delete_confirmation_title')',
      content: '@lang('street.delete_confirmation_content')',
      buttons: {
        confirm: function () {
          $.ajax({
            url: '{{ route('admin.conditions.destroy') }}/' + conditionId,
            method: 'POST',
            dataType: 'json',
          }).done(function(response) {
            window.location.reload(true);
          });
        },
        cancel: function () {
        }
      }
    });
  });

});
</script>
@stop