@extends('layouts.dashboard.main')

@section('title')
<h1>
  @lang('street.management')
  <small>@lang('street.all')</small>
</h1>
@stop

@section('content')
<div class="box box-primary">

  <div class="box-header with-border">
    <div class="row">
      <div class="col-xs-3 col-lg-2">
        <a href="{{ route('admin.streets.create') }}" class="btn btn-primary btn-block">
          <i class="fas fa-plus"></i><span class="hidden-xs"> @lang('street.add')</span> 
        </a>
      </div>
      <div class="col-xs-9 col-lg-10">
        <form action="{{ route('admin.streets.list') }}" method="GET">
          <div class="input-group">
            <input type="text" class="form-control" name="query" value="{{ $query }}">
            <div class="input-group-btn">
              <button type="button" class="btn btn-primary"><i class="fas fa-search"></i><span class="hidden-xs"> @lang('street.search')</span></button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="box-body table-responsive street-table-container">
    @if ($streets->count() > 0)
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th style="width: 10px">#</th>
          <th>@lang('street.section_number')</th>
          <th>@lang('street.name')</th>
          <th>@lang('street.base_section_name')</th>
          <th>@lang('street.end_section_name')</th>
          <th>@lang('street.kelurahan')</th>
          <th>@lang('street.kecamatan')</th>
          <th style="width: 40px">@lang('label.action')</th>
        </tr>
      </thead>

      <tbody class="street-container">
      @include('street.table-records', compact('streets', 'query'))
      </tbody>

    </table>
    @else
    <p class="text-muted">
      @lang('street.empty')
    </p>
    @endif
  </div>
  @if ($streets->lastPage() > 1)
  <div class="box-footer clearfix">
    {{ $streets->appends(['query' => Request::get('query')])->links('vendor.pagination.default') }}
  </div>
  @endif
</div> 
@stop

@section('customScripts')
<script>
$(function() {
  function loadStreets() {
    $.ajax({
      url: '{{ route('admin.streets.list') }}',
      method: 'GET',
      dataType: 'html',
    }).done(function(response) {
      $('.street-container').html(response);
    });
  };

  $('.street-container').on('click', '.delete-street', function(event) {
    event.preventDefault();
    var streetId = $(this).data('id');
    var recordEl = $(this).closest('tr');
    $.confirm({
      title: '@lang('street.delete_confirmation_title')',
      content: '@lang('street.delete_confirmation_content')',
      buttons: {
        confirm: function () {
          $.ajax({
            url: '{{ route('admin.streets.destroy') }}/' + streetId,
            method: 'POST',
            dataType: 'json',
          }).done(function(response) {
            window.location.reload(true);
          });
        },
        cancel: function () {
        }
      }
    });
  });
});
</script>
@stop