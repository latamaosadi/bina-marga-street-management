@extends('layouts.pdf.main')

@section('content')
  @include('street.style.pdf')
  
    <div class="header mb-4">
      <p class="text-center h1 font-bold mb-2">{{ $street->name }}</p>
      <p class="text-center h3 font-bold mb-2">{{ $street->base_section_name }} - {{ $street->end_section_name}}</p>
      <p class="text-center h5 font-bold mb-2">#{{ $street->section_number }}</p>
    </div>

    <div class="mb-4">
      <div class="mb-2">
        <p class="h1 font-bold">@lang('street.basic_information')</p>
      </div>
      <div class="">
        <table>
          <tr>
            <td class="font-bold p-2">@lang('street.section_number')</td>
            <td class="p-2">{{ $street->section_number or '-' }}</td>
          </tr>
          <tr>
            <td class="font-bold p-2">@lang('street.base_section_name')</td>
            <td class="p-2">{{ $street->base_section_name or '-' }}</td>
          </tr>
          <tr>
            <td class="font-bold p-2">@lang('street.end_section_name')</td>
            <td class="p-2">{{ $street->end_section_name or '-' }}</td>
          </tr>
          <tr>
            <td class="font-bold p-2">@lang('street.name')</td>
            <td class="p-2">{{ $street->name or '-' }}</td>
          </tr>
          <tr>
            <td class="font-bold p-2">@lang('street.kecamatan')</td>
            <td class="p-2">{{ $street->village->district->name or '-' }}</td>
          </tr>
          <tr>
            <td class="font-bold p-2">@lang('street.kelurahan')</td>
            <td class="p-2">{{ $street->village->name or '-' }}</td>
          </tr>
          <tr>
            <td class="font-bold p-2">@lang('street.base_coordinate_section')</td>
            <td class="p-2"><code>{{ $street->base_coordinate_section or '-' }}</code></td>
          </tr>
          <tr>
            <td class="font-bold p-2">@lang('street.end_coordinate_section')</td>
            <td class="p-2"><code>{{ $street->end_coordinate_section or '-' }}</code></td>
          </tr>
        </table>
      </div>
    </div>

    <div class="mb-4">
      <div class="mb-2">
        <p class="h1 font-bold">@lang('street.condition')</p>
      </div>
      <div class="">

        @if ($conditions->count() > 0)
        <table class="table-bordered">
          <thead>
            <tr>
              <td class="text-left px-2 font-bold">@lang('condition.year')</td>
              <td class="text-left px-2 font-bold">@lang('condition.good')</td>
              <td class="text-left px-2 font-bold">@lang('condition.average')</td>
              <td class="text-left px-2 font-bold">@lang('condition.light')</td>
              <td class="text-left px-2 font-bold">@lang('condition.heavy')</td>
            </tr>
          </thead>

          <tbody class="">
            @foreach ($conditions as $condition)
            <tr>
              <td class="p-2">{{ $condition->year }}</td>
              <td class="p-2">{{ $condition->good }}</td>
              <td class="p-2">{{ $condition->average }}</td>
              <td class="p-2">{{ $condition->light }}</td>
              <td class="p-2">{{ $condition->heavy }}</td>
            </tr> 
            @endforeach
          </tbody>

        </table>
        @else
        <p class="text-muted">
          @lang('condition.empty')
        </p>
        @endif
  
      </div>
    </div>

    <div class="mb-4">
      <div class="mb-2">
        <p class="h1 font-bold">@lang('street.technical')</p>
      </div>
      <div class="">

        @if ($technicals->count() > 0)
        <table class="table-bordered">
          <thead>
            <tr>
              <td class="text-left px-2 font-bold">@lang('technical.year')</td>
              <td class="text-left px-2 font-bold">@lang('technical.length')</td>
              <td class="text-left px-2 font-bold">@lang('technical.width')</td>
              <td class="text-left px-2 font-bold">@lang('technical.lhrt')</td>
              <td class="text-left px-2 font-bold">@lang('technical.vcr')</td>
              <td class="text-left px-2 font-bold">@lang('technical.street_type')</td>
              <td class="text-left px-2 font-bold">@lang('technical.mst')</td>
              <td class="text-left px-2 font-bold">@lang('technical.pavement')</td>
              <td class="text-left px-2 font-bold">@lang('technical.gravel')</td>
              <td class="text-left px-2 font-bold">@lang('technical.macadam')</td>
              <td class="text-left px-2 font-bold">@lang('technical.asphalt')</td>
              <td class="text-left px-2 font-bold">@lang('technical.rigid')</td>
              <td class="text-left px-2 font-bold">@lang('technical.last_maintained_at')</td>
              <td class="text-left px-2 font-bold">@lang('technical.maintenance_type')</td>
            </tr>
          </thead>

          <tbody class="street-container">
            @foreach ($technicals as $technical)
            <tr class="{{ ! empty($currentTechnical) && $currentTechnical->id == $technical->id ? 'active' : '' }}">
              <td class="p-2">{{ $technical->year or '-' }}</td>
              <td class="p-2">{{ $technical->length or '-' }}</td>
              <td class="p-2">{{ $technical->width or '-' }}</td>
              <td class="p-2">{{ $technical->lhrt or '-' }}</td>
              <td class="p-2">{{ $technical->vcr or '-' }}</td>
              <td class="p-2">{{ $technical->street_type_id or '-' }}</td>
              <td class="p-2">{{ $technical->mst or '-' }}</td>
              <td class="p-2">{{ $technical->pavement_id or '-' }}</td>
              <td class="p-2">{{ $technical->gravel or '-' }}</td>
              <td class="p-2">{{ $technical->macadam or '-' }}</td>
              <td class="p-2">{{ $technical->asphalt or '-' }}</td>
              <td class="p-2">{{ $technical->rigid or '-' }}</td>
              <td class="p-2">{{ $technical->last_maintained_at or '-' }}</td>
              <td class="p-2">{{ $technical->maintenance_type_id or '-' }}</td>
            </tr> 
            @endforeach
          </tbody>

        </table>
        @else
        <p class="text-muted">
          @lang('technical.empty')
        </p>
        @endif
  
      </div>
    </div>

  </div>
@stop
