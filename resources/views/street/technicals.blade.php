@extends('layouts.dashboard.main')

@section('title')
<h1>
  @lang('street.manage_technicals')
  <small>{{ $street->name }}</small>
</h1>
@stop

@section('content')
<div class="box box-primary">

  <div class="box-header with-border">
    <div class="row">
      <div class="col-xs-12">
        <div class="btn-group">
          <a href="{{ route('admin.streets.list') }}" class="btn btn-danger">
            <i class="fas fa-arrow-left"></i><span class="hidden-xs"> @lang('street.see_all')</span>
          </a>
          <a href="{{ route('admin.streets.edit', ['id' => $street->id]) }}" type="button" class="btn btn-default">
            <i class="fas fa-edit"></i><span class="hidden-xs"> @lang('street.edit')</span>
          </a>
        </div>
      </div>
    </div>
  </div>

  <div class="box-body">
    <div class="row">
      <div class="col-xs-12 technical-form-container">
        <form role="form" action="{{ route('admin.streets.technicals.new', ['id' => $street->id]) }}" method="POST">
          <div class="box-body">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $street->id }}" id="street_id" name="street_id">


            <div class="row">
              <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                  <label for="year">@lang('technical.year')</label>
                  <select class="form-control select2" id="year" name="year">
                    @foreach ($years as $year)
                    <option value="{{ $year }}" {{ ! empty($currentTechnical) ? ($currentTechnical->year == $year ? 'selected="selected"' : '') : ($year == $selectedYear ? 'selected="selected"' : '') }}>{{ $year }}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="col-xs-6 col-sm-3">
                <div class="form-group">
                  <label for="length">@lang('technical.length')</label>
                  <input type="number" class="form-control" id="length" name="length" placeholder="@lang('technical.length')" value="{{ $currentTechnical->length or '' }}">
                </div>
              </div>

              <div class="col-xs-6 col-sm-3">
                <div class="form-group">
                  <label for="width">@lang('technical.width')</label>
                  <input type="number" class="form-control" id="width" name="width" placeholder="@lang('technical.width')" value="{{ $currentTechnical->width or '' }}">
                </div>
              </div>

              <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                  <label for="lhrt">@lang('technical.lhrt')</label>
                  <input type="number" class="form-control" id="lhrt" name="lhrt" placeholder="@lang('technical.lhrt')" value="{{ $currentTechnical->lhrt or '' }}">
                </div>
              </div>

              <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                  <label for="vcr">@lang('technical.vcr')</label>
                  <input type="number" class="form-control" id="vcr" name="vcr" placeholder="@lang('technical.vcr')" value="{{ $currentTechnical->vcr or '' }}">
                </div>
              </div>

              <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                  <label for="street_type">@lang('technical.street_type')</label>
                  <input type="number" class="form-control" id="street_type" name="street_type_id" placeholder="@lang('technical.street_type')" value="{{ $currentTechnical->street_type_id or '' }}">
                </div>
              </div>

              <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                  <label for="mst">@lang('technical.mst')</label>
                  <input type="number" class="form-control" id="mst" name="mst" placeholder="@lang('technical.mst')" value="{{ $currentTechnical->mst or '' }}">
                </div>
              </div>

              <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                  <label for="pavement">@lang('technical.pavement')</label>
                  <input type="number" class="form-control" id="pavement" name="pavement_id" placeholder="@lang('technical.pavement')" value="{{ $currentTechnical->pavement_id or '' }}">
                </div>
              </div>

              <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                  <label for="gravel">@lang('technical.gravel')</label>
                  <input type="number" class="form-control" id="gravel" name="gravel" placeholder="@lang('technical.gravel')" value="{{ $currentTechnical->gravel or '' }}">
                </div>
              </div>

              <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                  <label for="macadam">@lang('technical.macadam')</label>
                  <input type="number" class="form-control" id="macadam" name="macadam" placeholder="@lang('technical.macadam')" value="{{ $currentTechnical->macadam or '' }}">
                </div>
              </div>

              <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                  <label for="asphalt">@lang('technical.asphalt')</label>
                  <input type="number" class="form-control" id="asphalt" name="asphalt" placeholder="@lang('technical.asphalt')" value="{{ $currentTechnical->asphalt or '' }}">
                </div>
              </div>

              <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                  <label for="rigid">@lang('technical.rigid')</label>
                  <input type="number" class="form-control" id="rigid" name="rigid" placeholder="@lang('technical.rigid')" value="{{ $currentTechnical->rigid or '' }}">
                </div>
              </div>

              <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                  <label for="last_maintained_at">@lang('technical.last_maintained_at')</label>
                  <select class="form-control select2" id="last_maintained_at" name="last_maintained_at">
                    @foreach ($years as $year)
                    <option value="{{ $year }}" {{ ! empty($currentTechnical) ? ($currentTechnical->last_maintained_at == $year ? 'selected="selected"' : '') : ($year == $selectedYear ? 'selected="selected"' : '') }}>{{ $year }}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                  <label for="maintenance_type_id">@lang('technical.maintenance_type')</label>
                  <input type="number" class="form-control" id="maintenance_type_id" name="maintenance_type_id" placeholder="@lang('technical.maintenance_type')" value="{{ $currentTechnical->maintenance_type_id or '' }}">
                </div>
              </div>
            </div>

          </div>
          <div class="box-footer clearfix">
            <button type="submit" class="btn btn-primary pull-left"><i class="fas fa-save"></i> @lang('technical.submit')</button>
          </div>
        </form>
      </div>
      <div class="col-xs-12 technical-container">
        @if ($technicals->count() > 0)
        <table class="table table-bordered table-condensed table-hover">
          <thead>
            <tr>
              <th>@lang('technical.year')</th>
              <th>@lang('technical.length')</th>
              <th>@lang('technical.width')</th>
              <th>@lang('technical.lhrt')</th>
              <th>@lang('technical.vcr')</th>
              <th>@lang('technical.street_type')</th>
              <th>@lang('technical.mst')</th>
              <th>@lang('technical.pavement')</th>
              <th>@lang('technical.gravel')</th>
              <th>@lang('technical.macadam')</th>
              <th>@lang('technical.asphalt')</th>
              <th>@lang('technical.rigid')</th>
              <th>@lang('technical.last_maintained_at')</th>
              <th>@lang('technical.maintenance_type')</th>
              <th style="width: 40px">@lang('label.action')</th>
            </tr>
          </thead>

          <tbody class="street-container">
            @foreach ($technicals as $technical)
            <tr class="{{ ! empty($currentTechnical) && $currentTechnical->id == $technical->id ? 'active' : '' }}">
              <td>{{ $technical->year or '-' }}</td>
              <td>{{ $technical->length or '-' }}</td>
              <td>{{ $technical->width or '-' }}</td>
              <td>{{ $technical->lhrt or '-' }}</td>
              <td>{{ $technical->vcr or '-' }}</td>
              <td>{{ $technical->street_type_id or '-' }}</td>
              <td>{{ $technical->mst or '-' }}</td>
              <td>{{ $technical->pavement_id or '-' }}</td>
              <td>{{ $technical->gravel or '-' }}</td>
              <td>{{ $technical->macadam or '-' }}</td>
              <td>{{ $technical->asphalt or '-' }}</td>
              <td>{{ $technical->rigid or '-' }}</td>
              <td>{{ $technical->last_maintained_at or '-' }}</td>
              <td>{{ $technical->maintenance_type_id or '-' }}</td>
              <td>
                <div class="btn-group">
                  <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="fas fa-cog"></i>
                  </button>
                  <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                      <a href="{{ route('admin.streets.technicals.list', ['id' => $street->id, 'year' => $technical->year]) }}" class=""><i class="fas fa-edit"></i> @lang('technical.edit')</a>
                    </li>
                    <li>
                      <a href="#" class="delete-technical" data-id="{{ $technical->id }}"><i class="fas fa-trash"></i> @lang('technical.delete')</a>
                    </li>
                  </ul>
                </div>
              </td>
            </tr> 
            @endforeach
          </tbody>

        </table>
        @else
        <p class="text-muted">
          @lang('technical.empty')
        </p>
        @endif
        @if ($technicals->lastPage() > 1)
        <div class="box-footer clearfix">
          {{ $technicals->appends(['query' => Request::get('query')])->links('vendor.pagination.default') }}
        </div>
        @endif
      </div>
    </div>
  </div>
</div> 
@stop

@section('customScripts')
<script>
$(function () {
  var currentUrl = window.location.href;
  var parsedUrl = new URI(currentUrl);

  $('#year').select2({
    placeholder: '@lang('technical.choose_year')',
    width: '100%',
    minimumResultsForSearch: -1,
  });

  $('#year').on('select2:select', function (e) {
    var year = e.params.data.id;
    parsedUrl.setSearch('year', year);
    window.location.href = parsedUrl.toString();
  });

  $('.technical-container').on('click', '.delete-technical', function(event) {
    event.preventDefault();
    var technicalId = $(this).data('id');
    var recordEl = $(this).closest('tr');
    $.confirm({
      title: '@lang('street.delete_confirmation_title')',
      content: '@lang('street.delete_confirmation_content')',
      buttons: {
        confirm: function () {
          $.ajax({
            url: '{{ route('admin.technicals.destroy') }}/' + technicalId,
            method: 'POST',
            dataType: 'json',
          }).done(function(response) {
            window.location.reload(true);
          });
        },
        cancel: function () {
        }
      }
    });
  });

});
</script>
@stop