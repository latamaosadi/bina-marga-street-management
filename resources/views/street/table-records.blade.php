@foreach ($streets as $street)
<tr>
  <td><strong>{{ $loop->iteration + (($streets->currentPage() - 1) * $streets->perPage()) }}</strong></td>
  <td>{!! ! empty($street->section_number) ? preg_replace('/' . preg_quote($query, '/') . '/i', '<strong>$0</strong>', $street->section_number) : '-' !!}</td>
  <td>{!! ! empty($street->name) ? preg_replace('/' . preg_quote($query, '/') . '/i', '<strong>$0</strong>', $street->name) : '-' !!}</td>
  <td>{!! ! empty($street->base_section_name) ? preg_replace('/' . preg_quote($query, '/') . '/i', '<strong>$0</strong>', $street->base_section_name) : '-' !!}</td>
  <td>{!! ! empty($street->end_section_name) ? preg_replace('/' . preg_quote($query, '/') . '/i', '<strong>$0</strong>', $street->end_section_name) : '-' !!}</td>
  <td>{{ $street->village->name or '-' }}</td>
  <td>{{ $street->district->name or '-' }}</td>
  <td>
    <div class="btn-group">
      <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
        <i class="fas fa-cog"></i>
      </button>
      <ul class="dropdown-menu dropdown-menu-right">
        <li>
          <a href="{{ route('admin.streets.edit', ['id' => $street->id]) }}" class=""><i class="fas fa-edit"></i> @lang('street.edit')</a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="{{ route('admin.streets.bridges.list', ['id' => $street->id]) }}" class=""><i class="fas fa-archway"></i> @lang('street.manage_bridges')</a>
        </li>
        <li>
          <a href="{{ route('admin.streets.conditions.list', ['id' => $street->id]) }}" class=""><i class="fas fa-clock"></i> @lang('street.manage_conditions')</a>
        </li>
        <li>
          <a href="{{ route('admin.streets.technicals.list', ['id' => $street->id]) }}" class=""><i class="fas fa-book"></i> @lang('street.manage_technicals')</a>
        </li>
        <li>
          <a href="{{ route('admin.streets.images.list', ['id' => $street->id]) }}" class=""><i class="fas fa-images"></i> @lang('street.manage_gallery')</a>
        </li>
        <li>
          <a href="{{ route('admin.streets.videos.list', ['id' => $street->id]) }}" class=""><i class="fab fa-youtube"></i> @lang('street.manage_videos')</a>
        </li>
        <li class="divider"></li>
        <li>
          <a href="#" class="delete-street" data-id="{{ $street->id }}"><i class="fas fa-trash"></i> @lang('street.delete')</a>
        </li>
      </ul>
    </div>
  </td>
</tr> 
@endforeach