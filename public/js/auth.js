$(function () {
  $('.signin').on('click', function (event) {
    event.preventDefault();

    MicroModal.show('modal-auth', {
      debugMode: true,
      disableScroll: true,
      onShow: function (modal) {

        $('.login-container').addClass('opened');

        $('.signup').on('click', function () {
          $('.login-container').removeClass('opened');
          $('.signup-container').addClass('opened');
        });

        $('.signin').on('click', function () {
          $('.signup-container').removeClass('opened');
          $('.login-container').addClass('opened');
        });

      },
      onClose: function (modal) {},
      closeTrigger: 'data-custom-close',
      awaitCloseAnimation: true
    });

  });
});