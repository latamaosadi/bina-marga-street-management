<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/streets/all', 'Street\StreetController@all')->name('loadStreets');
Route::get('/streets/search', 'Street\StreetController@search')->name('streetSearch');
Route::get('/streets/{id?}', 'Street\StreetController@detail')->name('streetDetail');
Route::get('/bridges/all', 'Bridge\BridgeController@all')->name('loadBridges');

Route::group([
    'prefix' => 'administrative',
    'as' => 'administrative.',
], function() {
    Route::get('/administrative/cities', 'Administrative\AdministrativeController@cities')->name('cities');
    Route::get('/administrative/districts', 'Administrative\AdministrativeController@districts')->name('districts');
    Route::get('/administrative/villages', 'Administrative\AdministrativeController@villages')->name('villages');
});

