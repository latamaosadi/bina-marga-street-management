<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Home\HomeController@index')->name('home');

Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
  Route::get('/signin', 'Auth\AuthController@signin')->name('signin');
  Route::get('/signup', 'Auth\AuthController@signup')->name('signup');
  Route::post('/signin', 'Auth\LoginController@authenticate')->name('authenticate');
  Route::post('/signup', 'Auth\RegisterController@register')->name('register');
  Route::get('/logout', 'Auth\LogoutController@logout')->name('logout');
});

Route::group(['prefix' => 'streets', 'as' => 'streets.'], function () {
  Route::get('/{id?}', 'Home\HomeController@streetDetail')->name('show');
});

Route::group(['prefix' => 'bridges', 'as' => 'bridges.'], function () {
  Route::get('/{id?}', 'Home\HomeController@bridgeDetail')->name('show');
});

// Route::get('/login', 'Auth\AuthController@login')->name('login');
// Route::get('/register', 'Auth\AuthController@register')->name('register');
// Route::get('/profile', 'Auth\AuthController@profile')->name('profile');

Route::group([
  'prefix' => 'p',
  'as' => 'admin.',
  // 'middleware' => ['auth']
], function () {

  Route::get('/', 'Dashboard\DashboardController@index')->name('dashboard');

  Route::group(['prefix' => 'streets', 'as' => 'streets.'], function () {
    Route::get('/', 'Street\StreetController@list')->name('list');
    Route::get('/create', 'Street\StreetController@create')->name('create');
    Route::post('/destroy/{id?}', 'Street\StreetController@destroy')->name('destroy');
    Route::post('/', 'Street\StreetController@new')->name('new');

    Route::group(['prefix' => '{id}'], function () {
      Route::get('/', 'Street\StreetController@edit')->name('edit');
      Route::post('/', 'Street\StreetController@update')->name('update');
      Route::post('/avatar', 'Street\StreetController@setAvatar')->name('setAvatar');
      Route::post('/cover', 'Street\StreetController@setCover')->name('setCover');

      Route::group(['prefix' => 'bridges', 'as' => 'bridges.'], function () {
        Route::get('/', 'Street\StreetController@bridgeList')->name('list');
      });

      Route::group(['prefix' => 'conditions', 'as' => 'conditions.'], function () {
        Route::get('/', 'Street\StreetController@conditionList')->name('list');
        Route::post('/', 'Street\StreetController@newCondition')->name('new');
        Route::post('/{conditionId}', 'Street\StreetController@updateCondition')->name('update');
      });

      Route::group(['prefix' => 'technicals', 'as' => 'technicals.'], function () {
        Route::get('/', 'Street\StreetController@technicalList')->name('list');
        Route::post('/', 'Street\StreetController@newTechnical')->name('new');
        Route::post('/{technicalId}', 'Street\StreetController@updateTechnical')->name('update');
      });

      Route::group(['prefix' => 'images', 'as' => 'images.'], function () {
        Route::get('/', 'Street\StreetController@imageList')->name('list');
        Route::post('/', 'Street\StreetController@newImage')->name('new');
        Route::post('/{imageId}', 'Street\StreetController@updateImage')->name('update');
      });

      Route::group(['prefix' => 'videos', 'as' => 'videos.'], function () {
        Route::get('/', 'Street\StreetController@videoList')->name('list');
        Route::post('/', 'Street\StreetController@newVideo')->name('new');
        // Route::post('/{videoId}', 'Street\StreetController@updateVideo')->name('update');
        Route::post('/destroy/{videoId?}', 'Street\StreetController@destroyStreetVideo')->name('destroy');
      });
    });
  });

  Route::group(['prefix' => 'conditions', 'as' => 'conditions.'], function () {
    Route::post('/destroy/{id?}', 'Condition\ConditionController@destroy')->name('destroy');
  });

  Route::group(['prefix' => 'technicals', 'as' => 'technicals.'], function () {
    Route::post('/destroy/{id?}', 'Technical\TechnicalController@destroy')->name('destroy');
  });

  Route::group(['prefix' => 'details', 'as' => 'details.'], function () {
    Route::post('/destroy/{id?}', 'BridgeDetail\BridgeDetailController@destroy')->name('destroy');
  });

  Route::group(['prefix' => 'bridges', 'as' => 'bridges.'], function () {
    Route::get('/', 'Bridge\BridgeController@list')->name('list');
    Route::get('/create', 'Bridge\BridgeController@create')->name('create');
    Route::post('/save', 'Bridge\BridgeController@store')->name('store');
    Route::post('/destroy/{id?}', 'Bridge\BridgeController@destroy')->name('destroy');

    Route::group(['prefix' => '{id}'], function () {
      Route::get('/', 'Bridge\BridgeController@edit')->name('edit');
      Route::post('/', 'Bridge\BridgeController@store')->name('update');

      Route::group(['prefix' => 'details', 'as' => 'details.'], function () {
        Route::get('/', 'Bridge\BridgeController@detailList')->name('list');
        Route::post('/', 'Bridge\BridgeController@newDetail')->name('new');
        Route::post('/{detailId}', 'Bridge\BridgeController@updateDetail')->name('update');
      });

      Route::group(['prefix' => 'images', 'as' => 'images.'], function () {
        Route::get('/', 'Bridge\BridgeController@imageList')->name('list');
        Route::post('/', 'Bridge\BridgeController@newImage')->name('new');
        Route::post('/{imageId}', 'Bridge\BridgeController@updateImage')->name('update');
      });
    });
  });

  Route::group(['prefix' => 'images', 'as' => 'images.'], function () {
    Route::post('/{id?}', 'Image\ImageController@update')->name('update');
    Route::post('/destroy/{id?}', 'Image\ImageController@destroy')->name('destroy');
  });

});