let mix = require('laravel-mix');
let tailwindcss = require('tailwindcss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([
  //   'resources/assets/js/vendor.js'
  //   // './node_modules/jquery/dist/jquery.min.js'
  // ], 'public/js/vendor.js')
  // .js([
    'resources/assets/js/app.js'
  ], 'public/js/app.js')
  .sass('resources/assets/sass/vendor.scss', 'public/css/vendor.css')
  .sass('resources/assets/sass/admin-vendor.scss', 'public/css/admin-vendor.css')
  .sass('resources/assets/sass/app.scss', 'public/css/app.css')
  .sass('resources/assets/sass/pdf.scss', 'public/css/pdf.css')
  .sass('resources/assets/sass/admin/app.scss', 'public/css/admin-custom.css')
  .copy('node_modules/bootstrap/dist/css/bootstrap.min.css', 'public/vendor/bootstrap/css')
  .copy('node_modules/bootstrap/dist/css/bootstrap.min.css', 'public/vendor/bootstrap/css')
  .copy('node_modules/fastclick/lib/fastclick.js', 'public/vendor/fastclick/js')
  .copy('resources/assets/js/auth.js', 'public/js')
  .options({
    processCssUrls: false,
    postCss: [tailwindcss('./tailwind.js')],
  })
  .copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/webfonts');